# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Tripi-A004\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-keepclassmembers class * extends android.webkit.WebChromeClient {
     public void openFileChooser(...);
}

# if proguard is enable
-keep class * extends android.webkit.WebChromeClient { *; }
-dontwarn im.delight.android.webview.**

#for appcompat
-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }


# for support library
-dontwarn android.support.**


# Photoviewer
-dontwarn uk.co.senab.photoview.**
-keep class uk.co.senab.photoview.** { *;}

# for jsoup
-keeppackagenames org.jsoup.nodes

-dontwarn com.github.siyamed.shapeimageview.**
-keep class com.github.siyamed.shapeimageview.** { *; }
-keep interface com.github.siyamed.shapeimageview.** { *; }
-keep class org.kxml2.io.KXmlParser.** { *; }
-keep interface org.kxml2.io.KXmlParser.** { *; }

-dontwarn nl.matshofman.saxrssreader.**
-keep class nl.matshofman.saxrssreader.** { *; }
-keep interface nl.matshofman.saxrssreader.** { *; }

-dontwarn com.friendly.android.**
-keep class com.friendly.android.** { *; }
-keep interface com.friendly.android.** { *; }


# for admob
-keep public class com.google.android.gms.ads.** {
   public *;
}

-keep public class com.google.ads.** {
   public *;
}
-dontwarn com.google.ads.**
-dontwarn com.google.android.gms.internal.**




-keep class * extends java.util.ListResourceBundle {
   protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
   public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
   @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
   public static final ** CREATOR;
}

# for firebase
-keepattributes Signature



# Glide specific rules #
# https://github.com/bumptech/glide

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}


# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**