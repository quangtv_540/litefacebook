package com.friendly.android.adapter.holder;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 10/4/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.event.UiEvent;
import com.friendly.android.model.InstaPhotos;
import com.friendly.android.ui.GridPhotoActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class GridPhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final View mRow;

    @BindView(R.id.thumbnail)
    ImageView thumbnail;
    @BindView(R.id.captain)
    TextView captain;
    @BindView(R.id.btnDownload)
    ImageView btnDownload;
    @BindView(R.id.btnShare)
    ImageView btnShare;

    GridPhotoActivity context;
    List<InstaPhotos> instaPhotosList;

    public GridPhotoHolder(final View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        ButterKnife.bind(this, itemView);

        btnDownload.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        this.mRow = itemView;
    }

    public void setInstaPhotosList(List<InstaPhotos> instaPhotosList) {
        this.instaPhotosList = instaPhotosList;
    }

    public void setContext(GridPhotoActivity context) {
        this.context = context;
    }

    public ImageView getThumbnail() {
        if (thumbnail == null) {
            thumbnail = (ImageView) mRow.findViewById(R.id.thumbnail);
        }
        return thumbnail;
    }

    public void setThumbnail(ImageView thumbnail) {
        this.thumbnail = thumbnail;
    }

    public TextView getCaptain() {
        if (captain == null) {
            captain = (TextView) mRow.findViewById(R.id.captain);
        }
        return captain;
    }

    public void setCaptain(TextView captain) {
        this.captain = captain;
    }

    public ImageView getBtnDownload() {
        if (btnDownload == null) {
            btnDownload = (ImageView) mRow.findViewById(R.id.btnDownload);
        }
        return btnDownload;
    }

    public void setBtnDownload(ImageView btnDownload) {
        this.btnDownload = btnDownload;
    }

    public ImageView getBtnShare() {
        if (btnShare == null) {
            btnShare = (ImageView) mRow.findViewById(R.id.btnShare);
        }
        return btnShare;
    }

    public void setBtnShare(ImageView btnShare) {
        this.btnShare = btnShare;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        int position = getAdapterPosition();
        InstaPhotos instaPhoto = instaPhotosList.get(position);
        if (instaPhoto == null || TextUtils.isEmpty(instaPhoto.url)) {
            return;
        }
        switch (id) {
            case R.id.btnDownload:
                EventBus.getDefault().post(new UiEvent.DownloadPhotoEvent(instaPhoto.url));
                break;
            case R.id.btnShare:
                EventBus.getDefault().post(new UiEvent.SharePhotoEvent(instaPhoto.url));
                break;
            default:
                break;
        }
    }
}
