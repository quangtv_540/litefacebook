package com.friendly.android.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.adapter.holder.AdViewHolder;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.model.Images;
import com.friendly.android.ui.PhotoDetailsActivity;
import com.google.android.gms.ads.AdRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mrquang on 8/14/15.
 */
public class RecentPhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity activity;
    LayoutInflater inflater;
    List<BgrPhotoItem> bgrPhotoItems;
    private static final int ITEM = 1;
    private static final int NATIVE_AD = 2;

    public RecentPhotoAdapter(List<BgrPhotoItem> bgrPhotoItems, Activity activity) {
        this.bgrPhotoItems = bgrPhotoItems;
        this.activity = activity;
        this.inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getItemCount() {
        return bgrPhotoItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return ((position > 0 && position % 21 == 0) ? 2 : 1);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                PhotoListViewHolder holder = (PhotoListViewHolder) viewHolder;
                final BgrPhotoItem item = bgrPhotoItems.get(position);

                if (item != null) {
                    Images images = item.getImagesHashMap().get("thumbnail");
                    if (images == null) {
                        images = item.getImages().get(0);
                    }

                    Picasso.with(activity)
                            .load(images.getUrl())
                            .placeholder(R.drawable.blue_2_drawable)
                            .error(R.drawable.blue_2_drawable)
                            .into(holder.photo);

                    holder.count.setVisibility(View.GONE);

                    holder.setOnClickListener(new PhotoListViewHolder.PhotoListClickListener() {
                        @Override
                        public void onWholePhotoClicked(int pos) {
                            BgrPhotoItem qhdPhotoItem = bgrPhotoItems.get(pos);
                            Intent intent = new Intent(activity, PhotoDetailsActivity.class);
                            intent.putExtra(BgrPhotoItem.KEY, qhdPhotoItem);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                activity.getWindow().setExitTransition(new Explode());
                                activity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
                            } else {
                                activity.startActivity(intent);
                            }
                        }
                    });
                }
                break;
            case NATIVE_AD:
                AdViewHolder holder1 = (AdViewHolder) viewHolder;
                //Load the Ad
                AdRequest request = new AdRequest.Builder()
                        .build();
                holder1.adView.loadAd(request);
                break;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == ITEM) {
            v = inflater.inflate(R.layout.gridview_photos_item, parent, false);
            PhotoListViewHolder holder = new PhotoListViewHolder(v);
            return holder;
        } else if (viewType == NATIVE_AD) {
            v = inflater.inflate(R.layout.native_ads_item, parent, false);
            RecyclerView.ViewHolder holder = new AdViewHolder(v);
            return holder;
        }
        return null;
    }

    public static class PhotoListViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        View itemView;

        @BindView(R.id.photo)
        ImageView photo;
        @BindView(R.id.count)
        TextView count;
        PhotoListClickListener mClickListener;

        public PhotoListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
            this.itemView.setOnClickListener(this);
        }

        public void setOnClickListener(PhotoListClickListener mClickListener) {
            this.mClickListener = mClickListener;
        }

        @Override
        public void onClick(View v) {
            if (mClickListener == null) return;
            int id = v.getId();
            int position = getAdapterPosition();
            switch (id) {
                default:// otherwise whole item be clicked
                    mClickListener.onWholePhotoClicked(position);
                    break;
            }
        }

        public interface PhotoListClickListener {
            void onWholePhotoClicked(int position);
        }
    }
}
