package com.friendly.android.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.model.BgrCategoryItem;
import com.friendly.android.ui.CategoryDetailsActivity;
import com.friendly.android.utils.AppDevices;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mrquang on 8/13/15.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    List<BgrCategoryItem> qhdCategoriesList;
    LayoutInflater infater = null;
    Activity mContext;

    public CategoriesAdapter(Activity context, List<BgrCategoryItem> qhdCategoriesList) {
        infater = LayoutInflater.from(context);
        mContext = context;
        this.qhdCategoriesList = qhdCategoriesList;
    }

    @Override
    public int getItemCount() {
        return qhdCategoriesList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(final CategoriesViewHolder holder, int position) {
        final BgrCategoryItem item = qhdCategoriesList.get(position);
        if (item != null) {
            Picasso.with(mContext)
                    .load(item.getIcon().getUrl())
                    .resize(AppDevices.dp(48), AppDevices.dp(48))
                    .centerCrop()
                    .placeholder(R.drawable.blue_2_drawable)
                    .error(R.drawable.blue_2_drawable)
                    .into(holder.icon);

            holder.name.setText(item.getName());
            holder.count.setText(String.valueOf(item.getCount()));

            holder.name.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Regular.otf"));
            holder.count.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Regular.otf"));

            holder.setOnClickListener(new CategoriesViewHolder.CategoriesClickListener() {
                @Override
                public void onWholeCategoriesClicked(int pos) {
                    BgrCategoryItem bgrCategoryItem = qhdCategoriesList.get(pos);
                    Intent intent = new Intent(mContext, CategoryDetailsActivity.class);
                    intent.putExtra(BgrCategoryItem.KEY, bgrCategoryItem);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mContext.getWindow().setExitTransition(new Explode());
                        mContext.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(mContext).toBundle());
                    } else {
                        mContext.startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = infater.inflate(R.layout.categories_item,
                parent, false);
        CategoriesViewHolder holder = new CategoriesViewHolder(convertView);
        return holder;
    }

    public static class CategoriesViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        View itemView;
        @BindView(R.id.avatar)
        ImageView icon;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.count)
        TextView count;

        CategoriesClickListener mClickListener;

        public CategoriesViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        public void setOnClickListener(CategoriesClickListener mClickListener) {
            this.mClickListener = mClickListener;
        }

        @Override
        public void onClick(View v) {
            if (mClickListener == null) return;
            int id = v.getId();
            int position = getAdapterPosition();
            switch (id) {
                default:// otherwise whole item be clicked
                    mClickListener.onWholeCategoriesClicked(position);
                    break;
            }
        }

        public interface CategoriesClickListener {
            void onWholeCategoriesClicked(int position);
        }
    }
}
