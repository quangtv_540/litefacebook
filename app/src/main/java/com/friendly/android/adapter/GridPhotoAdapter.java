package com.friendly.android.adapter;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 10/4/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.friendly.android.R;
import com.friendly.android.adapter.holder.GridPhotoHolder;
import com.friendly.android.model.InstaPhotos;
import com.friendly.android.ui.GridPhotoActivity;
import com.friendly.android.utils.AppDevices;

import java.util.List;

public class GridPhotoAdapter extends RecyclerView.Adapter<GridPhotoHolder> {

    private GridPhotoActivity context;

    private LayoutInflater inflater;

    private List<InstaPhotos> instaPhotosList;

    int width, height;

    public GridPhotoAdapter(List<InstaPhotos> instaPhotosList, GridPhotoActivity activity) {
        this.instaPhotosList = instaPhotosList;
        this.context = activity;
        this.inflater = LayoutInflater.from(activity);
        this.width = AppDevices.getDeviceWidth() - AppDevices.dp(20);
        this.height = AppDevices.dp(200);
    }

    @Override
    public int getItemCount() {
        return instaPhotosList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public void onBindViewHolder(final GridPhotoHolder holder, int position) {
        final InstaPhotos instaPhoto = instaPhotosList.get(position);

        if (instaPhoto != null) {
            if (!TextUtils.isEmpty(instaPhoto.url)) {
                Glide.with(context).load(instaPhoto.url).asBitmap().centerCrop()
                        .into(new BitmapImageViewTarget(holder.getThumbnail()) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCornerRadius(4);
                        holder.getThumbnail().setImageDrawable(circularBitmapDrawable);
                    }
                });
            }
        }
    }

    @Override
    public GridPhotoHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        View convertView = inflater.inflate(R.layout.grid_photo_item, arg0, false);
        GridPhotoHolder holder = new GridPhotoHolder(convertView);
        holder.setInstaPhotosList(instaPhotosList);
        holder.setContext(context);
        return holder;
    }
}
