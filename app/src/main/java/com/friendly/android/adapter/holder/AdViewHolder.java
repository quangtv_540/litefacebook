package com.friendly.android.adapter.holder;//

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.friendly.android.R;
import com.google.android.gms.ads.NativeExpressAdView;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 2/17/2017.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class AdViewHolder extends RecyclerView.ViewHolder {

    public NativeExpressAdView adView;

    public AdViewHolder(View v) {
        super(v);
        adView = (NativeExpressAdView) v.findViewById(R.id.nativeAdView);
    }
}
