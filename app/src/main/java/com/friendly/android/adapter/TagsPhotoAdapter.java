package com.friendly.android.adapter;//

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.model.Images;
import com.friendly.android.ui.PhotoDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 2/10/2017.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class TagsPhotoAdapter extends RecyclerView.Adapter<TagsPhotoAdapter.TagsPhotoListViewHolder> {

    Activity activity;
    LayoutInflater inflater;
    List<BgrPhotoItem> bgrPhotoItems;

    public TagsPhotoAdapter(List<BgrPhotoItem> bgrPhotoItems, Activity activity) {
        this.bgrPhotoItems = bgrPhotoItems;
        this.activity = activity;
        this.inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getItemCount() {
        return bgrPhotoItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final TagsPhotoListViewHolder holder, int position) {
        final BgrPhotoItem item = bgrPhotoItems.get(position);

        if (item != null) {
            Images images = item.getImagesHashMap().get("thumbnail");
            if (images == null) {
                images = item.getImages().get(0);
            }

            Picasso.with(activity)
                    .load(images.getUrl())
                    .placeholder(R.drawable.blue_2_drawable)
                    .error(R.drawable.blue_2_drawable)
                    .into(holder.photo);

            holder.setOnClickListener(new TagsPhotoListViewHolder.PhotoListClickListener() {
                @Override
                public void onWholePhotoClicked(int pos) {
                    BgrPhotoItem qhdPhotoItem = bgrPhotoItems.get(pos);
                    Intent intent = new Intent(activity, PhotoDetailsActivity.class);
                    intent.putExtra(BgrPhotoItem.KEY, qhdPhotoItem);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        activity.getWindow().setExitTransition(new Explode());
                        activity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
                    } else {
                        activity.startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public TagsPhotoListViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View convertView = inflater.inflate(R.layout.tags_photos_item, parent, false);
        TagsPhotoListViewHolder holder = new TagsPhotoListViewHolder(convertView);
        return holder;
    }

    public static class TagsPhotoListViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        View itemView;

        @BindView(R.id.photo)
        ImageView photo;
        @BindView(R.id.count)
        TextView count;
        PhotoListClickListener mClickListener;

        public TagsPhotoListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
            this.itemView.setOnClickListener(this);
        }

        public void setOnClickListener(PhotoListClickListener mClickListener) {
            this.mClickListener = mClickListener;
        }

        @Override
        public void onClick(View v) {
            if (mClickListener == null) return;
            int id = v.getId();
            int position = getAdapterPosition();
            switch (id) {
                default:// otherwise whole item be clicked
                    mClickListener.onWholePhotoClicked(position);
                    break;
            }
        }

        public interface PhotoListClickListener {
            void onWholePhotoClicked(int position);
        }
    }
}
