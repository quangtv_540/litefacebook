package com.friendly.android.model;//

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

//
//  Tripi
//
//  Created by Quang Tran on 2/9/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class BgrPhotoUsers implements Parcelable {

    @SerializedName("data")
    private List<BgrPhotoItem> bgrPhotoItems;
    @SerializedName("type")
    private String type;
    @SerializedName("next")
    private String next;
    @SerializedName("headers")
    private List<BgrUserHeaders> headers;

    public BgrPhotoUsers() {
        super();
    }

    public List<BgrPhotoItem> getBgrPhotoItems() {
        return bgrPhotoItems;
    }

    public void setBgrPhotoItems(List<BgrPhotoItem> bgrPhotoItems) {
        this.bgrPhotoItems = bgrPhotoItems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<BgrUserHeaders> getHeaders() {
        return headers;
    }

    public void setHeaders(List<BgrUserHeaders> headers) {
        this.headers = headers;
    }

    BgrPhotoUsers(Parcel in) {
        this.next = in.readString();
        this.type = in.readString();
        in.readTypedList(bgrPhotoItems, BgrPhotoItem.CREATOR);
        in.readTypedList(headers, BgrUserHeaders.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(next);
        dest.writeString(type);
        dest.writeTypedList(bgrPhotoItems);
        dest.writeTypedList(headers);
    }

    public static final Parcelable.Creator<BgrPhotoUsers> CREATOR = new Parcelable.Creator<BgrPhotoUsers>() {
        @Override
        public BgrPhotoUsers[] newArray(int size) {
            return new BgrPhotoUsers[size];
        }

        @Override
        public BgrPhotoUsers createFromParcel(Parcel source) {
            return new BgrPhotoUsers(source);
        }
    };
}
