package com.friendly.android.model;//

//
//  Tripi
//
//  Created by Quang Tran on 9/30/2016.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class InstaPhotos {

    public String title;
    public String url;

    public InstaPhotos(String title, String url) {
        this.url = url;
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InstaPhotos changePassword = (InstaPhotos) o;
        return (title == null ? changePassword.title == null : title.equals(changePassword.title)) &&
                (url == null ? changePassword.url == null : url.equals(changePassword.url));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (title == null ? 0: title.hashCode());
        result = 31 * result + (url == null ? 0: url.hashCode());
        return result;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class InstaPhotos {\n");

        sb.append("  title: ").append(title).append("\n");
        sb.append("  url: ").append(url).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
