package com.friendly.android.model;//

import com.google.gson.annotations.SerializedName;

import java.util.List;

//
//  Tripi
//
//  Created by Quang Tran on 2/10/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class BgrPhotoTags {

    @SerializedName("type")
    private String type;
    @SerializedName("next")
    private String next;
    @SerializedName("headers")
    private List<BgrTagsHeaders> headers = null;
    @SerializedName("data")
    private List<BgrPhotoItem> bgrPhotoItems;

    public BgrPhotoTags() {
        super();
    }

    public List<BgrPhotoItem> getBgrPhotoItems() {
        return bgrPhotoItems;
    }

    public void setBgrPhotoItems(List<BgrPhotoItem> bgrPhotoItems) {
        this.bgrPhotoItems = bgrPhotoItems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<BgrTagsHeaders> getHeaders() {
        return headers;
    }

    public void setHeaders(List<BgrTagsHeaders> headers) {
        this.headers = headers;
    }
}
