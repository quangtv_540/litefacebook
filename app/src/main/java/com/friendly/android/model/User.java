package com.friendly.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mrquang on 8/24/15.
 */
public class User implements Parcelable {

    public static final String KEY = "index.user";

    @SerializedName("avatar")
    private Avatar avatar;
    @SerializedName("description")
    private String description;
    @SerializedName("job")
    private String job;
    @SerializedName("location")
    private String location;
    @SerializedName("homepage")
    private String homepage;
    @SerializedName("username")
    private String username;
    @SerializedName("name")
    private String name;
    @SerializedName("data_url")
    private String data_url;
    @SerializedName("followers_count")
    private int followers_count;
    @SerializedName("followings_count")
    private int followings_count;
    @SerializedName("backgrounds_count")
    private int backgrounds_count;

    public User() {
        super();
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData_url() {
        return data_url;
    }

    public void setData_url(String data_url) {
        this.data_url = data_url;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }

    public int getFollowings_count() {
        return followings_count;
    }

    public void setFollowings_count(int followings_count) {
        this.followings_count = followings_count;
    }

    public int getBackgrounds_count() {
        return backgrounds_count;
    }

    public void setBackgrounds_count(int backgrounds_count) {
        this.backgrounds_count = backgrounds_count;
    }

    protected User(Parcel in) {
        avatar = (Avatar) in.readValue(Avatar.class.getClassLoader());
        description = in.readString();
        job = in.readString();
        location = in.readString();
        homepage = in.readString();
        name = in.readString();
        username = in.readString();
        data_url = in.readString();
        followers_count = in.readInt();
        followings_count = in.readInt();
        backgrounds_count = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(avatar);
        dest.writeString(description);
        dest.writeString(job);
        dest.writeString(location);
        dest.writeString(homepage);
        dest.writeString(name);
        dest.writeString(username);
        dest.writeString(data_url);
        dest.writeInt(followers_count);
        dest.writeInt(followings_count);
        dest.writeInt(backgrounds_count);
    }

    @SuppressWarnings("unused")
    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
