package com.friendly.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mrquang on 8/13/15.
 */
public class BgrCategoryItem implements Parcelable {

    public static final String KEY = "index.key.category.item";

    @SerializedName("uuid")
    private String uuid = null;
    @SerializedName("name")
    private String name = null;
    @SerializedName("count")
    private Integer count = null;
    @SerializedName("icon")
    private Icon icon = null;
    @SerializedName("data_url")
    private String data_url = null;

    public BgrCategoryItem() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getData_url() {
        return data_url;
    }

    public void setData_url(String data_url) {
        this.data_url = data_url;
    }

    public static class Icon implements Parcelable {

        @SerializedName("url")
        private String url = null;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        protected Icon(Parcel in) {
            url = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(url);
        }

        @SuppressWarnings("unused")
        public static final Creator<Icon> CREATOR = new Creator<Icon>() {
            @Override
            public Icon createFromParcel(Parcel in) {
                return new Icon(in);
            }

            @Override
            public Icon[] newArray(int size) {
                return new Icon[size];
            }
        };
    }

    protected BgrCategoryItem(Parcel in) {
        uuid = in.readString();
        name = in.readString();
        icon = (Icon) in.readValue(Icon.class.getClassLoader());
        data_url = in.readString();
        count = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uuid);
        dest.writeString(name);
        dest.writeValue(icon);
        dest.writeString(data_url);
        dest.writeInt(count);
    }

    @SuppressWarnings("unused")
    public static final Creator<BgrCategoryItem> CREATOR = new Creator<BgrCategoryItem>() {
        @Override
        public BgrCategoryItem createFromParcel(Parcel in) {
            return new BgrCategoryItem(in);
        }

        @Override
        public BgrCategoryItem[] newArray(int size) {
            return new BgrCategoryItem[size];
        }
    };

}
