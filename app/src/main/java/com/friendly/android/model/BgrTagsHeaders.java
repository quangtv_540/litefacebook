package com.friendly.android.model;//

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

//
//  Tripi
//
//  Created by Quang Tran on 2/10/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class BgrTagsHeaders implements Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("type")
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    protected BgrTagsHeaders(Parcel in) {
        name = in.readString();
        type = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(type);
    }

    @SuppressWarnings("unused")
    public static final Creator<BgrTagsHeaders> CREATOR = new Creator<BgrTagsHeaders>() {
        @Override
        public BgrTagsHeaders createFromParcel(Parcel in) {
            return new BgrTagsHeaders(in);
        }

        @Override
        public BgrTagsHeaders[] newArray(int size) {
            return new BgrTagsHeaders[size];
        }
    };
}
