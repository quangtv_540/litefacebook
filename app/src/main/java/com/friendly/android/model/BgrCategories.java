package com.friendly.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mrquang on 8/13/15.
 */
public class BgrCategories implements Parcelable {

    @SerializedName("data_type")
    private String data_type = null;
    @SerializedName("title")
    private String title = null;
    @SerializedName("type")
    private String type = null;
    @SerializedName("view_type")
    private String view_type = null;
    @SerializedName("headers")
    private List<BgrCategoryHeaders> headers = null;
    @SerializedName("data")
    private List<BgrCategoryItem> bgrCategoryItems = null;

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getView_type() {
        return view_type;
    }

    public void setView_type(String view_type) {
        this.view_type = view_type;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public List<BgrCategoryHeaders> getHeaders() {
        return headers;
    }

    public void setHeaders(List<BgrCategoryHeaders> headers) {
        this.headers = headers;
    }

    public List<BgrCategoryItem> getBgrCategoryItems() {
        return bgrCategoryItems;
    }
    public void setBgrCategoryItems(List<BgrCategoryItem> bgrCategoryItems) {
        this.bgrCategoryItems = bgrCategoryItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BgrCategories bgrCategories = (BgrCategories) o;
        return (this.type == null ? bgrCategories.type == null : this.type.equals(bgrCategories.type)) &&
                (this.bgrCategoryItems == null ? bgrCategories.bgrCategoryItems == null : this.bgrCategoryItems.equals(bgrCategories.bgrCategoryItems));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (this.type == null ? 0: this.type.hashCode());
        result = 31 * result + (this.bgrCategoryItems == null ? 0: this.bgrCategoryItems.hashCode());
        return result;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class BgrCategories {\n");

        sb.append("  type: ").append(type).append("\n");
        sb.append("  bgrCategoryItems: ").append(bgrCategoryItems).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    BgrCategories(Parcel in) {
        this.data_type = in.readString();
        this.title = in.readString();
        this.view_type = in.readString();
        this.type = in.readString();
        in.readTypedList(headers, BgrCategoryHeaders.CREATOR);
        in.readTypedList(bgrCategoryItems, BgrCategoryItem.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(data_type);
        dest.writeString(title);
        dest.writeString(view_type);
        dest.writeString(type);
        dest.writeTypedList(headers);
        dest.writeTypedList(bgrCategoryItems);
    }

    public static final Creator<BgrCategories> CREATOR = new Creator<BgrCategories>() {
        @Override
        public BgrCategories[] newArray(int size) {
            return new BgrCategories[size];
        }

        @Override
        public BgrCategories createFromParcel(Parcel source) {
            return new BgrCategories(source);
        }
    };
}
