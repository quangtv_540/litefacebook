package com.friendly.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mrquang on 8/14/15.
 */
public class BgrPhotoItem implements Parcelable {

    public static final String KEY = "bgr.photo.item.index";

    @SerializedName("id")
    private int id;
    @SerializedName("description")
    private String description;
    @SerializedName("license")
    private License license;
    @SerializedName("tags")
    private List<Tags> tags;
    @SerializedName("user")
    private User user;
    @SerializedName("regdate")
    private long regdate;
    @SerializedName("pubdate")
    private long pubdate;
    @SerializedName("title")
    private String title;
    @SerializedName("uuid")
    private int uuid;
    @SerializedName("images")
    private List<Images> images;
    @SerializedName("views_count")
    private int views_count;
    @SerializedName("likes_count")
    private int likes_count;
    @SerializedName("comments_count")
    private int comments_count;
    @SerializedName("downloads_count")
    private int downloads_count;
    @SerializedName("downloads_daily_count")
    private int downloads_daily_count;
    @SerializedName("downloads_weekly_count")
    private int downloads_weekly_count;
    @SerializedName("downloads_monthly_count")
    private int downloads_monthly_count;
    @SerializedName("share_url")
    private String share_url;
    @SerializedName("data_url")
    private String data_url;

    private HashMap<String, Images> imagesHashMap;

    public BgrPhotoItem() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Images> getImages() {
        return images;
    }

    public void setImages(List<Images> images) {
        this.images = images;
    }

    public long getRegdate() {
        return regdate;
    }

    public void setRegdate(long regdate) {
        this.regdate = regdate;
    }

    public long getPubdate() {
        return pubdate;
    }

    public void setPubdate(long pubdate) {
        this.pubdate = pubdate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUuid() {
        return uuid;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public int getDownloads_count() {
        return downloads_count;
    }

    public void setDownloads_count(int downloads_count) {
        this.downloads_count = downloads_count;
    }

    public int getDownloads_daily_count() {
        return downloads_daily_count;
    }

    public void setDownloads_daily_count(int downloads_daily_count) {
        this.downloads_daily_count = downloads_daily_count;
    }

    public int getDownloads_weekly_count() {
        return downloads_weekly_count;
    }

    public void setDownloads_weekly_count(int downloads_weekly_count) {
        this.downloads_weekly_count = downloads_weekly_count;
    }

    public int getDownloads_monthly_count() {
        return downloads_monthly_count;
    }

    public void setDownloads_monthly_count(int downloads_monthly_count) {
        this.downloads_monthly_count = downloads_monthly_count;
    }

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public String getData_url() {
        return data_url;
    }

    public void setData_url(String data_url) {
        this.data_url = data_url;
    }

    public HashMap<String, Images> getImagesHashMap() {
        if (imagesHashMap == null && images != null) {
            imagesHashMap = new HashMap<>();
            for (Images image : images) {
                imagesHashMap.put(image.getType(), image);
            }
        }
        return imagesHashMap;
    }

    protected BgrPhotoItem(Parcel in) {
        id = in.readInt();
        description = in.readString();
        license = (License) in.readValue(License.class.getClassLoader());
        if (in.readByte() == 0x01) {
            tags = new ArrayList<>();
            in.readList(tags, Tags.class.getClassLoader());
        } else {
            tags = null;
        }
        user = (User) in.readValue(User.class.getClassLoader());

        regdate = in.readLong();
        pubdate = in.readLong();
        title = in.readString();
        uuid = in.readInt();
        if (in.readByte() == 0x01) {
            images = new ArrayList<>();
            in.readList(images, Images.class.getClassLoader());
        } else {
            images = null;
        }
        views_count = in.readInt();
        likes_count = in.readInt();
        comments_count = in.readInt();
        downloads_count = in.readInt();
        downloads_daily_count = in.readInt();
        downloads_weekly_count = in.readInt();
        downloads_monthly_count = in.readInt();
        share_url = in.readString();
        data_url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(description);
        dest.writeValue(license);
        if (tags == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(tags);
        }
        dest.writeValue(user);

        dest.writeLong(regdate);
        dest.writeLong(pubdate);
        dest.writeString(title);
        dest.writeInt(uuid);
        if (images == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(images);
        }
        dest.writeInt(views_count);
        dest.writeInt(likes_count);
        dest.writeInt(comments_count);
        dest.writeInt(downloads_count);
        dest.writeInt(downloads_daily_count);
        dest.writeInt(downloads_weekly_count);
        dest.writeInt(downloads_monthly_count);
        dest.writeString(share_url);
        dest.writeString(data_url);
    }

    @SuppressWarnings("unused")
    public static final Creator<BgrPhotoItem> CREATOR = new Creator<BgrPhotoItem>() {
        @Override
        public BgrPhotoItem createFromParcel(Parcel in) {
            return new BgrPhotoItem(in);
        }

        @Override
        public BgrPhotoItem[] newArray(int size) {
            return new BgrPhotoItem[size];
        }
    };

}


