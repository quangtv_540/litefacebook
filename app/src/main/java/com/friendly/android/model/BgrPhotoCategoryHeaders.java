package com.friendly.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mrquang on 9/3/15.
 */
public class BgrPhotoCategoryHeaders implements Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("color")
    private String color;
    @SerializedName("backgrounds_count")
    private int backgrounds_count;
    @SerializedName("summary")
    private String summary;
    @SerializedName("background")
    private BgrBackground background;
    @SerializedName("type")
    private String type;
    @SerializedName("icon")
    private BgrCategoryItem.Icon icon;

    public BgrPhotoCategoryHeaders() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getBackgrounds_count() {
        return backgrounds_count;
    }

    public void setBackgrounds_count(int backgrounds_count) {
        this.backgrounds_count = backgrounds_count;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public BgrBackground getBackground() {
        return background;
    }

    public void setBackground(BgrBackground background) {
        this.background = background;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BgrCategoryItem.Icon getIcon() {
        return icon;
    }

    public void setIcon(BgrCategoryItem.Icon icon) {
        this.icon = icon;
    }

    protected BgrPhotoCategoryHeaders(Parcel in) {
        name = in.readString();
        color = in.readString();
        backgrounds_count = in.readInt();
        summary = in.readString();
        background = (BgrBackground) in.readValue(BgrBackground.class.getClassLoader());
        type = in.readString();
        icon = (BgrCategoryItem.Icon) in.readValue(BgrCategoryItem.Icon.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(color);
        dest.writeInt(backgrounds_count);
        dest.writeString(summary);
        dest.writeValue(background);
        dest.writeString(type);
        dest.writeValue(icon);
    }

    @SuppressWarnings("unused")
    public static final Creator<BgrPhotoCategoryHeaders> CREATOR = new Creator<BgrPhotoCategoryHeaders>() {
        @Override
        public BgrPhotoCategoryHeaders createFromParcel(Parcel in) {
            return new BgrPhotoCategoryHeaders(in);
        }

        @Override
        public BgrPhotoCategoryHeaders[] newArray(int size) {
            return new BgrPhotoCategoryHeaders[size];
        }
    };
}
