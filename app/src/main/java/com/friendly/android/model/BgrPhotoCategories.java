package com.friendly.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mrquang on 9/3/15.
 */
public class BgrPhotoCategories {

    @SerializedName("headers")
    private List<BgrPhotoCategoryHeaders> bgrHeaderses;
    @SerializedName("data")
    private List<BgrPhotoItem> bgrPhotoItems;
    @SerializedName("type")
    private String type;
    @SerializedName("next")
    private String next;

    public BgrPhotoCategories() {
        super();
    }

    public List<BgrPhotoCategoryHeaders> getBgrHeaderses() {
        return bgrHeaderses;
    }

    public void setBgrHeaderses(List<BgrPhotoCategoryHeaders> bgrHeaderses) {
        this.bgrHeaderses = bgrHeaderses;
    }

    public List<BgrPhotoItem> getBgrPhotoItems() {
        return bgrPhotoItems;
    }

    public void setBgrPhotoItems(List<BgrPhotoItem> bgrPhotoItems) {
        this.bgrPhotoItems = bgrPhotoItems;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
