package com.friendly.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mrquang on 9/3/15.
 */
public class BgrBackground implements Parcelable {

    @SerializedName("url")
    private String url;

    public BgrBackground() {
        super();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    protected BgrBackground(Parcel in) {
        url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
    }

    @SuppressWarnings("unused")
    public static final Creator<BgrBackground> CREATOR = new Creator<BgrBackground>() {
        @Override
        public BgrBackground createFromParcel(Parcel in) {
            return new BgrBackground(in);
        }

        @Override
        public BgrBackground[] newArray(int size) {
            return new BgrBackground[size];
        }
    };
}
