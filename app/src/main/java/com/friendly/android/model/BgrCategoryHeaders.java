package com.friendly.android.model;//

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

//
//  Tripi
//
//  Created by Quang Tran on 2/8/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class BgrCategoryHeaders implements Parcelable {

    @SerializedName("width")
    private Integer width = null;
    @SerializedName("height")
    private Integer height = null;
    @SerializedName("background")
    private Ground background = null;
    @SerializedName("foreground")
    private Ground foreground = null;
    @SerializedName("color")
    private String color = null;
    @SerializedName("cells")
    private List<Cells> cells = null;
    @SerializedName("type")
    private String type = null;
    @SerializedName("condition")
    private String condition = null;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Ground getBackground() {
        return background;
    }

    public void setBackground(Ground background) {
        this.background = background;
    }

    public Ground getForeground() {
        return foreground;
    }

    public void setForeground(Ground foreground) {
        this.foreground = foreground;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Cells> getCells() {
        return cells;
    }

    public void setCells(List<Cells> cells) {
        this.cells = cells;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public static class Ground implements Parcelable {

        @SerializedName("url")
        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        protected Ground(Parcel in) {
            url = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(url);
        }

        @SuppressWarnings("unused")
        public static final Creator<Ground> CREATOR = new Creator<Ground>() {
            @Override
            public Ground createFromParcel(Parcel in) {
                return new Ground(in);
            }

            @Override
            public Ground[] newArray(int size) {
                return new Ground[size];
            }
        };
    }

    public static class Cells implements Parcelable {

        @SerializedName("uri")
        private String uri;
        @SerializedName("rect")
        private String rect;

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public String getRect() {
            return rect;
        }

        public void setRect(String rect) {
            this.rect = rect;
        }

        protected Cells(Parcel in) {
            uri = in.readString();
            rect = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(uri);
            dest.writeString(rect);
        }

        @SuppressWarnings("unused")
        public static final Creator<Cells> CREATOR = new Creator<Cells>() {
            @Override
            public Cells createFromParcel(Parcel in) {
                return new Cells(in);
            }

            @Override
            public Cells[] newArray(int size) {
                return new Cells[size];
            }
        };
    }

    BgrCategoryHeaders(Parcel in) {
        this.width = in.readInt();
        this.height = in.readInt();
        this.color = in.readString();
        this.condition = in.readString();
        this.type = in.readString();
        background = (Ground) in.readValue(Ground.class.getClassLoader());
        foreground = (Ground) in.readValue(Ground.class.getClassLoader());
        in.readTypedList(cells, Cells.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeString(color);
        dest.writeString(type);
        dest.writeString(condition);
        dest.writeValue(background);
        dest.writeValue(foreground);
        dest.writeTypedList(cells);
    }

    public static final Creator<BgrCategoryHeaders> CREATOR = new Creator<BgrCategoryHeaders>() {
        @Override
        public BgrCategoryHeaders[] newArray(int size) {
            return new BgrCategoryHeaders[size];
        }

        @Override
        public BgrCategoryHeaders createFromParcel(Parcel source) {
            return new BgrCategoryHeaders(source);
        }
    };
}
