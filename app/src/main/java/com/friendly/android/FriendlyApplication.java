package com.friendly.android;//

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.friendly.android.db.DatabaseInit;
import com.friendly.android.manager.Theme;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.utils.AppDevices;
import com.friendly.android.utils.Utils;

import java.util.HashMap;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class FriendlyApplication extends Application {

    private static Context context;
    public static boolean onRunning;
    public static boolean isShowAds = true;
    public static HashMap<String, Object> resourcesCache;

    public static boolean isTablet;

    @Override
    public void onCreate() {
        context = getApplicationContext();
        super.onCreate();

        AppDevices.initDeviceValues(this);
        AppPreferences.load(this);
        DatabaseInit.init(this);
        FacebookSdk.sdkInitialize(this);
        Theme.init(this);
        Utils.setContext(this);
        resourcesCache = new HashMap<>();

        isTablet = !AppDevices.isPhone(this);
    }

    public static Context getContext() {
        return context;
    }
}
