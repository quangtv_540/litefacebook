package com.friendly.android.views;


//
// Social Trending News Project
//
//
// Created by Quang Tran on 9/29/16.
// Copyright (C) 2015 Crowddee, Inc. All rights reserved.
//

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.manager.Theme;

public class TypefaceTextView extends TextView {

    public TypefaceTextView(Context context) {
        super(context);
    }

    public TypefaceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TypefaceTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TypefaceTextView);
        String typeFonts = typedArray.getString(R.styleable.TypefaceTextView_typeface);
        if (typeFonts == null || typeFonts.isEmpty()) {
            typeFonts = Theme.Font.SF_REGULAR;
        } else {
            typeFonts = String.format("fonts/%s", typeFonts);
        }
        if (!isInEditMode()) {
            setTypeface(Theme.INSTANCE.getTypeface(typeFonts));
        }
        typedArray.recycle();
    }
}
