package com.friendly.android.views;//

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.ImageView;

//
//  Tripi
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class AutoHighlightSelectedImageView extends ImageView {

    public AutoHighlightSelectedImageView(Context context) {
        super(context);
    }

    public AutoHighlightSelectedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setColorFilter(0xFFC7C7C7, PorterDuff.Mode.SRC_ATOP); //0xFFA5A7AA
    }

    public AutoHighlightSelectedImageView(Context context, AttributeSet attrs,
                                          int defStyle) {
        super(context, attrs, defStyle);
        setColorFilter(0xFFC7C7C7, PorterDuff.Mode.SRC_ATOP);
    }


    @Override
    public void setSelected(boolean selected) {
        if (isEnabled()) {
            if (selected) {
                setColorFilter(0xFF4CAF50, PorterDuff.Mode.SRC_ATOP); //0xFFFFFFFF
            } else {
//                clearColorFilter();
                setColorFilter(0xFFC7C7C7, PorterDuff.Mode.SRC_ATOP);
            }
        }
        super.setSelected(selected);
    }
}
