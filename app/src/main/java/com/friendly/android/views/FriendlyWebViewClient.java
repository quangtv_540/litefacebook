package com.friendly.android.views;//

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.friendly.android.FriendlyApplication;
import com.friendly.android.ui.FacebookPhotoViewerActivity;
import com.friendly.android.ui.HomeActivity;
import com.friendly.android.utils.Connectivity;

//
//  Tripi
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class FriendlyWebViewClient extends WebViewClient {

    public static String currentlyLoadedPage;
    private static long lastSavingTime = System.currentTimeMillis();
    public static boolean wasOffline;

    private boolean refreshed;

    private static Context context = FriendlyApplication.getContext();

    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if ((url.contains("market://") || url.contains("mailto:")
                || url.contains("play.google") || url.contains("tel:") || url.contains("youtube") || url
                .contains("vid:"))) {
            view.getContext().startActivity(
                    new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return true;

        }
        if (url.contains("scontent") && url.contains("jpg")) {
            if (url.contains("l.php?u=")) {
                return false;
            }
            Intent photoViewer = new Intent(HomeActivity.context, FacebookPhotoViewerActivity.class);
            photoViewer.putExtra("url", url);
            photoViewer.putExtra("title", view.getTitle());
            view.getContext().startActivity(photoViewer);
            return true;

        } else if (Uri.parse(url).getHost().endsWith("facebook.com")
                || Uri.parse(url).getHost().endsWith("m.facebook.com")
                || Uri.parse(url).getHost().endsWith("mobile.facebook.com")
                || Uri.parse(url).getHost().endsWith("mobile.facebook.com/messages")
                || Uri.parse(url).getHost().endsWith("m.facebook.com/messages")
                || Uri.parse(url).getHost().endsWith("h.facebook.com")
                || Uri.parse(url).getHost().endsWith("l.facebook.com")
                || Uri.parse(url).getHost().endsWith("0.facebook.com")
                || Uri.parse(url).getHost().endsWith("zero.facebook.com")
                || Uri.parse(url).getHost().endsWith("fbcdn.net")
                || Uri.parse(url).getHost().endsWith("akamaihd.net")
                || Uri.parse(url).getHost().endsWith("fb.me")
                || Uri.parse(url).getHost().endsWith("googleusercontent.com")) {
            return false;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (Exception e) {
            Log.e("URL", "Exception: " + e.getMessage());
        }
        return true;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        view.stopLoading();  // may not be needed\
        if(!Connectivity.isConnected(view.getContext()))
            view.loadUrl("file:///android_asset/error.html");
        else view.reload();
    }

    @Override
    public void onPageFinished(WebView view, String url) {

    }
}
