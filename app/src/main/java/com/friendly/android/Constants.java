package com.friendly.android;//

//  
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class Constants {

    public static final String TAB_FEEDS = "tab.FEEDS";
    public static final String TAB_INBOX = "tab.INBOX";
    public static final String TAB_INSTA = "tab.INSTA";
    public static final String TAB_TWITTER = "tab.TWITTER";
    public static final String TAB_WALLPAPERS = "tab.WALLPAPERS";

    public static final String USER_AGENT_DESKTOP = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) " +
            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";

    public static final String USER_AGENT_MOBILE = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B411 Safari/600.1.4";

    public static final String U_A_CH = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
    public static final String U_A_C_O = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36";
    public static final String U_A_FX = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0";

    public static final String MESSENGER = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) " +
            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";
    public static final String TWITTER = "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Mobile Safari/537.36";

    public static final String ADMOB_INTERSTITIALS = "";
    public static final String ADMOB_BANNER = "";
    public static final String GOOGLE_ANALYTICS_KEY = "";
}
