package com.friendly.android.utils;


import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;

//
//  AppDevices
//  Lite
//
//  Created by Quang Tran on 12/10/15.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class AppDevices {

    private static int		deviceWidth;
    private static int		deviceHeight;
    private static int		deviceDPI;
    private static float	deviceDensity;
    private static float	deviceXdpi;
    private static float	deviceYdpi;
    private static boolean 	valuesInitialized	= false;
    private static int		orientation;

    public static final int DEVICE_N = 1;
    public static final int DEVICE_H = 2;
    public static final int DEVICE_XH = 3;
    public static final int DEVICE_XXH = 4;
    public static final int DEVICE_XXXH = 5;

    public static void initDeviceValues(Context context) {
        initDeviceValues(context, false);
    }

    public static void initDeviceValues(Context context, boolean  reset) {
        if (reset == true || valuesInitialized == false) {
            final DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();

            deviceWidth = displaymetrics.widthPixels;
            deviceHeight = displaymetrics.heightPixels;
            deviceDPI = displaymetrics.densityDpi;
            deviceDensity = displaymetrics.density;
            deviceXdpi = displaymetrics.xdpi;
            deviceYdpi = displaymetrics.ydpi;

            orientation = context.getResources().getConfiguration().orientation;

            valuesInitialized = true;
        }
    }

    public static int getScreenType() {
        if (deviceDPI >= DisplayMetrics.DENSITY_XXXHIGH) {
            return DEVICE_XXXH;
        } else if (deviceDPI >= DisplayMetrics.DENSITY_XXHIGH) {
            return DEVICE_XXH;
        } else if (deviceDPI >= DisplayMetrics.DENSITY_XHIGH) {
            return DEVICE_XH;
        } else if (deviceDPI >= DisplayMetrics.DENSITY_HIGH) {
            return DEVICE_H;
        } else {
            return DEVICE_N;
        }
    }

    public static int getDeviceWidth() {
        return deviceWidth;
    }

    public static int getDeviceHeight() {
        return deviceHeight;
    }

    /**
     * The dpi type of screen : DisplayMetrics.DENSITY_LOW, MEDIUM or HIGH
     */
    public static int getDeviceDPI() {
        return deviceDPI;
    }

    /**
     * The dpi factor of the screen (1.0 for a mdpi screen, 1.5 for hdpi, 2 for
     * xhdpi, ...)
     */
    public static float getDeviceDensity() {
        return deviceDensity;
    }

    public static float getDeviceXdpi() {
        return deviceXdpi;
    }

    public static float getDeviceYdpi() {
        return deviceYdpi;
    }

    public static boolean  isPortraitMode() {
        return orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    public static boolean  isLandscapeMode() {
        return orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static int dp(int value) {
        return (int)(Math.max(1, deviceDensity * value));
    }
    public static int dpf(float value) {
        return (int)Math.ceil(deviceDensity * value);
    }

    public static int dpToPx(Context context, int dp) {
        int px = Math.round(dp * getPixelScaleFactor(context));
        return px;
    }

    public static int pxToDp(Context context, int px) {
        int dp = Math.round(px / getPixelScaleFactor(context));
        return dp;
    }

    private static float getPixelScaleFactor(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * Get device UUID
     * @param context
     * @return
     */
    public synchronized static String getDeviceId(Context context) {
        String deviceIMEI = ""; //AppPreferences.INSTANCE.getDeviceId();
//        if (TextUtils.isEmpty(deviceIMEI)) {
//            deviceIMEI = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//            if ((deviceIMEI == null) || TextUtils.isEmpty(deviceIMEI) || "9774d56d682e549c".equals(deviceIMEI)) {
//                deviceIMEI = UUID.randomUUID().toString();
//            }
////            AppPreferences.INSTANCE.setDeviceId(deviceIMEI);
//        }
        return deviceIMEI;
    }

    /**
     * Retrieves physical screen size of device.
     *
     * @param context
     * @return
     */
    public static double getPhysicScreenSize(Context context) {
        double size = 0;
        try {
            DisplayMetrics dm = context.getResources().getDisplayMetrics();
            @SuppressWarnings("unused")
            int statusBarHeight = 0; // device screen included this value
            int resourceId = context.getResources().getIdentifier(
                    "status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = context.getResources().getDimensionPixelSize(
                        resourceId);
            }
            int navigationBarHeight = 0;
            resourceId = context.getResources().getIdentifier(
                    "navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                navigationBarHeight = context.getResources()
                        .getDimensionPixelSize(resourceId);
            }

            float screenWidth = dm.widthPixels / dm.xdpi;
            float screenHeight = (dm.heightPixels + navigationBarHeight)
                    / dm.ydpi;
            size = Math.sqrt(Math.pow(screenWidth, 2)
                    + Math.pow(screenHeight, 2));
        } catch (Throwable t) {
        }
        return size;
    }

    /**
     * @param context
     * @return
     */
    public static boolean isPhone(Context context) {
        double size = getPhysicScreenSize(context);
        // this value is real size
        return size <= 6.5 ? true : false;
    }
}
