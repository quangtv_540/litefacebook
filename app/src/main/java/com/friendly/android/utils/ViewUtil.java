/*
 * Copyright (C) 2012 www.amsoft.cn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.friendly.android.utils;

import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.friendly.android.R;


// TODO: Auto-generated Javadoc


public class ViewUtil {

	public static final int INVALID = Integer.MIN_VALUE;

    public static float applyDimension(int unit, float value,
                                       DisplayMetrics metrics){
        switch (unit) {
        case TypedValue.COMPLEX_UNIT_PX:
            return value;
        case TypedValue.COMPLEX_UNIT_DIP:
            return value * metrics.density;
        case TypedValue.COMPLEX_UNIT_SP:
            return value * metrics.scaledDensity;
        case TypedValue.COMPLEX_UNIT_PT:
            return value * metrics.xdpi * (1.0f/72);
        case TypedValue.COMPLEX_UNIT_IN:
            return value * metrics.xdpi;
        case TypedValue.COMPLEX_UNIT_MM:
            return value * metrics.xdpi * (1.0f/25.4f);
        }
        return 0;
    }


	public static void measureView(View view) {
		ViewGroup.LayoutParams p = view.getLayoutParams();
		if (p == null) {
			p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}

		int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
		int lpHeight = p.height;
		int childHeightSpec;
		if (lpHeight > 0) {
			childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight,
					MeasureSpec.EXACTLY);
		} else {
			childHeightSpec = MeasureSpec.makeMeasureSpec(0,
					MeasureSpec.UNSPECIFIED);
		}
		view.measure(childWidthSpec, childHeightSpec);
	}

	public static int getViewWidth(View view) {
		measureView(view);
		return view.getMeasuredWidth();
	}


	public static int getViewHeight(View view) {
		measureView(view);
		return view.getMeasuredHeight();
	}

	public static void removeSelfFromParent(View v) {
		ViewParent parent = v.getParent();
		if (parent != null) {
			if (parent instanceof ViewGroup) {
				((ViewGroup) parent).removeView(v);
			}
		}
	}

    public static int getLicenseIcon(String type) {
        if (type.contains("copy")) {
            return R.drawable.license_copyright;
        } else if (type.contains("public")) {
            return R.drawable.license_public;
        } else if (type.equals("cc-sa")) {
            return R.drawable.license_cc_sa;
        } else if (type.equals("cc-nd")) {
            return R.drawable.license_cc_nd;
        } else if (type.equals("cc-nc")) {
            return R.drawable.license_cc_nc;
        } else {
            return R.drawable.license_cc_by;
        }
    }

    public static int getLicenseText(String type) {
        if (type.contains("copy")) {
            return R.string.license_all_rights_reservered;
        } else if (type.contains("public")) {
            return R.string.license_public_license;
        } else {
            return R.string.license_creative_license;
        }
    }
}
