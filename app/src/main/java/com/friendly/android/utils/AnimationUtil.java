package com.friendly.android.utils;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 12/5/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;

public class AnimationUtil {

    @SuppressLint("NewApi")
    public static void hide(final View view, boolean isDown) {
        view.setY(1);
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(view.getContext(), "alpha", 1f, 0f);
        alphaAnim.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
                view.clearAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        ObjectAnimator yAnim = ObjectAnimator.ofFloat(view, "translationY", 0, ((isDown) ? (-1) : 1) * view.getHeight());

        AnimatorSet anim = new AnimatorSet();
        anim.playTogether(alphaAnim, yAnim);
        anim.setDuration(300);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }

    @SuppressLint("NewApi")
    public static void show(final View view, final boolean isDown) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(1);
        if (view.getHeight() <= 0) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onGlobalLayout() {
                    if (SdkUtils.hasJellyBean()) {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                    internalShow(view, isDown);
                }
            });
        } else {
            internalShow(view, isDown);
        }
    }

    @SuppressLint("NewApi")
    private static void internalShow(final View view, boolean isDown) {
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(view.getContext(), "alpha", 0f, 1f);
        ObjectAnimator yAnim = ObjectAnimator.ofFloat(view, "translationY", ((isDown) ? (-1) : 1) * view.getHeight(), 0);
        AnimatorSet anim = new AnimatorSet();
        anim.playTogether(alphaAnim, yAnim);
        anim.setDuration(300);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }

    @SuppressLint("NewApi")
    public static void setAlpha(View view, float alpha) {
        if (Build.VERSION.SDK_INT < 11) {
            final AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
            animation.setDuration(0);
            animation.setFillAfter(true);
            view.startAnimation(animation);
        }
        else view.setAlpha(alpha);
    }

    public static void hideHorizontal(final View view, final boolean toLeft) {
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(view.getContext(), "alpha", 1f, 1f);
        alphaAnim.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        ObjectAnimator yAnim = ObjectAnimator.ofFloat(view, "translationX", 0, ((toLeft) ? 1 : (-1)) * view.getWidth());

        AnimatorSet anim = new AnimatorSet();
        anim.playTogether(alphaAnim, yAnim);
        anim.setDuration(400);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }

    public static void showHorizontal(final View view, final boolean toLeft) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(1);
        if (view.getWidth() <= 0) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onGlobalLayout() {
                    if (SdkUtils.hasJellyBean()) {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                    internalShowHorizontal(view, toLeft);
                }
            });
        } else {
            internalShowHorizontal(view, toLeft);
        }
    }

    @SuppressLint("NewApi")
    private static void internalShowHorizontal(final View view, boolean toLeft) {
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(view.getContext(), "alpha", 0f, 1f);
        ObjectAnimator yAnim = ObjectAnimator.ofFloat(view, "translationX", (toLeft ? 1 : (-1)) * view.getWidth(), 0);
        AnimatorSet anim = new AnimatorSet();
        anim.playTogether(alphaAnim, yAnim);
        anim.setDuration(400);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }
}
