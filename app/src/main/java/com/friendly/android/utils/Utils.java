package com.friendly.android.utils;//

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;

import com.friendly.android.FriendlyApplication;
import com.friendly.android.preferences.AppPreferences;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//
//  Lite
//
//  Created by Quang Tran on 9/29/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class Utils {

    public static Context applicationContext = null;

    private static String defaultNewsFeedCss;
    private static String defaultMessengerCss;
    private static String blackCss;
    private static String darkCss;
    private static String materialCss;
    private static String instagramCss;

    private static NumberFormat numberFormat = null;
    static {
        numberFormat = NumberFormat.getInstance(Locale.US);
        numberFormat.setMaximumIntegerDigits(15);
    }

    public static String formatDownloadCount(int downloadsCount) {
        return numberFormat.format(downloadsCount);
    }

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static Bundle decodeUrl(String s) {
        Bundle params = new Bundle();
        if (s != null) {
            String array[] = s.split("\\?");
            for (String parameter : array) {
                String v[] = parameter.split("=");
                if (v.length == 2) {
                    params.putString(URLDecoder.decode(v[0]),
                            URLDecoder.decode(v[1]));
                }
            }
        }
        return params;
    }

    public static void setContext(Context context) {
        Utils.applicationContext = context;
        init();
    }

    public static void init() {
        log();
    }

    public static final double REMOVED_DAYS = 5;
    public static void log () {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        FriendlyApplication.isShowAds = !(currentTime - AppPreferences.INSTANCE.getLastTimeShareInvite()
                < REMOVED_DAYS * 24 * 60 * 60 * 1000);

        try {
            defaultNewsFeedCss = convertStreamToString(applicationContext.getAssets().open("web/default_feeds.css"));
            defaultMessengerCss = convertStreamToString(applicationContext.getAssets().open("web/default_messages.css"));
            blackCss = convertStreamToString(applicationContext.getAssets().open("web/black.css"));
            darkCss = convertStreamToString(applicationContext.getAssets().open("web/dark.css"));
            materialCss = convertStreamToString(applicationContext.getAssets().open("web/materialize.css"));
            instagramCss = convertStreamToString(applicationContext.getAssets().open("web/instagram.css"));
        } catch (Exception e) {}
    }

    public static String getTheme(String themes) {
        String css = defaultNewsFeedCss;
        if (themes.equalsIgnoreCase("default_feeds.css")) {
            if (defaultNewsFeedCss != null) {
                css = defaultNewsFeedCss;
            } else {
                try {
                    css = convertStreamToString(applicationContext.getAssets().open("web/default_feeds.css"));
                } catch (Exception e) {}
            }
        } else if (themes.equalsIgnoreCase("default_messages.css")) {
            if (defaultMessengerCss != null) {
                css = defaultMessengerCss;
            } else {
                try {
                    css = convertStreamToString(applicationContext.getAssets().open("web/default_messages.css"));
                } catch (Exception e) {}
            }
        } else if (themes.equalsIgnoreCase("black.css")) {
            if (blackCss != null) {
                css = blackCss;
            } else {
                try {
                    css = convertStreamToString(applicationContext.getAssets().open("web/black.css"));
                } catch (Exception e) {}
            }
        } else if (themes.equalsIgnoreCase("dark.css")) {
            if (darkCss != null) {
                css = darkCss;
            } else {
                try {
                    css = convertStreamToString(applicationContext.getAssets().open("web/dark.css"));
                } catch (Exception e) {}
            }
        } else if (themes.equalsIgnoreCase("materialize.css")) {
            if (materialCss != null) {
                css = materialCss;
            } else {
                try {
                    css = convertStreamToString(applicationContext.getAssets().open("web/materialize.css"));
                } catch (Exception e) {}
            }
        } else if (themes.equalsIgnoreCase("instagram.css")) {
            if (instagramCss != null) {
                css = instagramCss;
            } else {
                try {
                    css = convertStreamToString(applicationContext.getAssets().open("web/instagram.css"));
                } catch (Exception e) {}
            }
        }
        return css;
    }


    // Prevents instantiation.
    private Utils() {
    }

    public static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static Bitmap getImage(URL url) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            int responseCode = connection.getResponseCode();
            //Log.e(LOG_TAG, convertStreamToString(connection.getInputStream()));
            if (responseCode == 200) {
                return BitmapFactory.decodeStream(connection.getInputStream());
            } else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static Bitmap getImage(String urlString) {
        try {
            URL url = new URL(urlString);
            return getImage(url);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getTextBetween(String original, String regexString) {
        Pattern pattern = Pattern.compile(regexString);
        // text contains the full text that you want to extract data
        Matcher matcher = pattern.matcher(original);

        while (matcher.find()) {
            String textInBetween = matcher.group(1); // Since (.*?) is capturing group 1
            return textInBetween;
        }
        return "";
    }

    public static boolean isDownloadManagerAvailable(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public static String injectCss(String fileCss, Context context) {
        try {
            InputStream inputStream = context.getAssets().open(fileCss);
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            return "javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('style');style.type = 'text/css';style.innerHTML = window.atob('" + Base64.encodeToString(buffer, 2) + "');" + "parent.appendChild(style)" + "})()";
        } catch (Exception d) {
            d.printStackTrace();
        }
        return null;
    }

    public static String injectCssString(String fileCss) {
        try {
            return "javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('style');style.type = 'text/css';style.innerHTML = window.atob('" + fileCss + "');" + "parent.appendChild(style)" + "})()";
        } catch (Exception d) {
            d.printStackTrace();
        }
        return null;
    }
}
