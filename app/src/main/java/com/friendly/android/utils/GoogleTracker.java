package com.friendly.android.utils;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 11/14/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.content.Context;

import com.friendly.android.Constants;
import com.friendly.android.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

public class GoogleTracker {

    public static GoogleTracker instance;

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     *
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p/>
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */

    static HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public GoogleTracker(Context context) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        mTrackers.put(TrackerName.APP_TRACKER, analytics.newTracker(Constants.GOOGLE_ANALYTICS_KEY));
        mTrackers.put(TrackerName.GLOBAL_TRACKER, analytics.newTracker(R.xml.global_tracker));
        mTrackers.put(TrackerName.ECOMMERCE_TRACKER, analytics.newTracker(R.xml.ecommerce_tracker));
    }

    public static GoogleTracker init(Context context) {
        if (instance == null) {
            instance = new GoogleTracker(context);
        }
        return instance;

    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        return mTrackers.get(trackerId);
    }
}
