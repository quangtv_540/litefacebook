package com.friendly.android.utils;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 11/14/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class TrackerScreen {

    /**
     * Check data before tracking
     *
     * @param value1
     * @return
     */
    private static boolean checkData(String value1) {
        return !TextUtils.isEmpty(value1);
    }

    /**
     * Tracking number of users view Asapchat
     *
     * @param lite
     */
    public static void openLiteScreen(Context context, String lite) {
        if (!checkData(lite)) {
            return;
        }
        String key = "View Lite: " + lite;
        // Get tracker.
        Tracker myTracker = GoogleTracker.init(context).getTracker(GoogleTracker.TrackerName.APP_TRACKER);
        myTracker.enableAdvertisingIdCollection(true);
        // Set screen name.
        myTracker.setScreenName(key);
        // Send a screen view.
        myTracker.send(new HitBuilders.AppViewBuilder().build());
    }
}
