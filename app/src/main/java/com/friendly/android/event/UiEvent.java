package com.friendly.android.event;//

//  
//  AsapChat
//
//  Created by Quang Tran on 4/21/2016.
//  Copyright (c) 2015 AsapChat, Inc. All rights reserved.
//
public class UiEvent extends BaseEvent {

    public static class JumpToTopEvent extends BaseEvent {

        public JumpToTopEvent() {
            super();
        }
    }

    public static class ReloadPageEvent extends BaseEvent {

        public ReloadPageEvent() {
            super();
        }
    }

    public static class CountMessageNotificationsEvent extends BaseEvent {

        public final int count;

        public CountMessageNotificationsEvent(int count) {
            super();
            this.count = count;
        }
    }

    public static class CountNotificationsEvent extends BaseEvent {

        public final int count;

        public CountNotificationsEvent(int count) {
            super();
            this.count = count;
        }
    }

    public static class UpdateThemeEvent extends BaseEvent {

        public final int themeIndex;

        public UpdateThemeEvent(int themeIndex) {
            super();
            this.themeIndex = themeIndex;
        }
    }

    public static class EnableCollapseToolbarEvent extends BaseEvent {

        public EnableCollapseToolbarEvent() {
            super();
        }
    }

    public static class ShowHideFABEvent extends BaseEvent {

        public ShowHideFABEvent() {
            super();
        }
    }

    public static class UpdateTextSizeEvent extends BaseEvent {

        public final String font;

        public UpdateTextSizeEvent(String font) {
            super();
            this.font = font;
        }
    }

    public static class QuitConfirmPINEvent extends BaseEvent {

        public QuitConfirmPINEvent() {
            super();
        }
    }

    public static class AllowCheckinLocationEvent extends BaseEvent {

        public AllowCheckinLocationEvent() {
            super();
        }
    }

    public static class BlockPhotoEvent extends BaseEvent {

        public BlockPhotoEvent() {
            super();
        }
    }

    public static class SectionUpdateEvent extends BaseEvent {

        public final int sectionIndex;

        public SectionUpdateEvent(int sectionIndex) {
            super();
            this.sectionIndex = sectionIndex;
        }
    }


    public static class ChangeBubbleColorEvent extends BaseEvent {

        public ChangeBubbleColorEvent() {
            super();
        }
    }

    public static class DownloadPhotoEvent extends BaseEvent {

        public final String url;

        public DownloadPhotoEvent(String url) {
            super();
            this.url = url;
        }
    }

    public static class SharePhotoEvent extends BaseEvent {

        public final String url;

        public SharePhotoEvent(String url) {
            super();
            this.url = url;
        }
    }

    public static class UpdateBadgeMessageCount extends BaseEvent {

        public final int numMessages;

        public UpdateBadgeMessageCount(int numMessages) {
            super();
            this.numMessages = numMessages;
        }
    }

    public static class SelectTabNewsFeedEvent extends BaseEvent {

        public SelectTabNewsFeedEvent() {
            super();
        }
    }

    public static class SelectTabMessageEvent extends BaseEvent {

        public SelectTabMessageEvent() {
            super();
        }
    }

    public static class HideTwitterTabEvent extends BaseEvent {

        public HideTwitterTabEvent() {
            super();
        }
    }
}
