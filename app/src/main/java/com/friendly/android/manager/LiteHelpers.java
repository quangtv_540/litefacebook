package com.friendly.android.manager;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 11/29/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.webkit.WebView;

public class LiteHelpers {


    public static final int BADGE_UPDATE_INTERVAL = 5000;

    public static boolean isInteger(String str) {
        return (str.matches("^-?\\d+$"));
    }

    public static void hideMenuBar(WebView view) {
        view.loadUrl("javascript:try{if(!document.URL.match('facebook\\.com\\/composer')){document.getElementById('page').style.top='-46px';android.isComposer(false)}else{android.isComposer(true)}}catch(e){}android.loadingCompleted();");
    }


    public static void updateNotificationsService(WebView view, int interval) {
        view.loadUrl("javascript:function notification_service(){adv.getNotifications(document.querySelector('#notifications_jewel > a > div > span[data-sigil=count]').innerHTML);setTimeout(notification_service, " + interval + ");}try{notification_service();}catch(e){}");
    }

    public static void updateNotifications(WebView view) {
        view.loadUrl("javascript:adv.getNotifications(document.querySelector('#notifications_jewel > a > div > span[data-sigil=count]').innerHTML);");
    }

    public static void updateMessagesService(WebView view, int interval) {
        view.loadUrl("javascript:function message_service(){adv.getMessages(document.querySelector('#messages_jewel > a > div > span[data-sigil=count]').innerHTML);setTimeout(message_service, " + interval + ");}try{message_service();}catch(e){}");
    }

    public static void updateMessages(WebView view) {
        view.loadUrl("javascript:adv.getMessages(document.querySelector('#messages_jewel > a > div > span[data-sigil=count]').innerHTML);");
    }

    public static void updateNums(WebView view) {
        view.loadUrl("javascript:(function()%7Bandroid.getNums(document.querySelector(%22%23notifications_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML%2Cdocument.querySelector(%22%23messages_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML%2Cdocument.querySelector(%22%23requests_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML)%7D).innerHTML%2Cdocument.querySelector(%22%23feed_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML)%7D)()");
    }

    public static void updateNumsService(WebView view) {
        view.loadUrl("javascript:(function()%7Bfunction%20n_s()%7Bandroid.getNums(document.querySelector(%22%23notifications_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML%2Cdocument.querySelector(%22%23messages_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML%2Cdocument.querySelector(%22%23requests_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML%2Cdocument.querySelector(%22%23feed_jewel%20%3E%20a%20%3E%20div%20%3E%20span%5Bdata-sigil%3Dcount%5D%22).innerHTML)%2CsetTimeout(n_s%2C" + BADGE_UPDATE_INTERVAL + ")%7Dtry%7Bn_s()%7Dcatch(_)%7B%7D%7D)()");
    }
}
