package com.friendly.android.manager;//

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public enum Theme {

    INSTANCE;

    public interface Font {
        public static final String SF_REGULAR = "fonts/SF-UI-Text-Regular.otf";
        public static final String SF_MEDIUM = "fonts/SF-UI-Text-Medium.otf";

    }

    Context mContext;
    Map<String, Typeface> mTypefaces;

    Map<String, String> mColorBubble;

    public static void init(Context context) {
        INSTANCE.mContext = context;
        INSTANCE.initColorBubble();
    }

    public Typeface getTypeface(String name) {
        if (mTypefaces == null) {
            mTypefaces = new HashMap<>();
        }
        Typeface typeface = mTypefaces.get(name);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(mContext.getAssets(), name);
        }
        if (typeface == null) {
            throw new IllegalArgumentException("No typeface with provided font name");
        }
        mTypefaces.put(name, typeface);
        return typeface;
    }

    void initColorBubble() {
        if (mColorBubble == null) {
            mColorBubble = new HashMap<>();
        }
        mColorBubble.put("#0781f8", "#0c81e8");
        mColorBubble.put("#f44336", "#e53935");
        mColorBubble.put("#e91e63", "#d81b60");
        mColorBubble.put("#9c27b0", "#8e24aa");
        mColorBubble.put("#673ab7", "#5e35b1");
        mColorBubble.put("#3f51b5", "#3949ab");
        mColorBubble.put("#2196f3", "#1e88e5");
        mColorBubble.put("#03a9f4", "#039be5");
        mColorBubble.put("#00bcd4", "#00acc1");
        mColorBubble.put("#009688", "#00897b");
        mColorBubble.put("#4caf50", "#43a047");
        mColorBubble.put("#8bc34a", "#7cb342");
        mColorBubble.put("#cddc39", "#c0ca33");
        mColorBubble.put("#ffeb3b", "#fdd835");
        mColorBubble.put("#ffc107", "#ffb300");
        mColorBubble.put("#ff9800", "#fb8c00");
        mColorBubble.put("#ff5722", "#f4511e");
        mColorBubble.put("#795548", "#6d4c41");
        mColorBubble.put("#9e9e9e", "#757575");
        mColorBubble.put("#607d8b", "#546e7a");

    }

    public String getColorBubbleBorder(String colorBubble) {
        if (!TextUtils.isEmpty(mColorBubble.get(colorBubble)))
            return mColorBubble.get(colorBubble);
        return "#0c81e8";
    }
}
