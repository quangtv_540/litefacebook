package com.friendly.android.manager;//

//  
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/29/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public interface PhotoClickListener {
    void onPhotoClick();
}
