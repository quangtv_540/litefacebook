package com.friendly.android.manager;//

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.friendly.android.R;
import com.friendly.android.ui.HomeActivity;
import com.friendly.android.views.FriendlyWebView;

import java.io.File;
import java.util.List;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class FriendlyListener  implements FriendlyWebView.Listener {

    private static final int ID_SAVE_IMAGE = 0;

    private static final int ID_SHARE_IMAGE = 1;
    private static SharedPreferences preferences;
    private final FriendlyWebView mWebView;
    private final HomeActivity fActivity;
    private final int mScrollThreshold;
    private final DownloadManager mDownloadManager;

    public FriendlyListener(HomeActivity activity, WebView view) {
        fActivity = activity;
        mWebView = (FriendlyWebView) view;
        preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        mScrollThreshold = activity.getResources().getDimensionPixelOffset(R.dimen.fab_scroll_threshold);
        mDownloadManager = (DownloadManager) fActivity.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
        List<String> uri_segments = Uri.parse(url).getPathSegments();
        if (!(uri_segments.size() > 0 && uri_segments.get(0).equals("composer"))) {

        }
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }

    @Override
    public void onScrollChange(int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
    }

    private Resources getResources() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu) {
        final WebView.HitTestResult result = mWebView.getHitTestResult();
        MenuItem.OnMenuItemClickListener handler = new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == ID_SAVE_IMAGE) {
                    // Save the image
                    Uri uri = Uri.parse(result.getExtra());
                    DownloadManager.Request request = new DownloadManager.Request(uri);

                    // Set the download directory
                    File downloads_dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    if (!downloads_dir.exists()) {
                        if (!downloads_dir.mkdirs()) {
                            return false;
                        }
                    }
                    File destinationFile = new File(downloads_dir, uri.getLastPathSegment());
                    request.setDestinationUri(Uri.fromFile(destinationFile));

                    // Make notification stay after download
                    request.setVisibleInDownloadsUi(true);
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                    // Start the download
                    mDownloadManager.enqueue(request);
                    return true;
                } else if (i == ID_SHARE_IMAGE) {
                    final Uri uri = Uri.parse(result.getExtra());
                    return true;
                }
                return false;
            }

        };

        if (result.getType() == WebView.HitTestResult.IMAGE_TYPE || result.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
            contextMenu.add(0, ID_SAVE_IMAGE, 0, R.string.context_save_image).setOnMenuItemClickListener(handler);
            contextMenu.add(0, ID_SHARE_IMAGE, 0, R.string.context_share_image).setOnMenuItemClickListener(handler);
        }
    }
}
