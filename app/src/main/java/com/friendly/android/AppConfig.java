package com.friendly.android;//

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class AppConfig {
	
    // Todo Messenger
    public static final String FB_MESSAGE_URL = "https://m.facebook.com/messages?stype=lo&refid=7&_rdr";
    public static final String FB_MESSAGE_REQUESTS_URL = "https://m.facebook.com/messages/?folder=pending&refid=11";
    public static final String FB_MESSAGE_FILTERED_URL = "https://m.facebook.com/messages/?folder=other&refid=11";
    public static final String FB_MESSAGE_ARCHIVED_URL = "https://m.facebook.com/messages/?folder=archived&refid=11";
    public static final String FB_MESSAGE_SPAM_URL = "https://m.facebook.com/messages/?folder=spam&refid=11";
    public static final String FB_MESSAGE_UNREAD = "https://m.facebook.com/messages/?folder=unread&refid=11";


    public static final String FB_FACEBOOK_URL = "https://m.facebook.com/";
    public static final String FB_SEARCH_URL = "https://m.facebook.com/search";
    public static final String FB_SEARCH_JS_URL = "javascript:try{document.querySelector('#search_jewel > a').click();}catch(e){window.location.href='" + AppConfig.FB_FACEBOOK_URL + "search';}";
    public static final String FB_NOTIFY_JS_URL = "javascript:try{document.querySelector('#notifications_jewel > a').click();}catch(e){window.location.href='" + AppConfig.FB_FACEBOOK_URL + "notifications.php?more';}";
    public static final String FB_NOTIFY_URL = "https://m.facebook.com/notifications.php?more";
    public static final String FB_FRIEND_URL = "https://m.facebook.com/friends/center/requests/";
    public static final String FB_FRIEND_JS_URL = "javascript:try{document.querySelector('#requests_jewel > a').click();}catch(e){window.location.href='" + AppConfig.FB_FACEBOOK_URL + "friends/center/requests/';}";
    public static final String FB_ONLINE_URL = "https://m.facebook.com/buddylist.php";
    public static final String FB_MOST_URL = "https://m.facebook.com/home.php?sk=h_chr&ref=bookmarks&app_id=608920319153834";
    public static final String FB_TOP_URL = "https://m.facebook.com/home.php?sk=h_nor&ref=bookmarks";
    public static final String FB_NOTE_URL = "https://m.facebook.com/notes";
    public static final String FB_POKE_URL = "https://m.facebook.com/pokes";
    public static final String FB_GROUP_URL = "https://m.facebook.com/groups/?category=membership";
    public static final String FB_PAGE_URL = "https://m.facebook.com/pages/launchpoint/?from=pages_nav_discover&ref=bookmarks";
    public static final String FB_EVENT_URL = "https://m.facebook.com/events";


    public static final String FB_APP_LINKS = "";
    public static final String FB_INVITE_PREV_IMG = "";
    public static final String URL_CHECK_WALLPAPERS = "";
}
