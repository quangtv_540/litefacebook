package com.friendly.android.db;//

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.friendly.android.R;


public enum DatabaseInit {

    DB;
    private static String TAG = DatabaseInit.class.getName();

    public static final String NAME = "lite_wallpapers.db";
    private static String PATH = "";
    public static final int VERSION = 1;

    private WallpaperDb mDbHelper;

    /**
     * Creates or preloads database on startup.
     *
     * @param context
     */
    public static void init(Context context) {
        if (DB.mDbHelper == null) {
            PATH = String.format(context.getResources().getString(R.string.path_database), context.getPackageName());
            DB.mDbHelper = new WallpaperDb(context);
            DB.mDbHelper.createDataBase();
            DB.mDbHelper.openDataBase();
            DatabaseManager.init(DB.mDbHelper);
        }
    }

    public static class WallpaperDb extends SQLiteOpenHelper {

        private SQLiteDatabase mSQlite;

        public WallpaperDb(Context context) {
            super(context, NAME, null, VERSION);
        }

        /**
         * Creates a empty database on the system and rewrites it with your own
         * database.
         * */
        public void createDataBase() {
            this.getReadableDatabase();
        }

        public void openDataBase() {
            close();
            // Open the database
            String myPath = PATH + NAME;
            mSQlite = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE);
        }

        @Override
        public synchronized void close() {
            if (mSQlite != null) {
                mSQlite.close();
                mSQlite = null;
            }
            super.close();
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.beginTransaction();
            try {
                db.execSQL(Schema.CREATE_HOTEL_TABLE);

                db.setTransactionSuccessful();
            } finally  {
                db.endTransaction();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Schema.FAVORITE_NAME_TABLE + ";");
            onCreate(db);
        }
    }
}
