package com.friendly.android.db;//

//  
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 3/2/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class Schema {

    private static String TAG = Schema.class.getName();

    // Table Recent Search Hotel
    public static String FAVORITE_NAME_TABLE = "wallpapers_favorites";
    public static String PHOTO_ID = "photo_id";
    public static String PHOTO_CONTENT = "photo_content";
    public static String UPDATED_DATE = "updatedDate";
    public static String CREATE_HOTEL_TABLE = "CREATE TABLE wallpapers_favorites (photo_id INTEGER PRIMARY KEY UNIQUE, photo_content TEXT, updatedDate REAL)";

}
