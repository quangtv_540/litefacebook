package com.friendly.android.db;//

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.friendly.android.model.BgrPhotoItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 3/2/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public enum DatabaseManager {

    DB;
    private static String TAG = DatabaseManager.class.getName();

    private DatabaseInit.WallpaperDb mDbHelper;

    /**
     * This must be called when application starts.
     */
    public static void init(DatabaseInit.WallpaperDb mDbHelper) {
        DB.mDbHelper = mDbHelper;
    }

    public SQLiteDatabase getReadableDatabase() {
        return mDbHelper.getReadableDatabase();
    }

    public SQLiteDatabase getWritableDatabase() {
        return mDbHelper.getWritableDatabase();
    }

    public void clearAndStopData() {
        mDbHelper.getWritableDatabase().delete(Schema.FAVORITE_NAME_TABLE, null, null);
    }


    public synchronized ArrayList<BgrPhotoItem> getPhotos() {
        ArrayList<BgrPhotoItem> bgrPhotoItemArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + Schema.FAVORITE_NAME_TABLE + " ORDER BY updatedDate DESC";
        SQLiteDatabase db = this.mDbHelper.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                BgrPhotoItem bgrPhotoItem = new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Schema.PHOTO_CONTENT)),
                        new TypeToken<BgrPhotoItem>(){}.getType());

                bgrPhotoItemArrayList.add(bgrPhotoItem);

                cursor.moveToNext();
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (cursor != null)
                cursor.close();
        }
        return bgrPhotoItemArrayList;
    }


    public void saveOrUpdatePhotos(BgrPhotoItem bgrPhotoItem) {
        if (isPhotos(bgrPhotoItem)) {
            deletePhotos(bgrPhotoItem);
            return;
        }
        String selection = Schema.PHOTO_ID + " = ?";
        String selectionArgs[] = new String[]{ String.valueOf(bgrPhotoItem.getId()) };
        Cursor result = null;
        try {
            result = this.mDbHelper.getReadableDatabase().query(Schema.FAVORITE_NAME_TABLE, null, selection, selectionArgs, null, null, null);

            ContentValues values = new ContentValues();

            values.put(Schema.PHOTO_CONTENT, new Gson().toJson(bgrPhotoItem));
            values.put(Schema.UPDATED_DATE, Calendar.getInstance().getTimeInMillis());
            if (result == null || result.getCount() == 0) {
                values.put(Schema.PHOTO_ID, bgrPhotoItem.getId());
                this.mDbHelper.getWritableDatabase().insert(Schema.FAVORITE_NAME_TABLE, null, values);
            } else {
                this.mDbHelper.getReadableDatabase().update(Schema.FAVORITE_NAME_TABLE, values, selection, selectionArgs);
            }
        } catch(Exception e){
        } finally {
            if (result != null)
                result.close();
        }
    }

    public boolean isPhotos(BgrPhotoItem bgrPhotoItem) {
        String selection = Schema.PHOTO_ID + " = ?";
        String selectionArgs[] = new String[]{ String.valueOf(bgrPhotoItem.getId()) };
        Cursor result = null;
        try {
            result = this.mDbHelper.getReadableDatabase().query(Schema.FAVORITE_NAME_TABLE, null, selection, selectionArgs, null, null, null);
            if (result == null || result.getCount() == 0) {
                return false;
            } else {
                return true;
            }
        } catch(Exception e){
        } finally {
            if (result != null)
                result.close();
        }
        return false;
    }

    public void deletePhotos(BgrPhotoItem bgrPhotoItem) {
        String selection = Schema.PHOTO_ID + " = ?";
        String selectionArgs[] = new String[]{ String.valueOf(bgrPhotoItem.getId()) };
        this.mDbHelper.getReadableDatabase().delete(Schema.FAVORITE_NAME_TABLE, selection, selectionArgs);
    }
}
