package com.friendly.android.ui;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 10/1/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.friendly.android.R;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.ui.fragment.TutorialFragment;
import com.friendly.android.utils.TrackerScreen;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.viewpager_tutorial)
    ViewPager pager;

    @BindView(R.id.skip_tutorial_btn)
    Button skipButton;

    @BindView(R.id.indicator1)
    ImageButton indicator1;
    @BindView(R.id.indicator2)
    ImageButton indicator2;
    @BindView(R.id.indicator3)
    ImageButton indicator3;

    @BindView(R.id.viewPagerCountDots)
    LinearLayout viewPagerCountDots;

    CallbackManager callbackManager;

    @BindView(R.id.login_button)
    LoginButton loginButton;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    CookieSyncManager mCookieSyncManager = null;

    public static final int[] textIds = {R.string.description1, R.string.description2, R.string.description3};
    public static final int[] titleIds = {R.string.title1, R.string.title2, R.string.title3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            boolean isExit = getIntent().getBooleanExtra("Exit", false);
            if (isExit) {
                finish();
            }
        }

        TrackerScreen.openLiteScreen(this, LoginActivity.class.getSimpleName());

        //com.litefbwrapper.android

        // Create a CookieSyncManager instance and keep a reference of it
        mCookieSyncManager = CookieSyncManager.createInstance(this);

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        String cookieString = cookieManager.getCookie("https://m.facebook.com");

        if (cookieString != null && !TextUtils.isEmpty(cookieString) && AccessToken.getCurrentAccessToken() != null
                && AccessToken.getCurrentAccessToken().getToken() != null
                && !TextUtils.isEmpty(AccessToken.getCurrentAccessToken().getToken())) {
            goHome();
            return;
        }

        callbackManager = CallbackManager.Factory.create();

        TutorialFragmentAdapter adapter = new TutorialFragmentAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    viewPagerCountDots.setVisibility(View.VISIBLE);
                    skipButton.setVisibility(View.VISIBLE);
                    loginButton.setVisibility(View.GONE);
                    indicator1.setImageResource(R.drawable.selected_dot);
                    indicator2.setImageResource(R.drawable.non_selected_dot);
                    indicator3.setImageResource(R.drawable.non_selected_dot);
                } else if (position == 1) {
                    viewPagerCountDots.setVisibility(View.VISIBLE);
                    skipButton.setVisibility(View.VISIBLE);
                    loginButton.setVisibility(View.GONE);
                    indicator1.setImageResource(R.drawable.non_selected_dot);
                    indicator2.setImageResource(R.drawable.selected_dot);
                    indicator3.setImageResource(R.drawable.non_selected_dot);
                } else if (position == 2) {
                    viewPagerCountDots.setVisibility(View.GONE);
                    skipButton.setVisibility(View.GONE);
                    loginButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        pager.setPageTransformer(false, new IntroPageTransformer());


        //getStartedButtn.setOnClickListener(listener);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(2);
            }
        });

        if (!AppPreferences.INSTANCE.isFist()) {
            pager.setCurrentItem(2);
        }

        loginButton.setReadPermissions(Arrays.asList("public_profile"));
        loginButton.setLoginBehavior(LoginBehavior.SUPPRESS_SSO);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                progressBar.setVisibility(View.VISIBLE);
                CookieManager manager = CookieManager.getInstance();
                manager.setAcceptCookie(true);

                String cookieString = manager.getCookie("https://m.facebook.com");

                manager.setCookie("https://m.facebook.com", cookieString);
                mCookieSyncManager.sync();

                AppPreferences.INSTANCE.setUserId(loginResult.getAccessToken().getUserId());
                AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                loadUserName(cookieString);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        goHome();
                    }
                }, 300);
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login Facebook to continue", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(LoginActivity.this, "Please check your Internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }

    protected void goHome () {
        AppPreferences.INSTANCE.setIsFirst(false);
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    void loadUserName(final String cookieString) {
        // get username or id
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Document doc = Jsoup.connect("https://www.facebook.com/settings")
                            .userAgent("")
                            .cookie("https://m.facebook.com", cookieString).get();
                    Element element = doc.select("div._1k67[data-click] > a._2s25[href]").first();

                    String href = element.attr("href");
                    String userName = href.split("\\?")[0].split("facebook.com/")[1];
                    if(userName != null && !userName.contains("profile.php"))
                        AppPreferences.INSTANCE.setUserName(userName);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public static class TutorialFragmentAdapter extends FragmentStatePagerAdapter {
        public TutorialFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TutorialFragment.instantiate(textIds[position], titleIds[position], position);
        }

        @Override
        public int getCount() {
            return textIds.length;
        }
    }

    public static class IntroPageTransformer implements ViewPager.PageTransformer {
        private View title1;
        private View title2;
        private View title3;
        private View description1;
        private View description2;
        private View description3;
        private View wifi;
        private View memory;
        private View hand;
        private View umbrella;
        private View roomDivider;
        private View newsfeed;
        private View share;
        private View videos;
        private View like;
        private View camera;
        private View messenger;
        private View events;
        private View friends;

        @Override
        public void transformPage(View page, float position) {

            // Get the page index from the tag. This makes
            // it possible to know which page index you're
            // currently transforming - and that can be used
            // to make some important performance improvements.
            int pagePosition = (int) page.getTag();

            // Here you can do all kinds of stuff, like get the
            // width of the page and perform calculations based
            // on how far the user has swiped the page.
            int pageWidth = page.getWidth();
            float pageWidthTimesPosition = pageWidth * position;
            float absPosition = Math.abs(position);
            //Log.e("transformPage", "pagePosition: " + pagePosition + " \t" + "position: " + position);

            // Now it's time for the effects
            if (position <= -1.0f || position >= 1.0f) {

                // The page is not visible. This is a good place to stop
                // any potential work / animations you may have running.

            } else if (position == 0.0f) {

                // The page is selected. This is a good time to reset Views
                // after animations as you can't always count on the PageTransformer
                // callbacks to match up perfectly.
                //if (pagePosition == 0) {
                title1 = null;
                description1 = null;
                wifi = null;
                memory = null;
                //} else if (pagePosition == 1) {
                title2 = null;
                description2 = null;
                umbrella = null;
                roomDivider = null;
                //} else if (pagePosition == 2) {
                title3 = null;
                description3 = null;
                newsfeed = null;
                share = null;
                videos = null;
                like = null;
                camera = null;
                messenger = null;
                events = null;
                friends = null;
                // }

            } else {

                // The page is currently being scrolled / swiped. This is
                // a good place to show animations that react to the user's
                // swiping as it provides a good user experience.

                // Let's start by animating the title.
                // We want it to fade as it scrolls out
                /*View title = page.findViewById(R.id.tutorial_title);
                if (title != null)
                title.setAlpha(1.0f - absPosition);*/

                // Now the description. We also want this one to
                // fade, but the animation should also slowly move
                // down and out of the screen
                /*View description = page.findViewById(R.id.text_view_tutorial);
                description.setTranslationY(-pageWidthTimesPosition / 2f);
                description.setAlpha(1.0f - absPosition);*/

                // Now, we want the image to move to the right,
                // i.e. in the opposite direction of the rest of the
                // content while fading out

                // We're attempting to create an effect for a View
                // specific to one of the pages in our ViewPager.
                // In other words, we need to check that we're on
                // the correct page and that the View in question
                // isn't null.
                if (pagePosition == 0) {

                    if (title1 == null)
                        title1 = page.findViewById(R.id.tutorial_title);
                    if (title1 != null)
                        title1.setAlpha(1.0f - absPosition);

                    if (description1 == null)
                        description1 = page.findViewById(R.id.text_view_tutorial);
                    description1.setTranslationY(-pageWidthTimesPosition / 2f);
                    description1.setAlpha(1.0f - absPosition);

                    if (hand == null)
                        hand = page.findViewById(R.id.imageView);
                    hand.setAlpha(1.0f - absPosition);
                    hand.setTranslationX(-pageWidthTimesPosition * 1.5f);

                    if (wifi == null)
                        wifi = page.findViewById(R.id.imageView3);
                    wifi.setAlpha(1.0f - absPosition);
                    wifi.setTranslationY(-pageWidthTimesPosition / 2f);
                    wifi.setTranslationX(pageWidthTimesPosition * 1.5f);

                    if (memory == null)
                        memory = page.findViewById(R.id.imageView2);
                    memory.setAlpha(1.0f - absPosition);
                    memory.setTranslationY(-pageWidthTimesPosition / 2f);
                    memory.setTranslationX(-pageWidthTimesPosition * 1.5f);
                }else if (pagePosition == 1) {
                    if (title2 == null)
                        title2 = page.findViewById(R.id.tutorial_title);
                    if (title2 != null)
                        title2.setAlpha(1.0f - absPosition);

                    if (description2 == null)
                        description2 = page.findViewById(R.id.text_view_tutorial);
                    description2.setTranslationY(-pageWidthTimesPosition / 2f);
                    description2.setAlpha(1.0f - absPosition);

                    if(umbrella == null)
                        umbrella = page.findViewById(R.id.imageView2);
                    umbrella.setTranslationY(pageWidthTimesPosition / 2f);
                    if(roomDivider == null)
                        roomDivider = page.findViewById(R.id.imageView1);
                    roomDivider.setTranslationY(-pageWidthTimesPosition / 2f);

                }else if (pagePosition == 2){
                    if (title3 == null)
                        title3 = page.findViewById(R.id.tutorial_title);
                    if (title3 != null)
                        title3.setAlpha(1.0f - absPosition);

                    if (description3 == null)
                        description3 = page.findViewById(R.id.text_view_tutorial);
                    description3.setTranslationY(-pageWidthTimesPosition / 2f);
                    description3.setAlpha(1.0f - absPosition);

                    if(newsfeed == null)
                        newsfeed = page.findViewById(R.id.imageView7);
                    newsfeed.setTranslationX(-pageWidthTimesPosition / 2f);

                    if(share == null)
                        share = page.findViewById(R.id.imageView12);
                    share.setTranslationX(-pageWidthTimesPosition / 2f);

                    if(videos == null)
                        videos = page.findViewById(R.id.imageView11);
                    videos.setTranslationX(-pageWidthTimesPosition / 2f);

                    if(like == null)
                        like = page.findViewById(R.id.imageView10);
                    like.setTranslationX(-pageWidthTimesPosition / 2f);

                    if(camera == null)
                        camera = page.findViewById(R.id.imageView8);
                    camera.setTranslationX(pageWidthTimesPosition / 2f);

                    if(messenger == null)
                        messenger = page.findViewById(R.id.imageView5);
                    messenger.setTranslationX(pageWidthTimesPosition / 2f);

                    if(events == null)
                        events = page.findViewById(R.id.tutorial3_event);
                    events.setTranslationX(pageWidthTimesPosition / 2f);

                    if(friends == null)
                        friends = page.findViewById(R.id.imageView9);
                    friends.setTranslationX(pageWidthTimesPosition / 2f);

                }

                // Finally, it can be useful to know the direction
                // of the user's swipe - if we're entering or exiting.
                // This is quite simple:
                if (position < 0) {
                    // Create your out animation here
                } else {
                    // Create your in animation here
                }
            }
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean isExit = intent.getBooleanExtra("Exit", true);
        if (isExit) {
            finish();
        }
    }
}
