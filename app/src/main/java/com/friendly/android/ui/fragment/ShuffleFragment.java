package com.friendly.android.ui.fragment;//

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.friendly.android.R;
import com.friendly.android.adapter.PopularPhotoAdapter;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.model.BgrPhotoRecents;
import com.friendly.android.service.BgrApi;
import com.friendly.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

//
//  Tripi
//
//  Created by Quang Tran on 2/10/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class ShuffleFragment extends Fragment {

    List<BgrPhotoItem> bgrPhotoItems;

    @BindView(R.id.recent)
    RecyclerView gridView;
    PopularPhotoAdapter qhdPhotoAdapter;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    boolean isLoading, isFinished;

    GridLayoutManager gridLayoutManager;
    int visibleItemCount, totalItemCount, pastVisiblesItems, maxInvisibleItems = 8;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recent, container,false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (bgrPhotoItems == null) {
            bgrPhotoItems = new ArrayList<>();
        }

        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        gridView.setLayoutManager(gridLayoutManager);

        gridView.setHasFixedSize(true);
        gridView.setItemAnimator(new DefaultItemAnimator());

        qhdPhotoAdapter = new PopularPhotoAdapter(bgrPhotoItems, getActivity());
        gridView.setAdapter(qhdPhotoAdapter);
        gridView.addOnScrollListener(
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (isLoading || isFinished || totalItemCount == 0)
                            return;
                        visibleItemCount = gridLayoutManager.getChildCount();
                        pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                        if (visibleItemCount + pastVisiblesItems + maxInvisibleItems > totalItemCount) {
                            if (!Utils.isInternetAvailable(getActivity())){
                                Snackbar.make(getView(), "Please check your internet connection.", Snackbar.LENGTH_SHORT);
                                isLoading = false;
                                return;
                            }
                            new LoadListPhotos(false).execute();
                        }
                    }
                });


        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Utils.isInternetAvailable(getActivity())){
                    Snackbar.make(getView(), "Refresh failed! Please check your internet connection.", Snackbar.LENGTH_SHORT);
                    return;
                }

                new LoadListPhotos(true).execute();
            }
        });

        if (bgrPhotoItems.isEmpty())
            new LoadListPhotos(true).execute();
    }

    class LoadListPhotos extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        boolean isStarted;

        public LoadListPhotos(boolean isStarted) {
            this.isStarted = isStarted;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
            if (isStarted)
                mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoRecents> bgrPhotosCall = BgrApi.getBgrService().listShufflePhotos();
                if (bgrPhotosCall != null) {
                    BgrPhotoRecents bgrPhotos  = bgrPhotosCall.execute().body();
                    if (bgrPhotos != null) {
                        if (bgrPhotos.getBgrPhotoItems() != null && !bgrPhotos.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotos.getBgrPhotoItems();
                        }
                    }

                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            isLoading = false;
            if (params != null && !params.isEmpty()) {
                Log.e("BGR", "Size: " + params.size());
                if (isStarted)
                    bgrPhotoItems.clear();
                bgrPhotoItems.addAll(params);
                qhdPhotoAdapter.notifyDataSetChanged();
            } else if (!Utils.isInternetAvailable(getActivity())) {
                Snackbar.make(getView(), "Please check your internet connection.", Snackbar.LENGTH_SHORT);
            }
            totalItemCount = bgrPhotoItems.size();
            mSwipeRefreshLayout.setRefreshing(false);
            super.onPostExecute(params);
        }
    }
}
