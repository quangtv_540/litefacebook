package com.friendly.android.ui.fragment;//

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.friendly.android.Constants;
import com.friendly.android.R;
import com.friendly.android.event.UiEvent;
import com.friendly.android.manager.FriendlyListener;
import com.friendly.android.model.InstaPhotos;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.ui.GridPhotoActivity;
import com.friendly.android.ui.HomeActivity;
import com.friendly.android.ui.InstagramPhotoViewerActivity;
import com.friendly.android.ui.PagerPhotoViewerActivity;
import com.friendly.android.utils.Connectivity;
import com.friendly.android.utils.TrackerScreen;
import com.friendly.android.utils.Utils;
import com.friendly.android.views.FriendlyWebView;
import com.friendly.android.views.InstagramWebView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class InstagramFragment extends BaseFragment implements HomeActivity.HeaderControl {

    HomeActivity mActivity;

    private static final int REQUEST_STORAGE = 1;
    private static final int FILECHOOSER_RESULTCODE = 1;
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.friendlyWebView)
    FriendlyWebView webView;

    Toolbar toolbar;

    @Override
    public void onBackPressed() {
        if (webView.canGoBack() && Connectivity.isConnected(mActivity)) {
            webView.goBack();
            return;
        }
        mActivity.popFragments(true);
    }

    public InstagramFragment() {

    }

    @Override
    public void onHomeHeaderUpdate(Toolbar toolbar) {
        super.onHomeHeaderUpdate(toolbar);
        toolbar.setBackgroundResource(R.color.colorPrimary);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {
                    webView.goBack();
                    return;
                }
                mActivity.onBackPressed();
            }
        });
        this.toolbar = toolbar;
        toolbar.setTitle(R.string.instagram);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_instagram, null);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mActivity = (HomeActivity) getActivity();

        TrackerScreen.openLiteScreen(mActivity, "InstagramFragment");

        setups();

        webViewSettings();
    }

    void setups() {
        swipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });
    }

    void webViewSettings() {
        String webViewUrl = "https://www.instagram.com/accounts/login/";

        webView.getSettings().setUserAgentString(Constants.MESSENGER);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.setVerticalScrollBarEnabled(false);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.NORMAL);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.setListener(mActivity, new FriendlyListener(mActivity, webView));
        webView.addJavascriptInterface(new WebViewJavaScriptInterface(mActivity), "adv");

        setupTextSize(AppPreferences.INSTANCE.getFont());

        if (AppPreferences.INSTANCE.isBlockPhotoInstagram()) {
            webView.getSettings().setLoadsImagesAutomatically(false);
        }

        webView.loadUrl(webViewUrl);

        setupWebViewClient();

        setupWebChromeClient();
    }

    int fileCss = 0;
    void setupWebViewClient() {
        webView.setWebViewClient(new InstagramWebView() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("https://www.instagram.com")) {
                    return false;
                }
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try {
                    view.getContext().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e("shouldOverrideUrlLoad", "" + e.getMessage());
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                fileCss = 0;
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                if (view.canGoBack()) {
                    toolbar.setNavigationIcon(R.drawable.nav_back);
                } else {
                    toolbar.setNavigationIcon(null);
                }

                if (++fileCss == 1 || (url != null && url.contains("instagram.com/query"))) {
                    setupListPhotos();
                }

                if (!TextUtils.isEmpty(url) && url.contains("?__a=1")) {
                    new OpenMediaAsyncTask().execute(url);
                }

                mActivity.supportInvalidateOptionsMenu();
            }

            @Override
            public void onPageFinished(WebView view, String url){
                super.onPageFinished(view, url);
                try {
                    String css = Utils.convertStreamToString(mActivity.getAssets().open("web/instagram.css"));
                    view.loadUrl("javascript:function addStyleString(str) { var node = document.createElement('style'); " +
                            "node.innerHTML = str; document.body.appendChild(node); } addStyleString('" + css + "');");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void setupWebChromeClient() {
        webView.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 80) {
                    swipeRefreshLayout.setRefreshing(true);
                }
                if (progress >= 80) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin,
                                                           GeolocationPermissions.Callback callback) {
                super.onGeolocationPermissionsShowPrompt(origin, callback);
                callback.invoke(origin, true, false);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (!TextUtils.isEmpty(title) && getActivity() != null &&
                        !title.contains("https://") && !title.contains("http://")) {
                    toolbar.setTitle("Instagram");
                } else {
                    toolbar.setTitle(title);
                }
            }

            public boolean onShowFileChooser(
                    WebView webView, ValueCallback<Uri[]> filePathCallback,
                    FileChooserParams fileChooserParams) {
                if (mFilePathCallback != null) {
                    mFilePathCallback.onReceiveValue(null);
                }
                mFilePathCallback = filePathCallback;

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {

                    // create the file where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        Log.e("Instagram", "Unable to create Image File", ex);
                    }

                    // continue only if the file was successfully created
                    if (photoFile != null) {
                        mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }

                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("image/*");

                Intent[] intentArray;
                if (takePictureIntent != null) {
                    intentArray = new Intent[]{takePictureIntent};
                } else {
                    intentArray = new Intent[0];
                }

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, getString(R.string.image_chooser));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

                startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);

                return true;
            }

            // creating image files (Lollipop only)
            private File createImageFile() throws IOException {

                File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Folio");

                if (!imageStorageDir.exists()) {
                    //noinspection ResultOfMethodCallIgnored
                    imageStorageDir.mkdirs();
                }

                // create an image file name
                imageStorageDir  = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                return imageStorageDir;
            }

            // openFileChooser for Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;

                try {
                    File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Folio");

                    if (!imageStorageDir.exists()) {
                        //noinspection ResultOfMethodCallIgnored
                        imageStorageDir.mkdirs();
                    }

                    File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");

                    mCapturedImageURI = Uri.fromFile(file); // save to the private variable

                    final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
                    // captureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType("image/*");

                    Intent chooserIntent = Intent.createChooser(i, getString(R.string.image_chooser));
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{captureIntent});

                    startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
                } catch (Exception e) {
//                    Snackbar.make( drawerLayout, "Camera Exception:" + e, Snackbar.LENGTH_SHORT).show();
                }

            }

            // not needed but let's make it overloaded just in case
            // openFileChooser for Android < 3.0
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                openFileChooser(uploadMsg, "");
            }

            // openFileChooser for other Android versions
            /** may not work on KitKat due to lack of implementation of openFileChooser() or onShowFileChooser()
             *  https://code.google.com/p/android/issues/detail?id=62220
             *  however newer versions of KitKat fixed it on some devices */
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                openFileChooser(uploadMsg, acceptType);
            }
        });
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_instagram, menu);
        MenuItem item = menu.findItem(R.id.menu_block_photo);
        item.setChecked(AppPreferences.INSTANCE.isBlockPhotoInstagram());

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu) {
        try {
            menu.findItem(R.id.menu_grid).setVisible(
                    (instaPhotos == null || instaPhotos.isEmpty()) ? false : true);

            MenuItem item = menu.findItem(R.id.menu_block_photo);
            item.setChecked(AppPreferences.INSTANCE.isBlockPhotoInstagram());
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_grid:
                GridPhotoActivity.instaPhotoses = instaPhotos;
                Intent intent = new Intent(mActivity, GridPhotoActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_block_photo:
                AppPreferences.INSTANCE.setBlockPhotoInstagram(!item.isChecked());
                item.setChecked(!item.isChecked());
                blockPhotos();
                mActivity.supportInvalidateOptionsMenu();
                return true;
            case R.id.menu_remove_ads:
                mActivity.showRemoveAdsOptions(false);
                return true;
            case R.id.menu_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("unused")
    private boolean requestStoragePermission() {
        String storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int hasPermission = ContextCompat.checkSelfPermission(mActivity, storagePermission);
        String[] permissions = new String[] { storagePermission };
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(permissions, REQUEST_STORAGE);
            return false;
        } else {
            return true;
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
//                    Snackbar noPermission = Snackbar .make(drawerLayout, getString(R.string.permission_not_granted, Snackbar.LENGTH_SHORT), Snackbar.LENGTH_SHORT);
//                    noPermission.show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == this.mUploadMessage)
                    return;
                Uri result = null;
                try {
                    if (resultCode != Activity.RESULT_OK)
                        result = null;
                    else {
                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                }
                catch(Exception e) {
                    Toast.makeText(mActivity, "activity :"+e, Toast.LENGTH_LONG).show();
                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }

        } // end of code for all versions except of Lollipop

        // start of code for Lollipop only
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode != FILECHOOSER_RESULTCODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            Uri[] results = null;
            // check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null || data.getData() == null) {
                    // if there is not data, then we may have taken a photo
                    if (mCameraPhotoPath != null) {
                        results = new Uri[] {Uri.parse(mCameraPhotoPath)};
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[] {Uri.parse(dataString)};
                    }
                }
            }
            mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.removeAllViews();
            webView.destroy();
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    ArrayList<InstaPhotos> instaPhotos;

    void javascriptRun(final String script) {
        webView.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    webView.evaluateJavascript(script, null);
                } else {
                    webView.loadUrl(script);
                }
            }
        }, 900);
    }

    void setupListPhotos() {
        javascriptRun("javascript: setTimeout(function() {\n" +
                "    var t = document.querySelectorAll('div._jjzlb');\n" +
                "    for (var i = 0; i < t.length; i++) {\n" +
                "        var img_tags = t[i].getElementsByTagName('img');\n" +
                "        if (img_tags.length > 0 && !t[i].hasAttribute(\"lite_open_photo\")) {\n" +
                "            var img_tag = img_tags[0];\n" +
                "            var title = img_tag.getAttribute('alt');\n" +
                "            adv.addPhoto(img_tag.getAttribute('src'), title);\n " +
                "            t[i].setAttribute(\"lite_open_photo\", \"yes\");\n" +
                "            \n" +
                "            \n" +
                "                t[i].addEventListener('click', function(event) {\n" +
                "                    event.preventDefault();\n" +
                "                    console.log(title);\n" +
                "                    adv.openPhoto(img_tag.getAttribute('src'), title);\n" +
                "                });\n" +
                "        }\n" +
                "    }\n" +
                "}, 400);");
    }

    /*
     * JavaScript Interface. Web code can access methods in here
     * (as long as they have the @JavascriptInterface annotation)
     */
    public class WebViewJavaScriptInterface {

        private Context context;

        /*
         * Need a reference to the context in order to sent a post message
         */
        public WebViewJavaScriptInterface(Context context) {
            this.context = context;
        }

        /*
         * This method can be called from Android. @JavascriptInterface
         * required after SDK version 17.
         */
        @JavascriptInterface
        public void makeToast(String message, boolean lengthLong) {
            Toast.makeText(context, message, (lengthLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
        }


        @JavascriptInterface
        public boolean saveToDisk(String fileUrl, String fileName) {
            return true;
        }

        @JavascriptInterface
        public boolean openPhoto(String url, String title) {
            if (TextUtils.isEmpty(title) && webView != null) {
                try {
                    title = (String) mActivity.getTitle();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Intent intent = new Intent(mActivity, PagerPhotoViewerActivity.class);
            int index = -1;
            if (instaPhotos != null && (index = instaPhotos.indexOf(url)) > -1) {
                intent.putExtra(PagerPhotoViewerActivity.PHOTOS_URL, instaPhotos);
                intent.putExtra(PagerPhotoViewerActivity.START_POS, index);
            } else {
                ArrayList<String> urls = new ArrayList<>();
                urls.add(url);
                intent.putExtra(PagerPhotoViewerActivity.PHOTOS_URL, urls);
                intent.putExtra(PagerPhotoViewerActivity.START_POS, 0);
            }
            intent.putExtra(PagerPhotoViewerActivity.TITLE_STRING, title);

            startActivityForResult(intent, 777);
            return true;
        }

        @JavascriptInterface
        public boolean addPhoto(String url, String title) {
            InstaPhotos instaPhoto = new InstaPhotos(title, url);
            if (instaPhotos == null)
                instaPhotos = new ArrayList<>();
            if (!instaPhotos.contains(instaPhoto))
                instaPhotos.add(instaPhoto);
            mActivity.supportInvalidateOptionsMenu();
            return true;
        }
    }

    class OpenMediaAsyncTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                OkHttpClient okHttpClient = new OkHttpClient();
                final Call call = okHttpClient.newCall(new Request.Builder()
                        .url(params[0]).build());
                Response response;
                try {
                    response = call.execute();
                } catch (Exception e) {
                    response = call.execute();
                }
                if (response == null)
                    return null;

                String streamToString = convertStreamToString(response.body().byteStream());
                JSONObject object = new JSONObject(streamToString);


                response.close();

                return object;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            if (jsonObject != null) {
                try {
                    JSONObject jsMedia = jsonObject.getJSONObject("media");
                    boolean isAds = jsMedia.getBoolean("is_ad");
                    boolean isVideo = jsMedia.getBoolean("is_video");
                    if (!isAds && !isVideo) {

                        String caption = jsMedia.getString("caption");
                        String display_src = jsMedia.getString("display_src");

                        Intent intent = new Intent(mActivity, InstagramPhotoViewerActivity.class);
                        ArrayList<String> urls = new ArrayList<>();
                        urls.add(display_src);
                        intent.putExtra(InstagramPhotoViewerActivity.PHOTOS_URL, urls);
                        intent.putExtra(InstagramPhotoViewerActivity.START_POS, 0);
                        intent.putExtra(InstagramPhotoViewerActivity.TITLE_STRING, caption);

                        startActivityForResult(intent, 777);

                    }
                } catch (JSONException ex) {
                }
            }
        }
    }

    void blockPhotos() {
        if (AppPreferences.INSTANCE.isBlockPhotoInstagram()) {
            webView.getSettings().setLoadsImagesAutomatically(false);
        } else {
            webView.getSettings().setLoadsImagesAutomatically(true);
        }
        webView.reload();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.UpdateTextSizeEvent e) {
        setupTextSize(e.font);
    }

    void setupTextSize(String font) {
        boolean defaultfont = font.equals("default_font");
        boolean mediumfont = font.equals("medium_font");
        boolean largefont = font.equals("large_font");
        boolean xlfont = font.equals("xl_font");
        boolean xxlfont = font.equals("xxl_font");
        boolean smallfont = font.equals("small_font");

        if (defaultfont)
            webView.getSettings().setTextZoom(100);

        if (smallfont)
            webView.getSettings().setTextZoom(90);

        if (mediumfont)
            webView.getSettings().setTextZoom(105);

        if (largefont)
            webView.getSettings().setTextZoom(110);

        if (xlfont)
            webView.getSettings().setTextZoom(120);

        if (xxlfont)
            webView.getSettings().setTextZoom(150);
    }
}
