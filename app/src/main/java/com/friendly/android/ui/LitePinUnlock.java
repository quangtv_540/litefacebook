package com.friendly.android.ui;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 10/14/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.os.Bundle;
import android.widget.Toast;

import com.friendly.android.pinlock.ConfirmPinActivity;
import com.friendly.android.preferences.AppPreferences;

public class LitePinUnlock extends ConfirmPinActivity {

    private String currentPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentPin = AppPreferences.INSTANCE.getString("lockcode", "");;
    }

    @Override
    public boolean isPinCorrect(String pin) {
        return pin.equals(currentPin);
    }

    @Override
    public void onForgotPin() {
        Toast.makeText(this, "Sorry. Not Implemented", Toast.LENGTH_SHORT).show();
    }
}
