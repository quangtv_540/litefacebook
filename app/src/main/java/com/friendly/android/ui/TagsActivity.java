package com.friendly.android.ui;//

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.friendly.android.R;
import com.friendly.android.adapter.TagsPhotoAdapter;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.model.BgrPhotoTags;
import com.friendly.android.model.Images;
import com.friendly.android.model.Tags;
import com.friendly.android.service.BgrApi;
import com.friendly.android.utils.AppDevices;
import com.friendly.android.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

//
//  Tripi
//
//  Created by Quang Tran on 2/10/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class TagsActivity extends AppCompatActivity {

    Tags tags;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsing;

    @BindView(R.id.header)
    ImageView header;

    List<BgrPhotoItem> bgrPhotoItems;
    @BindView(R.id.photos)
    RecyclerView recyclerView;
    TagsPhotoAdapter photoAdapter;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    String last_pos;

    boolean isLoading, isFinished;

    GridLayoutManager gridLayoutManager;
    int visibleItemCount, totalItemCount, pastVisiblesItems, maxInvisibleItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.nav_back);
        setSupportActionBar(toolbar);

        tags = getIntent().getParcelableExtra(Tags.KEY);
        if (tags == null) {
            finish();
        }

        if (bgrPhotoItems == null) {
            bgrPhotoItems = new ArrayList<>();
        }

        toolbar.setTitle(tags.getTag());
        collapsing.setTitle(tags.getTag());

        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        maxInvisibleItems = 12;

        photoAdapter = new TagsPhotoAdapter(bgrPhotoItems, this);
        recyclerView.setAdapter(photoAdapter);

        recyclerView.addOnScrollListener(
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (isLoading || isFinished || totalItemCount == 0 || TextUtils.isEmpty(last_pos))
                            return;
                        visibleItemCount = gridLayoutManager.getChildCount();
                        pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                        if (visibleItemCount + pastVisiblesItems + maxInvisibleItems > totalItemCount) {
                            if (!Utils.isInternetAvailable(TagsActivity.this)){
                                isLoading = false;
                                return;
                            }
                            new LoadListNextPhotoByTags().execute();
                        }
                    }
                });

        new LoadListPhotoByTags().execute();

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Utils.isInternetAvailable(TagsActivity.this)){
                    Snackbar.make(findViewById(R.id.root), "Refresh failed! Please check your internet connection.", Snackbar.LENGTH_SHORT);
                    return;
                }

                new LoadListPhotoByTags().execute();
            }
        });
    }

    void updateHeaders() {
        if (bgrPhotoItems == null || bgrPhotoItems.isEmpty())
            return;
        int position = new Random().nextInt(bgrPhotoItems.size()-1);

        BgrPhotoItem bgrPhotoItem = bgrPhotoItems.get(position);

        Images images = bgrPhotoItem.getImagesHashMap().get("preview");
        if (images == null) {
            images = bgrPhotoItem.getImagesHashMap().get("image");
        }
        if (images != null) {
            Picasso.with(this)
                    .load(images.getUrl())
                    .resize(AppDevices.getDeviceWidth(), AppDevices.dp(120))
                    .centerCrop()
                    .into(header);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class LoadListPhotoByTags extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoTags> bgrPhotoTagsCall = BgrApi.getBgrService().listPhotoByTags(tags.getTag());
                if (bgrPhotoTagsCall != null) {
                    BgrPhotoTags bgrPhotoTags = bgrPhotoTagsCall.execute().body();
                    if (bgrPhotoTags != null) {
                        last_pos = Utils.decodeUrl(bgrPhotoTags.getNext()).getString("last_pos", "");
                        Log.e("BGR", "Last Post: " + last_pos);
                        if (bgrPhotoTags.getBgrPhotoItems() != null && !bgrPhotoTags.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotoTags.getBgrPhotoItems();
                        }
                    }
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            isLoading = false;
            if (params != null && !params.isEmpty()) {
                bgrPhotoItems.clear();
                bgrPhotoItems.addAll(params);
                photoAdapter.notifyDataSetChanged();
            } else if (!Utils.isInternetAvailable(TagsActivity.this)) {
            }
            totalItemCount = bgrPhotoItems.size();
            mSwipeRefreshLayout.setRefreshing(false);
            // Todo Update Headers
            updateHeaders();
            super.onPostExecute(params);
        }
    }

    class LoadListNextPhotoByTags extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoTags> bgrPhotoTagsCall = BgrApi.getBgrService().listNextPhotoByTags(tags.getTag(), last_pos);
                if (bgrPhotoTagsCall != null) {
                    BgrPhotoTags bgrPhotoTags = bgrPhotoTagsCall.execute().body();
                    if (bgrPhotoTags != null) {
                        last_pos = Utils.decodeUrl(bgrPhotoTags.getNext()).getString("last_pos", "");
                        if (bgrPhotoTags.getBgrPhotoItems() != null && !bgrPhotoTags.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotoTags.getBgrPhotoItems();
                        } else {
                            isFinished = true;
                        }
                    } else {
                        isFinished = true;
                    }
                } else {
                    isFinished = true;
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            isLoading = false;
            if (params != null && !params.isEmpty()) {
                bgrPhotoItems.addAll(params);
                photoAdapter.notifyDataSetChanged();
            } else if (!Utils.isInternetAvailable(TagsActivity.this)) {
            }
            totalItemCount = bgrPhotoItems.size();
            super.onPostExecute(params);
        }
    }
}
