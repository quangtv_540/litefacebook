package com.friendly.android.ui;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 9/30/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.friendly.android.Constants;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.manager.PhotoClickListener;
import com.friendly.android.utils.Connectivity;
import com.friendly.android.utils.ToastAdListener;
import com.friendly.android.utils.Utils;
import com.friendly.android.views.CustomViewPager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoViewAttacher;

public class InstagramPhotoViewerActivity extends AppCompatActivity implements PhotoClickListener {

    private static final int REQUEST_STORAGE = 1;

    public static final String PHOTOS_URL = "photo_url";
    public static final String TITLE_STRING = "title_key";
    public static final String START_POS = "start_position";
    //private String photoUrl;
    private ArrayList<String> urls;
    private String photoCaptain;
    private int startPos;
    private CustomViewPager viewPager;
    private InstaPhotoFragmentAdapter photosAdapter;
    private View saveButton;
    private View shareButton;
    private Toolbar toolbar;
    private boolean isHidden;
    private TextView captain;
    // declare the dialog as a member field of your activity
    private ProgressDialog mProgressDialog;

    boolean isDownloadPhoto = false;
    String currentUrlPhoto;

    @BindView(R.id.root)
    View root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_photo_viewpager);

        ButterKnife.bind(this);
        saveButton = findViewById(R.id.save_image);
        shareButton = findViewById(R.id.share_image);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        viewPager = (CustomViewPager) findViewById(R.id.photo_viewer_view_pager);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState == null) {
            urls = getIntent().getStringArrayListExtra(PHOTOS_URL);
            photoCaptain = getIntent().getStringExtra(TITLE_STRING);
            startPos = getIntent().getIntExtra(START_POS, 0);
        } else {
            urls = savedInstanceState.getStringArrayList(PHOTOS_URL);
            photoCaptain = savedInstanceState.getString(TITLE_STRING);
            startPos = savedInstanceState.getInt(START_POS, 0);
        }


        Log.e(PagerPhotoViewerActivity.class.getName(), startPos + " ");

        photosAdapter = new InstaPhotoFragmentAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(photosAdapter);
        viewPager.setCurrentItem(startPos);

        setTitle("Instagram Photos");
        captain = (TextView) findViewById(R.id.captain);

        captain.setText(photoCaptain);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int currentPos = viewPager.getCurrentItem();
                    downloadCurrentUrl(urls.get(currentPos));
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int currentPos = viewPager.getCurrentItem();
                    String url = urls.get(currentPos);

                    if (!Connectivity.isConnected(InstagramPhotoViewerActivity.this)) {
                        return;
                    }

                    sharePhotos(url);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (FriendlyApplication.isShowAds)
            initAdmob();
        else findViewById(R.id.adBanner).setVisibility(View.GONE);
    }

    // AdMob
    AdView mAdView;
    void initAdmob() {
        mAdView = new AdView(this);
        mAdView.setAdUnitId(Constants.ADMOB_BANNER);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdListener(new ToastAdListener(this));
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.adBanner);
        layout.addView(mAdView);

        mAdView.loadAd(new AdRequest.Builder().build());

        layout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList(PHOTOS_URL, urls);
        outState.putString(TITLE_STRING, photoCaptain);
        outState.putInt(START_POS, viewPager.getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static boolean checkWriteExternalPermission(Context context) {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onPhotoClick() {
        hide();
    }

    public void hide() {
        if(!isHidden)
            getSupportActionBar().hide();
        else
            getSupportActionBar().show();
        saveButton.setVisibility(isHidden ? View.VISIBLE : View.GONE);
        shareButton.setVisibility(isHidden ? View.VISIBLE : View.GONE);
        isHidden = !isHidden;
    }



    private class InstaPhotoFragmentAdapter extends FragmentStatePagerAdapter {

        PhotoClickListener listener;

        public InstaPhotoFragmentAdapter(FragmentManager fm, PhotoClickListener listener) {
            super(fm);
            this.listener = listener;
        }

        @Override
        public Fragment getItem(int position) {
            InstaPhotoFragmentItem item = InstaPhotoFragmentItem.instance(urls.get(position));
            item.setListener(this.listener);
            return item;
        }

        @Override
        public int getCount() {
            return urls == null ? 0 : urls.size();
        }
    }

    public static class InstaPhotoFragmentItem extends Fragment {

        public static final String URL_KEY = "url_key";

        private String photoUrl;

        private ImageView fullImageView;
        private ProgressBar progressBar;

        private PhotoViewAttacher mAttacher;
        private PhotoClickListener listener;

        public static InstaPhotoFragmentItem instance(String photoUrl) {
            InstaPhotoFragmentItem fragmentItem = new InstaPhotoFragmentItem();
            Bundle params = new Bundle();
            params.putString(URL_KEY, photoUrl);
            fragmentItem.setArguments(params);
            return fragmentItem;
        }
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.photoUrl = getArguments().getString(URL_KEY);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_photo_viewer_instagram_item, container, false);
            return rootView;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            fullImageView = (ImageView) view.findViewById(R.id.fullSizedImage);
            progressBar = (ProgressBar) view.findViewById(R.id.image_progress);

            mAttacher = new PhotoViewAttacher(fullImageView);
            mAttacher.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
                @Override
                public void onPhotoTap(View view, float v, float v1) {
                    listener.onPhotoClick();
                }

                @Override
                public void onOutsidePhotoTap() {

                }
            });

            loadImage(photoUrl);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mAttacher.cleanup();
        }

        private void loadImage(String photoUrl) {
        /*if (this.url.contains("gif"))
        {
            this.share.setVisibility(8);
            this.imageViewTarget = new GlideDrawableImageViewTarget(this.fullImage);
            Glide.with(this).load(this.url).listener(new RequestListener()
            {
                public boolean onException(Exception paramAnonymousException, String paramAnonymousString, Target<GlideDrawable> paramAnonymousTarget, boolean paramAnonymousBoolean)
                {
                    return false;
                }

                public boolean onResourceReady(GlideDrawable paramAnonymousGlideDrawable, String paramAnonymousString, Target<GlideDrawable> paramAnonymousTarget, boolean paramAnonymousBoolean1, boolean paramAnonymousBoolean2)
                {
                    PhotoActivity.this.fullImage.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                    PhotoActivity.this.findViewById(2131624086).setVisibility(8);
                    return false;
                }
            }).into(this.imageViewTarget);
        }*/
            Glide.with(getActivity()).load(photoUrl)
                    //.fitCenter()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            fullImageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
                            progressBar.setVisibility(View.GONE);
                            mAttacher.update();
                            return false;
                        }
                    }).into(fullImageView);

        }

        public PhotoClickListener getListener() {
            return listener;
        }

        public void setListener(PhotoClickListener listener) {
            this.listener = listener;
        }
    }


    public void downloadCurrentUrl (String photoUrl) {
        //if(Build.VERSION_CODES)
        //PermissionChecker
        if (!checkWriteExternalPermission(InstagramPhotoViewerActivity.this)) {
            String[] permissions = new String[REQUEST_STORAGE];
            permissions[0] = "android.permission.WRITE_EXTERNAL_STORAGE";
            ActivityCompat.requestPermissions(this, permissions, REQUEST_STORAGE);

            isDownloadPhoto = true;
            currentUrlPhoto = photoUrl;
            Toast.makeText(InstagramPhotoViewerActivity.this, "No permission to write on storage!", Toast.LENGTH_LONG).show();
            return;
        }
        if (!Utils.isDownloadManagerAvailable(this)) {
            downloadByAsyncTask(photoUrl);
            return;
        }
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(photoUrl));
        request.setDescription("Facebook Photos");
        request.setTitle("Downloading by AsapChat");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        Calendar calendar = Calendar.getInstance();
        StringBuilder fileName = new StringBuilder();
        fileName.append(calendar.get(Calendar.YEAR));
        fileName.append("-" +calendar.get(Calendar.MONTH));
        fileName.append( "-" +calendar.get(Calendar.DAY_OF_MONTH));
        fileName.append( "-" + calendar.get(Calendar.HOUR_OF_DAY));
        fileName.append("-" + calendar.get(Calendar.MINUTE));
        fileName.append("-" + calendar.get(Calendar.SECOND));
        fileName.append( ".jpg");
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName.toString());

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private void downloadByAsyncTask (String imgUrl) {
        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(InstagramPhotoViewerActivity.this);
        mProgressDialog.setMessage("A message");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        // execute this when the downloader must be fired
        final InstaDownloadTask downloadTask = new InstaDownloadTask(InstagramPhotoViewerActivity.this);
        downloadTask.execute(imgUrl);

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });
    }


    private class InstaDownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public InstaDownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            try {
                Bitmap bitmap = Glide.
                        with(context).
                        load(sUrl[0]).
                        asBitmap().
                        into(Target.SIZE_ORIGINAL,Target.SIZE_ORIGINAL).
                        get();
                Calendar calendar = Calendar.getInstance();
                StringBuilder fileName = new StringBuilder();
                fileName.append(calendar.get(Calendar.YEAR));
                fileName.append("-" +calendar.get(Calendar.MONTH));
                fileName.append( "-" +calendar.get(Calendar.DAY_OF_MONTH));
                fileName.append( "-" + calendar.get(Calendar.HOUR_OF_DAY));
                fileName.append("-" + calendar.get(Calendar.MINUTE));
                fileName.append("-" + calendar.get(Calendar.SECOND));
                fileName.append( ".jpg");
                //File file = new File(Environment.getExternalStorageDirectory().toString(),  fileName.toString());
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),  fileName.toString());

                OutputStream outStream= new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (final ExecutionException e) {
                Log.e("DownloadTask", e.getMessage());
                return e.toString();
            } catch (final Exception e) {
                Log.e("DownloadTask", e.getMessage());
                Log.e("DownloadTask", e.getMessage());
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null)
                Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG);
            else
                Toast.makeText(context,"File saved in Download folder", Toast.LENGTH_SHORT).show();
        }


    }


    void sharePhotos(String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isDownloadPhoto && !TextUtils.isEmpty(currentUrlPhoto)) {
                        downloadCurrentUrl(currentUrlPhoto);
                    } else if (!TextUtils.isEmpty(currentUrlPhoto)) {
                        sharePhotos(currentUrlPhoto);
                    }
                } else {
                    Snackbar.make(root, getString(R.string.permission_not_granted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
