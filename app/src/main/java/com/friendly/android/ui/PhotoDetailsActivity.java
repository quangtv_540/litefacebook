package com.friendly.android.ui;

import android.app.ActivityOptions;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.friendly.android.Constants;
import com.friendly.android.R;
import com.friendly.android.adapter.SimilarAdapter;
import com.friendly.android.db.DatabaseManager;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.model.BgrPhotoRecents;
import com.friendly.android.model.Images;
import com.friendly.android.model.Tags;
import com.friendly.android.model.User;
import com.friendly.android.service.BgrApi;
import com.friendly.android.utils.AppDevices;
import com.friendly.android.utils.ToastAdListener;
import com.friendly.android.utils.Utils;
import com.friendly.android.utils.ViewUtil;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * Created by mrquang on 8/24/15.
 */
public class PhotoDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    BgrPhotoItem qhdPhotoItem;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.license)
    TextView license;

    @BindView(R.id.views)
    TextView views;
    @BindView(R.id.likes)
    TextView likes;
    @BindView(R.id.comments)
    TextView comments;
    @BindView(R.id.download)
    TextView download;

    @BindView(R.id.header)
    ImageView header;

    @BindView(R.id.avatar)
    ImageView userAvatar;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_homepage)
    TextView userHomePage;
    @BindView(R.id.user_description)
    TextView userDesc;
    @BindView(R.id.user_location)
    TextView userLocation;

    @BindView(R.id.tags)
    ViewGroup tags;

    @BindView(R.id.similar)
    TextView similarTitle;
    @BindView(R.id.similarRecycler)
    RecyclerView similarRecycler;

    List<BgrPhotoItem> bgrPhotoItems;
    LinearLayoutManager mLayoutManager;
    SimilarAdapter photoAdapter;

    @BindView(R.id.fabFavorite)
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);

        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.nav_back);
        setSupportActionBar(toolbar);

        qhdPhotoItem = getIntent().getParcelableExtra(BgrPhotoItem.KEY);
        if (qhdPhotoItem == null) {
            finish();
        }

        collapsingToolbar.setTitle(qhdPhotoItem.getTitle());
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.transparent));
        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));

        updateFavorites();

        setups();

        updatePhotoInformation();

        updateTags();

        addBanner();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.avatar:
                moveToUserProfile();
                break;
            case R.id.user_name:
                moveToUserProfile();
                break;
            case R.id.btnDownload:
                download();
                break;
            case R.id.btnWallpaper:
                setWallpapers();
                break;
            case R.id.fabFavorite:
                DatabaseManager.DB.saveOrUpdatePhotos(qhdPhotoItem);
                updateFavorites();
                break;
        }
    }

    void setups() {
        userAvatar.setOnClickListener(this);
        userName.setOnClickListener(this);
        findViewById(R.id.btnDownload).setOnClickListener(this);
        findViewById(R.id.btnWallpaper).setOnClickListener(this);
        findViewById(R.id.fabFavorite).setOnClickListener(this);

        Images images = qhdPhotoItem.getImagesHashMap().get("image");
        if (images == null) {
            images = qhdPhotoItem.getImagesHashMap().get("preview");
        }

        if (images != null) {
            float xScale = images.getWidth() / images.getHeight();

            Picasso.with(this)
                    .load(images.getUrl())
                    .resize(AppDevices.getDeviceWidth(), (int) (AppDevices.getDeviceWidth() / xScale))
                    .centerCrop()
                    .into(header);
        }

        // Todo get similar photos
        if (bgrPhotoItems == null)
            bgrPhotoItems = new ArrayList<>();

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        photoAdapter = new SimilarAdapter(bgrPhotoItems, this);
        similarRecycler.setAdapter(photoAdapter);
        similarRecycler.setLayoutManager(mLayoutManager);
        if (bgrPhotoItems.isEmpty())
            new LoadListSimilarPhotos().execute();
    }

    void updatePhotoInformation() {
        title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/SF-UI-Text-Regular.otf"));
        description.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/SF-UI-Text-Regular.otf"));
        license.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/SF-UI-Text-Regular.otf"));
        userName.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/SF-UI-Text-Regular.otf"));
        userHomePage.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/SF-UI-Text-Regular.otf"));
        userDesc.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/SF-UI-Text-Regular.otf"));
        userLocation.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/SF-UI-Text-Regular.otf"));

        title.setText(qhdPhotoItem.getTitle());
        title.setVisibility(TextUtils.isEmpty(qhdPhotoItem.getTitle()) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(qhdPhotoItem.getDescription()))
            description.setText(Html.fromHtml(qhdPhotoItem.getDescription()));
        description.setVisibility(TextUtils.isEmpty(qhdPhotoItem.getDescription()) ? View.GONE : View.VISIBLE);

        license.setText(ViewUtil.getLicenseText(qhdPhotoItem.getLicense().getType()));
        license.setCompoundDrawablesWithIntrinsicBounds(ViewUtil.getLicenseIcon(qhdPhotoItem.getLicense().getType()), 0, 0, 0);

        views.setText(Utils.formatDownloadCount(qhdPhotoItem.getViews_count()));
        likes.setText(Utils.formatDownloadCount(qhdPhotoItem.getLikes_count()));
        comments.setText(Utils.formatDownloadCount(qhdPhotoItem.getComments_count()));
        download.setText(Utils.formatDownloadCount(qhdPhotoItem.getDownloads_count()));

        Picasso.with(this)
                .load(qhdPhotoItem.getUser().getAvatar().getUrl())
                .resize(AppDevices.dp(40), AppDevices.dp(40))
                .centerCrop()
                .into(userAvatar);

        userName.setText(qhdPhotoItem.getUser().getName());
        userHomePage.setText(qhdPhotoItem.getUser().getHomepage());
        userDesc.setText(qhdPhotoItem.getUser().getDescription());
        userDesc.setVisibility(TextUtils.isEmpty(qhdPhotoItem.getUser().getDescription()) ? View.GONE : View.VISIBLE);
        userLocation.setText(String.format("Location: %s", qhdPhotoItem.getUser().getLocation()));
        userLocation.setVisibility(TextUtils.isEmpty(qhdPhotoItem.getUser().getLocation()) ? View.GONE : View.VISIBLE);
    }

    void updateTags() {
        if (qhdPhotoItem.getTags() == null || qhdPhotoItem.getTags().isEmpty())
            return;
        for (final Tags agency : qhdPhotoItem.getTags()) {
            final TextView view = (TextView) LayoutInflater.from(this).inflate(R.layout.tags_item, null, false);
            view.setText("#"+agency.getTag());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Todo move to Tags screen
                    Intent intent = new Intent(PhotoDetailsActivity.this, TagsActivity.class);
                    intent.putExtra(Tags.KEY, agency);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setExitTransition(new Explode());
                        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(PhotoDetailsActivity.this).toBundle());
                    } else {
                        startActivity(intent);
                    }
                }
            });
            tags.addView(view,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    void updateFavorites() {
        if (DatabaseManager.DB.isPhotos(qhdPhotoItem)) {
            fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            fab.setImageResource(R.drawable.ic_like);
        } else {
            fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            fab.setImageResource(R.drawable.ic_unlike);
        }
        supportInvalidateOptionsMenu();
    }

    void moveToUserProfile() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra(User.KEY, qhdPhotoItem.getUser());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivity(intent);
        }
    }

    void download() {
        Images images = qhdPhotoItem.getImagesHashMap().get("max");
        if (images == null) {
            images = qhdPhotoItem.getImagesHashMap().get("image");
        }
        if (images == null) {
            images = qhdPhotoItem.getImagesHashMap().get("preview");
        }
        downloadCurrentUrl(images.getUrl());
    }

    void setWallpapers() {
        Images images1 = qhdPhotoItem.getImagesHashMap().get("max");
        if (images1 == null) {
            images1 = qhdPhotoItem.getImagesHashMap().get("image");
        }
        if (images1 == null) {
            images1 = qhdPhotoItem.getImagesHashMap().get("preview");
        }
        new WallpapersFromURL(this).execute(images1.getUrl());
    }

    void sharePhotos(String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    AdView mAdView;
    @BindView(R.id.layoutBanner)
    LinearLayout layoutBanner;
    void addBanner() {
        mAdView = new AdView(this);
        mAdView.setAdUnitId(Constants.ADMOB_BANNER);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdListener(new ToastAdListener(this));
        layoutBanner.addView(mAdView);

        mAdView.loadAd(new AdRequest.Builder().build());
    }

    class LoadListSimilarPhotos extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoRecents> bgrPhotosCall = BgrApi.getBgrService().listPhotoBySimilar(qhdPhotoItem.getUuid());
                if (bgrPhotosCall != null) {
                    BgrPhotoRecents bgrPhotos  = bgrPhotosCall.execute().body();
                    if (bgrPhotos != null) {
                        if (bgrPhotos.getBgrPhotoItems() != null && !bgrPhotos.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotos.getBgrPhotoItems();
                        }
                    }

                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            if (params != null && !params.isEmpty()) {
                Log.e("BGR", "Size: " + params.size());
                bgrPhotoItems.clear();
                bgrPhotoItems.addAll(params);
                photoAdapter.notifyDataSetChanged();
            }
            if (!bgrPhotoItems.isEmpty()) {
                similarTitle.setVisibility(View.VISIBLE);
                similarRecycler.setVisibility(View.VISIBLE);
            } else {
                similarRecycler.setVisibility(View.GONE);
                similarTitle.setVisibility(View.GONE);
            }
            super.onPostExecute(params);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.photo_details, menu);
        menu.findItem(R.id.action_add_to_favorites).setTitle(DatabaseManager.DB.isPhotos(qhdPhotoItem)
        ? R.string.action_remove_favorite : R.string.action_add_favorite);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_add_to_favorites).setTitle(DatabaseManager.DB.isPhotos(qhdPhotoItem)
                ? R.string.action_remove_favorite : R.string.action_add_favorite);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_set_as_wallpaper:
                setWallpapers();
                return true;
            case R.id.action_share:
                Images images1 = qhdPhotoItem.getImagesHashMap().get("max");
                if (images1 == null) {
                    images1 = qhdPhotoItem.getImagesHashMap().get("image");
                }
                if (images1 == null) {
                    images1 = qhdPhotoItem.getImagesHashMap().get("preview");
                }
                sharePhotos(images1.getUrl());
                return true;
            case R.id.action_download:
                download();
                return true;
            case R.id.action_add_to_favorites:
                DatabaseManager.DB.saveOrUpdatePhotos(qhdPhotoItem);
                updateFavorites();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static boolean checkWriteExternalPermission(Context context) {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    ProgressDialog mProgressDialog;
    public void downloadCurrentUrl (String photoUrl) {
        if (!checkWriteExternalPermission(PhotoDetailsActivity.this)) {
            String[] permissions = new String[REQUEST_STORAGE];
            permissions[0] = "android.permission.WRITE_EXTERNAL_STORAGE";
            ActivityCompat.requestPermissions(this, permissions, REQUEST_STORAGE);

            isDownloadPhoto = true;
            currentUrlPhoto = photoUrl;
            Toast.makeText(PhotoDetailsActivity.this, "No permission to write on storage!", Toast.LENGTH_LONG).show();
            return;
        }
        if (!Utils.isDownloadManagerAvailable(this)) {
            downloadByAsyncTask(photoUrl);
            return;
        }
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(photoUrl));
        request.setDescription("Wallpaper Photos");
        request.setTitle("Downloading by Wallpaper HD+");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        Calendar calendar = Calendar.getInstance();
        StringBuilder fileName = new StringBuilder();
        fileName.append(calendar.get(Calendar.YEAR));
        fileName.append("-" +calendar.get(Calendar.MONTH));
        fileName.append( "-" +calendar.get(Calendar.DAY_OF_MONTH));
        fileName.append( "-" + calendar.get(Calendar.HOUR_OF_DAY));
        fileName.append("-" + calendar.get(Calendar.MINUTE));
        fileName.append("-" + calendar.get(Calendar.SECOND));
        fileName.append( ".jpg");
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName.toString());

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private void downloadByAsyncTask (String imgUrl) {
        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(PhotoDetailsActivity.this);
        mProgressDialog.setMessage("Downloading");
        mProgressDialog.setTitle("Please wait...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        // execute this when the downloader must be fired
        final DownloadTask downloadTask = new DownloadTask(PhotoDetailsActivity.this);
        downloadTask.execute(imgUrl);

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });
    }

    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            try {
                Bitmap bitmap = Picasso.with(context)
                        .load(sUrl[0])
                        .get();
                Calendar calendar = Calendar.getInstance();
                StringBuilder fileName = new StringBuilder();
                fileName.append(calendar.get(Calendar.YEAR));
                fileName.append("-" +calendar.get(Calendar.MONTH));
                fileName.append( "-" +calendar.get(Calendar.DAY_OF_MONTH));
                fileName.append( "-" + calendar.get(Calendar.HOUR_OF_DAY));
                fileName.append("-" + calendar.get(Calendar.MINUTE));
                fileName.append("-" + calendar.get(Calendar.SECOND));
                fileName.append( ".jpg");
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),  fileName.toString());

                OutputStream outStream= new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (final Exception e) {
                Log.e("DownloadTask", e.getMessage());
                Log.e("DownloadTask", e.getMessage());
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null)
                Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG);
            else
                Toast.makeText(context,"File saved in Download folder", Toast.LENGTH_SHORT).show();
        }


    }

    class WallpapersFromURL extends AsyncTask<String, Void, Void> {

        PhotoDetailsActivity activity;
        ProgressDialog dialog;

        public WallpapersFromURL(PhotoDetailsActivity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(PhotoDetailsActivity.this,
                    "Set As Wallpaper", "Downloading file. Please wait...");
        }

        @Override
        protected Void doInBackground(String... f_url) {
            String url = f_url[0];
            try {
                Bitmap result = Picasso.with(activity)
                        .load(url)
                        .get();

                WallpaperManager wallpaperManager = WallpaperManager.getInstance(activity);

                wallpaperManager.setBitmap(result);
            } catch (IOException e) {
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                    Intent intent = new Intent(PhotoDetailsActivity.this, SetWallpaperDoneActivity.class);
                    intent.putExtra(BgrPhotoItem.KEY, qhdPhotoItem);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        PhotoDetailsActivity.this.getWindow().setExitTransition(new Explode());
                        PhotoDetailsActivity.this.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
                    } else {
                        PhotoDetailsActivity.this.startActivity(intent);
                    }
                }
            }, 800);
        }
    }

    private static final int REQUEST_STORAGE = 1;
    boolean isDownloadPhoto = false;
    String currentUrlPhoto;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isDownloadPhoto && !TextUtils.isEmpty(currentUrlPhoto)) {
                        downloadCurrentUrl(currentUrlPhoto);
                    } else if (!TextUtils.isEmpty(currentUrlPhoto)) {
                        sharePhotos(currentUrlPhoto);
                    }
                } else {
                    Snackbar.make(findViewById(R.id.coordinator), getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
