package com.friendly.android.ui.fragment;//

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareDialog;
import com.friendly.android.AppConfig;
import com.friendly.android.Constants;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.event.UiEvent;
import com.friendly.android.manager.FriendlyListener;
import com.friendly.android.manager.LiteHelpers;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.ui.BrowserActivity;
import com.friendly.android.ui.FacebookPhotoViewerActivity;
import com.friendly.android.ui.HomeActivity;
import com.friendly.android.ui.PagerPhotoViewerActivity;
import com.friendly.android.ui.SettingsActivity;
import com.friendly.android.utils.Connectivity;
import com.friendly.android.utils.TrackerScreen;
import com.friendly.android.utils.Utils;
import com.friendly.android.views.FriendlyWebView;
import com.friendly.android.views.FriendlyWebViewClient;
import com.github.clans.fab.FloatingActionMenu;
import com.mikepenz.actionitembadge.library.ActionItemBadge;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

import static android.app.Activity.RESULT_OK;
import static com.friendly.android.manager.LiteHelpers.updateMessagesService;
import static com.friendly.android.manager.LiteHelpers.updateNotificationsService;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class FeedsFragment extends BaseFragment implements HomeActivity.HeaderControl, FriendlyWebView.OnScrollChangedCallback {

    HomeActivity mActivity;

    @BindView(R.id.root)
    View root;

    View mCustomView;
    @BindView(R.id.fullscreen_custom_content)
    FrameLayout customViewContainer;
    WebChromeClient.CustomViewCallback mCustomViewCallback;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.friendlyWebView)
    FriendlyWebView webView;

    @BindView(R.id.btnFullscreen)
    RelativeLayout btnFullscreen;

    Toolbar toolbar;
    MenuItem mNotificationButton;

    @Subscribe
    public void onEventMainThread(UiEvent.SelectTabNewsFeedEvent e) {
        if (webView.canGoBack() && Connectivity.isConnected(mActivity)) {
            btnFullscreen.setVisibility(View.GONE);
            webView.goBack();
            return;
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack() && Connectivity.isConnected(mActivity)) {
            btnFullscreen.setVisibility(View.GONE);
            webView.goBack();
            return;
        }
        mActivity.popFragments(true);
    }

    public FeedsFragment() {

    }

    @Override
    public void onHomeHeaderUpdate(Toolbar toolbar) {
        super.onHomeHeaderUpdate(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {
                    btnFullscreen.setVisibility(View.GONE);
                    webView.goBack();
                    return;
                }
                mActivity.onBackPressed();
            }
        });
        this.toolbar = toolbar;
        toolbar.setTitle(R.string.newsfeed);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_newsfeed, null);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mActivity = (HomeActivity) getActivity();

        visitedPages = new HashSet<>();
        askedVideos = new HashSet<>();
        firstTimeVideos = new HashSet<>();


        TrackerScreen.openLiteScreen(mActivity, "FeedsFragment");

        setups();

        webViewSettings();

        /** if opened by a notification or a shortcut */
        try {
            if (mActivity.getIntent().getExtras().getString("start_url") != null) {
                boolean isMessage = mActivity.getIntent().getExtras().getBoolean("isMessage", false);
                if (!isMessage) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl(AppConfig.FB_NOTIFY_URL);
                        }
                    }, 800);
                }
            }
        } catch (Exception ignored) {}
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_newsfeed, menu);
        mNotificationButton = menu.findItem(R.id.menu_notifications);
        menu.findItem(R.id.menu_remove_ads).setVisible(FriendlyApplication.isShowAds);
        if (isAdded()) {
            ActionItemBadge.update(HomeActivity.context, mNotificationButton, ResourcesCompat.getDrawable(getResources(),
                    R.drawable.ic_notify_white, null), ActionItemBadge.BadgeStyles.RED, Integer.MIN_VALUE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notifications:
                webView.loadUrl(AppConfig.FB_NOTIFY_JS_URL);
                LiteHelpers.updateNotifications(webView);
                return true;
            case R.id.menu_search:
                webView.loadUrl(AppConfig.FB_SEARCH_JS_URL);
                return true;
            case R.id.menu_upload_video:
                if (!hasStoragePermission("android.permission.READ_EXTERNAL_STORAGE")) {
                    String[] permissions = new String[REQUEST_STORAGE];
                    permissions[0] = "android.permission.READ_EXTERNAL_STORAGE";
                    requestPermissions(permissions, REQUEST_STORAGE);
                    fabUploadVideo = true;
                    return true;
                }
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/*");
                startActivityForResult(
                        Intent.createChooser(intent, "Select Video"),
                        SELECT_VIDEO);
                return true;
            case R.id.menu_most_recent:
                webView.loadUrl(AppConfig.FB_MOST_URL);
                return true;
            case R.id.menu_top_news:
                webView.loadUrl(AppConfig.FB_TOP_URL);
                return true;
            case R.id.menu_groups:
                webView.loadUrl(AppConfig.FB_GROUP_URL);
                return true;
            case R.id.menu_pages:
                webView.loadUrl(AppConfig.FB_PAGE_URL);
                return true;
            case R.id.menu_events:
                webView.loadUrl(AppConfig.FB_EVENT_URL);
                return true;
            case R.id.menu_notes:
                webView.loadUrl(AppConfig.FB_NOTE_URL);
                return true;
            case R.id.menu_pokes:
                webView.loadUrl(AppConfig.FB_POKE_URL);
                return true;
            case R.id.menu_settings:
                mActivity.startActivity(new Intent(mActivity, SettingsActivity.class));
                return true;
            case R.id.menu_remove_ads:
                mActivity.showRemoveAdsOptions(false);
                return true;
            case R.id.menu_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void setups() {
        getView().findViewById(R.id.jumpFab).setOnClickListener(mFABClickListener);
        getView().findViewById(R.id.photoFab).setOnClickListener(mFABClickListener);
        getView().findViewById(R.id.updateFab).setOnClickListener(mFABClickListener);

        swipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("Lite", "Url: " + webView.getUrl());
                webView.reload();
            }
        });

        if (toolbar == null)
            toolbar = mActivity.toolbar;

        // Todo show/hide FAB
        showHideFAB();

        btnFullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listUrlsPhotoViewerSet != null && !listUrlsPhotoViewerSet.isEmpty()) {
                    Intent intent = new Intent(mActivity, PagerPhotoViewerActivity.class);
                    intent.putExtra(PagerPhotoViewerActivity.PHOTOS_URL, listUrlsPhotoViewerSet);
                    intent.putExtra(PagerPhotoViewerActivity.START_POS, 0);
                    intent.putExtra(PagerPhotoViewerActivity.TITLE_STRING, "Photos");

                    startActivityForResult(intent, 777);
                }
            }
        });
    }

    void webViewSettings() {
        webView.setVerticalScrollBarEnabled(false);
        webView.setScrollbarFadingEnabled(true);

        if (FriendlyApplication.isTablet) {
            webView.getSettings().setUserAgentString(Constants.USER_AGENT_DESKTOP);
        } else {
            webView.getSettings().setUserAgentString(Constants.MESSENGER);
        }

        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setGeolocationDatabasePath(mActivity.getFilesDir().getPath());
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

        webView.setVerticalScrollBarEnabled(false);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        if (Build.VERSION.SDK_INT < 18) {
            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webView.getSettings().setAppCacheMaxSize(5 * 1024 * 1024);
        }
        webView.getSettings().setAppCachePath(mActivity.getCacheDir().getAbsolutePath());
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.setListener(mActivity, new FriendlyListener(mActivity, webView));
        webView.addJavascriptInterface(new WebViewJavaScriptInterface(mActivity), "adv");
        webView.setOnScrollChangedCallback(this);

        setupTextSize(AppPreferences.INSTANCE.getFont());

        if (preferences.getBoolean("no_images", false))
            webView.getSettings().setLoadsImagesAutomatically(false);

        webView.loadUrl(AppConfig.FB_FACEBOOK_URL);


        setupWebViewClient();

        setupWebChromeClient();
    }

    void setupWebViewClient() {
        webView.setWebViewClient(new FriendlyWebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.split("\\?")[0].endsWith(".jpg")) {
                    Intent photoViewer = new Intent(mActivity, FacebookPhotoViewerActivity.class);
                    photoViewer.putExtra("url", url);
                    photoViewer.putExtra("title", view.getTitle());
                    startActivity(photoViewer);
                    return true;
                }

                if (url.contains("scontent") && url.contains("jpg")) {
                    if (url.contains("l.php?u=")) {
                        return false;
                    }
                    Intent photoViewer = new Intent(mActivity, FacebookPhotoViewerActivity.class);
                    photoViewer.putExtra("url", url);
                    photoViewer.putExtra("title", view.getTitle());
                    startActivity(photoViewer);
                    return true;
                }

                if ((url.contains("market://") || url.contains("mailto:")
                        || url.contains("play.google") || url.contains("youtube")
                        || url.contains("tel:")
                        || url.contains("vid:")) == true) {
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                }

                if (url != null && url.contains("https://m.facebook.com/messages/?pageNum=")) {
                    webView.loadUrl(AppConfig.FB_MESSAGE_URL);
                    return true;
                }


                if (url.startsWith("https://m.facebook.com") || url.contains("http://m.facebook.com")
                        || (url.startsWith("https://mobile.facebook.com")) || (url.startsWith("http://h.facebook.com"))
                        || url.contains("messenger.com") || url.startsWith("https://free.facebook.com")
                        || url.startsWith("https://0.facebook.com")) {
                    return false;
                }
                if (url.startsWith("https://www.facebook.com/") || url.startsWith("http://www.facebook.com/")) {
                    url = url.replace("www.facebook.com", "m.facebook.com");
                    webView.loadUrl(url);
                    return true;
                }

                if (preferences.getBoolean("allow_inside", true)) {
                    Intent intent = new Intent(mActivity, BrowserActivity.class);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    return true;
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try {
                    view.getContext().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e("shouldOverrideUrlLoad", "" + e.getMessage());
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                if (view.canGoBack()) {
                    if (isAdded())
                        mActivity.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.nav_back));
                } else {
                    if (isAdded())
                        mActivity.toolbar.setNavigationIcon(null);
                }
                if (url != null && (url.startsWith("https://m.facebook.com/stories.php?aftercursor")
                        || url.contains("tsid=") || url.contains("v=year-overview&timecutoff="))) {
                    set4Videos(2357);
                }

                //set photoViewer for set of photos  https://scontent
                if (url != null && (url.contains("photos/viewer/?photoset_token=")
                        || url.contains("&mds=%2Fphotos%2Fviewer%2F%3Fphotoset_token"))) {
                    isSetPhotoViewer4PhotoSet = false;
                    listUrlsPhotoViewerSet = new ArrayList<>();
                    if (!visitedPages.contains(url)) {
                        firstTimeVisitViewerSet = true;
                        visitedPages.add(url);
                    } else
                        firstTimeVisitViewerSet = false;
                    setUpJSCollectionPhotoViewer(2500L);
                }
                if (url != null && url.contains("m.facebook.com/photos/snowflake/page"))
                    isSetPhotoViewer4PhotoSet = false;


                if (!isSetPhotoViewer4PhotoSet && url != null
                        && ((url.contains("m.facebook.com/favicon.ico") && !firstTimeVisitViewerSet)
                        || url.contains("https://scontent"))) {

                    isSetPhotoViewer4PhotoSet = true;
                    setUpJSCollectionPhotoViewer(4L);
                }
            }

            @SuppressLint("ResourceAsColor")
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (preferences.getBoolean("show_fab", false) && (url.endsWith("facebook.com/home.php")
                        || url.endsWith("facebook.com/") || url.endsWith("facebook.com"))) {
                    mFab.setVisibility(View.VISIBLE);
                } else {
                    mFab.setVisibility(View.GONE);
                }

                if (url.contains("sharer") || url.contains("/composer/") || url.contains("throwback_share_source")) {
                    view.loadUrl(Utils.injectCss("web/showtopbar.css", context));
                }

                updateNotificationsService(view, 5000);
                updateMessagesService(view, 5000);

                String themeCss = "default_feeds.css";
                if (mActivity.materialTheme) {
                    themeCss = "materialize.css";
                } else if (mActivity.blackTheme) {
                    themeCss = "black.css";
                } else if (mActivity.draculaTheme) {
                    themeCss = "dark.css";
                }

                view.loadUrl(Utils.injectCss("web/"+themeCss, context));
            }
        });
    }

    void setupWebChromeClient() {
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 20)
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }
                mCustomView = view;
                customViewContainer.setVisibility(View.VISIBLE);
                customViewContainer.addView(view);
                mCustomViewCallback = callback;
            }

            @Override
            public void onHideCustomView() {
                super.onHideCustomView();
                if (mCustomView == null)
                    return;
                // hide and remove customViewContainer
                mCustomView.setVisibility(View.GONE);
                customViewContainer.setVisibility(View.GONE);
                customViewContainer.removeView(mCustomView);
                mCustomViewCallback.onCustomViewHidden();
                mCustomView = null;
            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                super.onGeolocationPermissionsShowPrompt(origin, callback);
                callback.invoke(origin, true, false);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (!TextUtils.isEmpty(title) && getActivity() != null &&
                        !title.contains("https://") && !title.contains("http://")) {
                    if ("Facebook".equals(title)) {
                        toolbar.setTitle(getString(R.string.newsfeed));
                    } else if (webView.getUrl() != null && webView.getUrl().contains("facebook.com/help"))
                        toolbar.setTitle(getString(R.string.app_name));
                    else {
                        toolbar.setTitle(title);
                    }
                }
            }
        });
    }


    boolean hasStoragePermission(String permission) {
        return ContextCompat.checkSelfPermission(mActivity, permission) == 0;
    }

    boolean isSetPhotoViewer4PhotoSet = true;
    boolean firstTimeVisitViewerSet;
    boolean isSetUp4SinglePhoto = true;
    ArrayList<String> listUrlsPhotoViewerSet;
    Set<String> visitedPages;

    void setUpJSCollectionPhotoViewer(long delay) {
        webView.postDelayed(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript: setTimeout(function() {\n" +
                        "    var t = document.querySelectorAll('div._i81');\n" +
                        "  \tvar title_tag = document.querySelector('div._26p9');\n" +
                        "  \tvar title;\n" +
                        "  \tif (title_tag) {\n" +
                        "                title = title_tag.textContent;\n" +
                        "    }else {title = \"\"}\n" +
                        "  \tconsole.log(title);\n" +
                        "    for (var i = 0; i < t.length; i++) {\n" +
                        "        var img_tags = t[i].getElementsByTagName('img');\n" +
                        "        if (img_tags.length > 0 && !t[i].hasAttribute(\"lite_open_photo\")) {\n" +
                        "            var img_tag = img_tags[0];\n" +
                        "            adv.addPhoto(img_tag.getAttribute('src'));\n " +
                        "            t[i].setAttribute(\"lite_open_photo\", \"yes\");\n" +
                        "            \n" +
                        "            \n" +
                        "            (function(img_tag) {\n" +
                        "                t[i].addEventListener('click', function(event) {\n" +
                        "                    event.preventDefault();\n" +
                        "                    console.log(title);\n" +
                        "                    adv.openPhoto(img_tag.getAttribute('src'), title);\n" +
                        "                });\n" +
                        "            })(img_tag);\n" +
                        "        }\n" +
                        "    }\n" +
                        "}, 400);");
            }
        }, delay);
    }

    boolean isDownloadVideo = false;
    String currentUrlVideo;
    Set<String> askedVideos;
    Set<String> firstTimeVideos;

    void set4Videos(long delayTime) {
        webView.postDelayed(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript: setTimeout(function() {\n" +
                        "            var t = document.querySelectorAll('div._5t8z._4gbt > .widePic');\n" +
                        "\t\t\tif (t.length == 0) {\n" +
                        "\t\t\t\tconsole.log('video container not found');\n" +
                        "\t\t\t}\n" +
                        "            for (var i = 0; i < t.length; i++) {\n" +
                        "                var data_tag = t[i].firstElementChild;\n" +
                        "                if (!t[i].hasAttribute(\"lite_open_video\")) {\n" +
                        "\t\t\t\t\t\tconsole.log(data_tag.getAttribute('data-store'));\n" +
                        "\t\t\t\t\t\tt[i].setAttribute(\"lite_open_video\", \"yes\");\n" +
                        "                        (function(data_tag) {\n" +
                        "                                t[i].addEventListener('click', function(event) {\n" +
                        "                                    event.preventDefault();\n" +
                        "\t\t\t\t\t\t\t\t\tconsole.log(data_tag.getAttribute('data-store'));\n" +
                        "                                    adv.onVideoClicked(data_tag.getAttribute('data-store'))\n" +
                        "                                });\n" +
                        "                        })(data_tag);\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t\telse {\n" +
                        "\t\t\t\t\tconsole.log('video not found');\n" +
                        "\t\t\t\t}\n" +
                        "            }\n" +
                        "                }, 337);");
            }
        }, delayTime);
    }

    /*
     * JavaScript Interface. Web code can access methods in here
     * (as long as they have the @JavascriptInterface annotation)
     */
    public class WebViewJavaScriptInterface {

        private Context context;

        /*
         * Need a reference to the context in order to sent a post message
         */
        public WebViewJavaScriptInterface(Context context) {
            this.context = context;
        }

        /*
         * This method can be called from Android. @JavascriptInterface
         * required after SDK version 17.
         */
        @JavascriptInterface
        public void makeToast(String message, boolean lengthLong) {
            Toast.makeText(context, message, (lengthLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
        }

        @JavascriptInterface
        public void onVideoClicked(String json) {
            if (!AppPreferences.INSTANCE.isAskToDownloadVideo())
                return;
            try {
                JSONObject object = new JSONObject(json);
                String src = object.getString("src");
                if (!firstTimeVideos.contains(src)) {
                    firstTimeVideos.add(src);
                    return;
                }
                if (!askedVideos.contains(src)) askDownloadVideo(src);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public boolean setVideoUrl(String videoUrl) {
            Log.e("JavaScriptInterface", videoUrl);
            return true;
        }

        @JavascriptInterface
        public boolean openPhoto(String url, String title) {
            if (TextUtils.isEmpty(title) && webView != null) {
                try {
                    title = (String) mActivity.getTitle();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Intent intent = new Intent(mActivity, PagerPhotoViewerActivity.class);
            int index = -1;
            if (listUrlsPhotoViewerSet != null && (index = listUrlsPhotoViewerSet.indexOf(url)) > -1) {
                intent.putExtra(PagerPhotoViewerActivity.PHOTOS_URL, listUrlsPhotoViewerSet);
                intent.putExtra(PagerPhotoViewerActivity.START_POS, index);
            } else {
                ArrayList<String> urls = new ArrayList<>();
                urls.add(url);
                intent.putExtra(PagerPhotoViewerActivity.PHOTOS_URL, urls);
                intent.putExtra(PagerPhotoViewerActivity.START_POS, 0);
            }
            intent.putExtra(PagerPhotoViewerActivity.TITLE_STRING, title);

            startActivityForResult(intent, 777);
            return true;
        }

        @JavascriptInterface
        public boolean addPhoto(String url) {
            if (listUrlsPhotoViewerSet == null)
                listUrlsPhotoViewerSet = new ArrayList<>();
            if (!listUrlsPhotoViewerSet.contains(url))
                listUrlsPhotoViewerSet.add(url);

            if (!listUrlsPhotoViewerSet.isEmpty() && btnFullscreen.getVisibility() != View.VISIBLE) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnFullscreen.setVisibility(View.VISIBLE);
                    }
                });
            }
            return true;
        }

        @JavascriptInterface
        public void getNotifications(final String number) {
            try {
                final int num = Integer.parseInt(number);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setNotificationNum(num);
                    }
                });

            } catch (NumberFormatException e) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setNotificationNum(0);
                    }
                });

            }
        }

        @JavascriptInterface
        public void getMessages(final String number) {
            try {
                final int num = Integer.parseInt(number);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new UiEvent.UpdateBadgeMessageCount(num));
                    }
                });

            } catch (NumberFormatException e) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new UiEvent.UpdateBadgeMessageCount(0));
                    }
                });

            }
        }
    }

    public void setNotificationNum(int num) {
        if (mActivity == null)
            return;
        if (num > 0 && mNotificationButton != null) {
            ActionItemBadge.update(mNotificationButton, ResourcesCompat.getDrawable(mActivity.getResources(), R.drawable.ic_notify_white, null), num);
        } else if (mNotificationButton != null) {
            ActionItemBadge.update(mNotificationButton, ResourcesCompat.getDrawable(mActivity.getResources(), R.drawable.ic_notify_white, null), Integer.MIN_VALUE);
        }
    }

    void askDownloadVideo(final String url) {
        isDownloadVideo = false;
        currentUrlVideo = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        final boolean[] checked = new boolean[1];
        checked[0] = false;
        boolean[] selected = new boolean[1];
        Arrays.fill(selected, Boolean.FALSE);

        builder.setTitle(R.string.ask_download_video)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        downloadCurrentVIDEOUrl(url);
                        askedVideos.add(url);
                        dialog.dismiss();
                    }
                })
                .setMultiChoiceItems(R.array.dont_ask_video_items, selected, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        checked[0] = isChecked;
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Log.e("setOnDismissListener", "checked: " + checked[0]);
                if (checked[0])
                    askedVideos.add(url);
            }
        }).show();
    }

    void downloadCurrentVIDEOUrl(String url) {
        //PermissionChecker
        if (!hasStoragePermission("android.permission.WRITE_EXTERNAL_STORAGE")) {
            String[] permissions = new String[REQUEST_STORAGE];
            permissions[0] = "android.permission.WRITE_EXTERNAL_STORAGE";
            requestPermissions(permissions, REQUEST_STORAGE);

            isDownloadVideo = true;
            currentUrlVideo = url;
            Toast.makeText(mActivity, "No permission to write on storage!", Toast.LENGTH_LONG).show();
            return;
        }
        if (!Utils.isDownloadManagerAvailable(mActivity)) {
            downloadByAsyncTask(url);
            return;
        }

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Facebook Video");
        request.setTitle("Downloading by Lite");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        Calendar calendar = Calendar.getInstance();
        StringBuilder fileName = new StringBuilder();
        fileName.append(calendar.get(Calendar.YEAR));
        fileName.append("-" + calendar.get(Calendar.MONTH));
        fileName.append("-" + calendar.get(Calendar.DAY_OF_MONTH));
        fileName.append("-" + calendar.get(Calendar.HOUR_OF_DAY));
        fileName.append("-" + calendar.get(Calendar.MINUTE));
        fileName.append("-" + calendar.get(Calendar.SECOND));
        fileName.append(".mp4");
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName.toString());

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) mActivity.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

        Snackbar.make(root, getString(R.string.downloading), Snackbar.LENGTH_SHORT).show();
    }

    ProgressDialog mProgressDialog;
    void downloadByAsyncTask(String imgUrl) {
        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(mActivity);
        mProgressDialog.setMessage("A message");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        // execute this when the downloader must be fired
        final DownloadTask downloadTask = new DownloadTask(mActivity);
        downloadTask.execute(imgUrl);

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });
    }

    class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            FileOutputStream f = null;
            InputStream in = null;
            HttpURLConnection c = null;
            try {
                Calendar calendar = Calendar.getInstance();
                StringBuilder fileName = new StringBuilder();
                fileName.append(calendar.get(Calendar.YEAR));
                fileName.append("-" + calendar.get(Calendar.MONTH));
                fileName.append("-" + calendar.get(Calendar.DAY_OF_MONTH));
                fileName.append("-" + calendar.get(Calendar.HOUR_OF_DAY));
                fileName.append("-" + calendar.get(Calendar.MINUTE));
                fileName.append("-" + calendar.get(Calendar.SECOND));
                fileName.append(".mp4");
                String RootDir = Environment.getExternalStorageDirectory()
                        + File.separator + "Video";
                File RootFile = new File(RootDir);
                RootFile.mkdir();
                // File root = Environment.getExternalStorageDirectory();
                URL u = new URL(sUrl[0]);
                c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                int fileLength = c.getContentLength();
                int total = 0;
                byte[] buffer = new byte[1024];
                int len1 = 0;

                f = new FileOutputStream(new File(RootFile,
                        fileName.toString()));
                in = c.getInputStream();
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                    total += len1;
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));

                }

            } catch (final Exception e) {
                Log.e("DownloadTask", e.getMessage());
                return e.toString();
            } finally {
                try {
                    if (f != null)
                        f.close();
                    if (in != null)
                        in.close();
                    if (c != null)
                        c.disconnect();

                } catch (Exception e) {
                }
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null)
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG);
            else
                Toast.makeText(context, "File saved in Download folder", Toast.LENGTH_LONG).show();
        }

    }

    static final int REQUEST_STORAGE = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isDownloadVideo && !TextUtils.isEmpty(currentUrlVideo)) {
                        downloadCurrentVIDEOUrl(currentUrlVideo);
                    } else if (fabUploadPhoto) {
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(
                                Intent.createChooser(intent, "Select Images"), SELECT_FILE);
                        fabUploadPhoto = false;
                    } else if (fabUploadVideo) {
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("video/*");
                        startActivityForResult(Intent.createChooser(intent, "Select Video"), SELECT_VIDEO);
                        fabUploadVideo = false;
                    }
                } else {
                    Snackbar.make(root, getString(R.string.permission_not_granted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Fab Button: Post status, Upload photos - video, Check-ins
     */
    int REQUEST_CAMERA = 0, SELECT_FILE = 1, SELECT_VIDEO = 2;

    void selectImage() {
        final CharSequence[] items = { getString(R.string.select_photo_camera), getString(R.string.select_photo_library),
                getString(R.string.cancel) };
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(R.string.update_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (item == 1) {
                    if (!hasStoragePermission("android.permission.READ_EXTERNAL_STORAGE")) {
                        String[] permissions = new String[REQUEST_STORAGE];
                        permissions[0] = "android.permission.READ_EXTERNAL_STORAGE";
                        requestPermissions(permissions, REQUEST_STORAGE);
                        fabUploadPhoto = true;
                        return;
                    }
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Images"),
                            SELECT_FILE);
                } else if (item == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**** this method used for select image From Gallery *****/
    void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = mActivity.managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap thumbnail;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        thumbnail = BitmapFactory.decodeFile(selectedImagePath, options);
        sharePhotoDialog(thumbnail);
    }

    void onSelectVideoResult(Intent data) {
        Uri selectedVideoLocation = data.getData();

        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = mActivity.managedQuery(selectedVideoLocation, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        File videoFile = new File(selectedImagePath);

        ShareVideo video = new ShareVideo.Builder()
                .setLocalUrl(Uri.fromFile(videoFile))
                .build();

        ShareVideoContent content = new ShareVideoContent.Builder()
                .setVideo(video)
                .build();

        ShareDialog shareDialog = new ShareDialog(this);
        if (ShareDialog.canShow(ShareVideoContent.class)) {
            shareDialog.show(content);
        } else {
            ShareApi.share(content, null);
        }
    }

    /*** this method used for take profile photo *******/

    void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sharePhotoDialog(thumbnail);
    }

    // This method is used to share Image on facebook timeline.
    void sharePhotoDialog(Bitmap imagePath) {
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(imagePath).setUserGenerated(true)
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareDialog shareDialog = new ShareDialog(this);
        if (ShareDialog.canShow(SharePhotoContent.class)) {
            shareDialog.show(content);
        } else {
            ShareApi.share(content, null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        webView.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == SELECT_VIDEO)
                onSelectVideoResult(data);
        }
    }

    @Subscribe
    public void onEventMainThread(UiEvent.UpdateTextSizeEvent e) {
        setupTextSize(e.font);
    }

    @Subscribe
    public void onEventMainThread(UiEvent.BlockPhotoEvent e) {
        if (preferences.getBoolean("no_images", false)) {
            webView.getSettings().setLoadsImagesAutomatically(false);
        } else {
            webView.getSettings().setLoadsImagesAutomatically(true);
        }
        webView.reload();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.ShowHideFABEvent e) {
        showHideFAB();
    }

    void setupTextSize(String font) {
        boolean defaultfont = font.equals("default_font");
        boolean mediumfont = font.equals("medium_font");
        boolean largefont = font.equals("large_font");
        boolean xlfont = font.equals("xl_font");
        boolean xxlfont = font.equals("xxl_font");
        boolean smallfont = font.equals("small_font");

        if (defaultfont)
            webView.getSettings().setTextZoom(100);

        if (smallfont)
            webView.getSettings().setTextZoom(90);

        if (mediumfont)
            webView.getSettings().setTextZoom(105);

        if (largefont)
            webView.getSettings().setTextZoom(110);

        if (xlfont)
            webView.getSettings().setTextZoom(120);

        if (xxlfont)
            webView.getSettings().setTextZoom(150);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.removeAllViews();
            webView.destroy();
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @BindView(R.id.fab)
    FloatingActionMenu mFab;
    boolean fabUploadPhoto = false;
    boolean fabUploadVideo = false;

    void showHideFAB() {
        if (preferences.getBoolean("show_fab", false)) {
            mFab.setVisibility(View.VISIBLE);
        } else {
            mFab.setVisibility(View.GONE);
        }
    }

    final View.OnClickListener mFABClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.jumpFab:
                    webView.loadUrl("javascript:scroll(0,0)");
                    break;
                case R.id.photoFab:
                    selectImage();
                    break;
                case R.id.updateFab:
                    ShareLinkContent.Builder post = new ShareLinkContent.Builder();
                    post = post.setContentUrl(Uri.parse(""));
                    post.setContentTitle("");
                    ShareDialog.show(HomeActivity.context, post.build());
                    break;
                default:
                    break;
            }
            mFab.close(true);
        }
    };


    int beforePosition;

    @Override
    public void onScroll(int l, int t) {
        if (preferences.getBoolean("show_fab", false)) {
            int step = t - beforePosition;
            if (step > 0)
                mFab.hideMenu(false);
            else
                mFab.showMenu(false);
        }
        beforePosition = t;
    }
}