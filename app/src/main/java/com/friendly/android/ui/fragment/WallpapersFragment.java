package com.friendly.android.ui.fragment;//

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.ui.FavoritesActivity;
import com.friendly.android.ui.HomeActivity;
import com.friendly.android.utils.TrackerScreen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//
//  Tripi
//
//  Created by Quang Tran on 2/17/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class WallpapersFragment extends BaseFragment implements HomeActivity.HeaderControl {

    HomeActivity mActivity;

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    WallpapersAdapter adapter;

    @Override
    public void onBackPressed() {
        mActivity.popFragments(true);
    }

    public WallpapersFragment() {

    }

    @Override
    public void onHomeHeaderUpdate(Toolbar toolbar) {
        super.onHomeHeaderUpdate(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });
        toolbar.setTitle(R.string.menu_explore);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallpapers, null);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mActivity = (HomeActivity) getActivity();

        TrackerScreen.openLiteScreen(mActivity, "WallpapersFragment");

        // Assigning ViewPager View and setting the adapter
        if (pager != null) {
            setupViewPager(pager);
        }

        tabLayout.setupWithViewPager(pager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
        }

        pager.setCurrentItem(1);
    }

    void setupViewPager(ViewPager viewPager) {
        adapter = new WallpapersAdapter(mActivity, getChildFragmentManager());
        adapter.addFragment(new CategoriesFragment(), getString(R.string.menu_categories));
        adapter.addFragment(new RecentFragment(), getString(R.string.menu_recent));
        adapter.addFragment(new PopularPhotoFragment(), getString(R.string.menu_daily_popular));
        adapter.addFragment(new ShuffleFragment(), getString(R.string.menu_shuffle));
        viewPager.setAdapter(adapter);
    }

    static class WallpapersAdapter extends FragmentStatePagerAdapter {
        Context context;
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public WallpapersAdapter(Context context, FragmentManager fm) {
            super(fm);
            this.context = context;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(context).inflate(R.layout.sliding_tab_title, null);
            TextView tv = (TextView) v.findViewById(R.id.title);
            tv.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/SF-UI-Text-Bold.otf"));
            tv.setText(getPageTitle(position));
            if (position == 0) {
                v.setSelected(true);
            }
            return v;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_wallpapers, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_favorites:
                Intent intent = new Intent(activity, FavoritesActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity.getWindow().setExitTransition(new Explode());
                    activity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
                } else {
                    activity.startActivity(intent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
