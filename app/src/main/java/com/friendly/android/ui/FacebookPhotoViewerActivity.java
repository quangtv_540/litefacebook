package com.friendly.android.ui;//

import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.friendly.android.Constants;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.utils.Connectivity;
import com.friendly.android.utils.ToastAdListener;
import com.friendly.android.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoViewAttacher;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/29/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class FacebookPhotoViewerActivity extends AppCompatActivity {

    private static final int REQUEST_STORAGE = 1;
    private static final String TAG = FacebookPhotoViewerActivity.class.getSimpleName();
    private static String appDirectoryName;
    @BindView(R.id.pictureholder)
    ImageView fullImage;
    PhotoViewAttacher Image;
    String title;
    String url;
    private GlideDrawableImageViewTarget gif;


    boolean isDownloadPhoto = false;

    @BindView(R.id.root)
    View root;

    @SuppressWarnings("ConstantConditions")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_Friendly_Dark);

        setContentView(R.layout.activity_photoviewer_single);
        ButterKnife.bind(this);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        url = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        if (url == null) {
            onBackPressed();
            onDestroy();
        }
        ((TextView) findViewById(R.id.pic_title)).setText(title);
        appDirectoryName = getString(R.string.app_name).replace(" ", " ");
        loadImage();

        LinearLayout downloadPhoto = (LinearLayout) findViewById(R.id.save_image);
        downloadPhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Connectivity.isConnected(FacebookPhotoViewerActivity.this)) {
                    return;
                }
                isDownloadPhoto = true;
                if (!hasStoragePermission()) {
                    String[] permissions = new String[REQUEST_STORAGE];
                    permissions[0] = "android.permission.WRITE_EXTERNAL_STORAGE";
                    ActivityCompat.requestPermissions(FacebookPhotoViewerActivity.this, permissions, REQUEST_STORAGE);
                    return;
                } else {
                    if (Utils.isDownloadManagerAvailable(FacebookPhotoViewerActivity.this)) {
                        downloadPhoto();
                        return;
                    }
                }
            }
        });

        LinearLayout sharePhoto = (LinearLayout) findViewById(R.id.share_image);
        sharePhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Connectivity.isConnected(FacebookPhotoViewerActivity.this)) {
                    return;
                }
                sharePhotos();
            }
        });

        if (FriendlyApplication.isShowAds)
            initAdmob();
        else findViewById(R.id.adBanner).setVisibility(View.GONE);
    }

    // AdMob
    AdView mAdView;
    void initAdmob() {
        mAdView = new AdView(this);
        mAdView.setAdUnitId(Constants.ADMOB_BANNER);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdListener(new ToastAdListener(this));
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.adBanner);
        layout.addView(mAdView);

        mAdView.loadAd(new AdRequest.Builder().build());

        layout.setVisibility(View.VISIBLE);
    }

    void loadImage() {
        if (url.contains("gif")) {
            gif = new GlideDrawableImageViewTarget(fullImage);
            Glide.with(this).load(url).listener(new RequestListener<String, GlideDrawable>() {
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @SuppressWarnings("ConstantConditions")
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    fullImage.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                    findViewById(R.id.photoprogress).setVisibility(View.INVISIBLE);
                    return false;
                }
            }).into(gif);
        } else {
            Glide.with(this).load(url).listener(new RequestListener<String, GlideDrawable>() {
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @SuppressWarnings("ConstantConditions")
                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    fullImage.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                    findViewById(R.id.photoprogress).setVisibility(View.INVISIBLE);
                    return false;
                }
            }).into(fullImage);
            Image = new PhotoViewAttacher(fullImage);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_photo, menu);
        return true;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.photo_copy:
                ClipboardManager clipboard = (ClipboardManager) FacebookPhotoViewerActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newUri(this.getContentResolver(), "URI", Uri.parse(url));
                clipboard.setPrimaryClip(clip);
                Snackbar.make(root, getString(R.string.content_copy_link_done), Snackbar.LENGTH_SHORT).show();
                return true;
            case R.id.photo_open:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                try {
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    finish();
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void onBackPressed() {
        Glide.clear(fullImage);
        fullImage.setImageDrawable(null);
        if (url.contains(".gif")){
            Glide.clear(gif);
            gif.setDrawable(null);
        }
        finish();

    }

    public void onDestroy(){
        Glide.get(this).clearMemory();
        super.onDestroy();
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }

    void downloadPhoto() {
        try {
            File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageStorageDir.exists()) {
                imageStorageDir.mkdirs();
            }
            String imgExtension = ".jpg";

            if (url.contains(".gif")) {
                imgExtension = ".gif";

            } else if (url.contains(".png")) {
                imgExtension = ".png";

            } else if (url.contains(".jpeg")) {
                imgExtension = ".jpeg";
            }
            String file = "IMG_" + System.currentTimeMillis() + imgExtension;
            DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES + File.separator + appDirectoryName, file)
                    .setTitle(file).setDescription(getString(R.string.lbl_save_image))
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            dm.enqueue(request);
            Snackbar.make(root, getString(R.string.downloading), Snackbar.LENGTH_SHORT).show();
        } catch (IllegalStateException ex) {
            Snackbar.make(root, getString(R.string.permission_denied), Snackbar.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Snackbar.make(root, getString(R.string.fb_offline), Snackbar.LENGTH_SHORT).show();
        }
    }

    void sharePhotos() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isDownloadPhoto) {
                        downloadPhoto();
                    } else {
                        sharePhotos();
                    }
                } else {
                    Snackbar.make(root, getString(R.string.permission_not_granted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
