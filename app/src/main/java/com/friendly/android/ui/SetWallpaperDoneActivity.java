package com.friendly.android.ui;//

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.friendly.android.R;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.model.Images;
import com.friendly.android.utils.AppDevices;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

//
//  Tripi
//
//  Created by Quang Tran on 2/14/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class SetWallpaperDoneActivity extends AppCompatActivity {

    BgrPhotoItem bgrPhotoItem;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsing;

    @BindView(R.id.header)
    ImageView header;

    NativeExpressAdView mNativeExpressAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_wallpaper_done);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.nav_back);
        setSupportActionBar(toolbar);

        bgrPhotoItem = getIntent().getParcelableExtra(BgrPhotoItem.KEY);
        if (bgrPhotoItem == null) {
            finish();
        }

        mNativeExpressAdView = (NativeExpressAdView) findViewById(R.id.adView);

        collapsing.setTitle(bgrPhotoItem.getTitle());
        collapsing.setExpandedTitleColor(getResources().getColor(R.color.transparent));
        collapsing.setCollapsedTitleTextColor(getResources().getColor(R.color.white));

        updateHeaders();

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        // Optionally populate the ad request builder.
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        // Start loading the ad.
        mNativeExpressAdView.loadAd(adRequestBuilder.build());
    }

    void updateHeaders() {
        if (bgrPhotoItem != null) {
            Images images = bgrPhotoItem.getImagesHashMap().get("image");
            if (images == null) {
                images = bgrPhotoItem.getImagesHashMap().get("preview");
            }
            if (images != null) {
                Picasso.with(this)
                        .load(images.getUrl())
                        .resize(AppDevices.getDeviceWidth(), AppDevices.dp(320))
                        .centerCrop()
                        .into(header);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Resume the NativeExpressAdView.
        mNativeExpressAdView.resume();
    }

    @Override
    public void onPause() {
        // Pause the NativeExpressAdView.
        mNativeExpressAdView.pause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Destroy the NativeExpressAdView.
        mNativeExpressAdView.destroy();
        super.onDestroy();
    }
}
