package com.friendly.android.ui.fragment;//

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.friendly.android.R;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 10/12/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class TutorialFragment extends Fragment {

    public static final String TEXT_KEY = "tutorial_text_key";
    public static final String TITLE_KEY = "tutorial_title_key";
    private static final String PAGE = "page";
    private int textResourceId;
    private int titleResourceId;
    private TextView textView;
    private int mPage;
    private TextView titleView;

    public static TutorialFragment instantiate(int textId, int titleId, int page) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle params = new Bundle();
        params.putInt(TEXT_KEY, textId);
        params.putInt(PAGE, page);
        params.putInt(TITLE_KEY, titleId);
        fragment.setArguments(params);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.textResourceId = getArguments().getInt(TEXT_KEY);
            this.titleResourceId = getArguments().getInt(TITLE_KEY);
            this.mPage = getArguments().getInt(PAGE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layoutResId;
        switch (mPage) {
            case 0:
                layoutResId = R.layout.fragment_tutorial1;
                break;
            case 1:
                layoutResId = R.layout.fragment_tutorial2;
                break;

            default:
                layoutResId = R.layout.fragment_tutorial3;
        }
        View view =  inflater.inflate(layoutResId, container, false);
        view.setTag(mPage);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //imgView = (ImageView) view.findViewById(R.id.image_view_tutorial);
        textView = (TextView) view.findViewById(R.id.text_view_tutorial);
        textView.setText(textResourceId);
        titleView = (TextView) view.findViewById(R.id.tutorial_title);
        titleView.setText(titleResourceId);
        //Glide.with(getActivity()).load(imageDrawableId)
        //.centerCrop().into(imgView);
    }
}
