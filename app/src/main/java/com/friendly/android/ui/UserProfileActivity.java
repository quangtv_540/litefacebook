package com.friendly.android.ui;//

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.adapter.RecentPhotoAdapter;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.model.BgrPhotoUsers;
import com.friendly.android.model.BgrUserHeaders;
import com.friendly.android.model.Images;
import com.friendly.android.model.User;
import com.friendly.android.service.BgrApi;
import com.friendly.android.utils.AppDevices;
import com.friendly.android.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

//
//  Tripi
//
//  Created by Quang Tran on 2/9/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class UserProfileActivity extends AppCompatActivity {

    User user;
    BgrUserHeaders bgrUserHeaders;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsing;

    @BindView(R.id.header)
    ImageView header;
    @BindView(R.id.avatar)
    ImageView avatar;
    @BindView(R.id.followers)
    TextView followers;
    @BindView(R.id.followings)
    TextView followings;
    @BindView(R.id.backgrounds)
    TextView backgrounds;
    @BindView(R.id.homepage)
    TextView homepage;
    @BindView(R.id.description)
    TextView description;

    List<BgrPhotoItem> bgrPhotoItems;
    @BindView(R.id.photos)
    RecyclerView recyclerView;
    RecentPhotoAdapter photoAdapter;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    String last_pos;

    boolean isLoading, isFinished;

    GridLayoutManager gridLayoutManager;
    int visibleItemCount, totalItemCount, pastVisiblesItems, maxInvisibleItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.nav_back);
        setSupportActionBar(toolbar);

        user = getIntent().getParcelableExtra(User.KEY);
        if (user == null) {
            finish();
        }

        if (bgrPhotoItems == null) {
            bgrPhotoItems = new ArrayList<>();
        }

        String url = user.getAvatar().getUrl();

        Picasso.with(this)
                .load(url)
                .resize(AppDevices.dp(60), AppDevices.dp(60))
                .centerCrop()
                .into(avatar);

        toolbar.setTitle(user.getName());
        collapsing.setTitle(user.getName());

        setups();

        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        maxInvisibleItems = 8;

        photoAdapter = new RecentPhotoAdapter(bgrPhotoItems, this);
        recyclerView.setAdapter(photoAdapter);

        recyclerView.addOnScrollListener(
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (isLoading || isFinished || totalItemCount == 0 || TextUtils.isEmpty(last_pos))
                            return;
                        visibleItemCount = gridLayoutManager.getChildCount();
                        pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                        if (visibleItemCount + pastVisiblesItems + maxInvisibleItems > totalItemCount) {
                            if (!Utils.isInternetAvailable(UserProfileActivity.this)){
                                isLoading = false;
                                return;
                            }
                            new LoadListNextPhotoByUsers().execute();
                        }
                    }
                });

        new LoadListPhotoByUsers().execute();

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Utils.isInternetAvailable(UserProfileActivity.this)){
                    Snackbar.make(findViewById(R.id.root), "Refresh failed! Please check your internet connection.", Snackbar.LENGTH_SHORT);
                    return;
                }

                new LoadListPhotoByUsers().execute();
            }
        });
    }

    void setups() {
        followers.setText(String.format("%s followers", user.getFollowers_count()));
        followings.setText(String.format("%s followings", user.getFollowings_count()));
        backgrounds.setText(String.format("%s photos", user.getBackgrounds_count()));
        homepage.setText(user.getHomepage());
        description.setText(user.getDescription());
        description.setVisibility(!TextUtils.isEmpty(user.getDescription()) ? View.VISIBLE : View.GONE);
    }

    void updateHeaders() {
        String cover = user.getAvatar().getUrl();
        if (bgrPhotoItems != null && !bgrPhotoItems.isEmpty()) {
            int position = new Random().nextInt(bgrPhotoItems.size()-1);
            BgrPhotoItem bgrPhotoItem = bgrPhotoItems.get(position);
            Images images = bgrPhotoItem.getImagesHashMap().get("preview");
            if (images == null) {
                images = bgrPhotoItem.getImagesHashMap().get("image");
            }
            if (images != null)
                cover = images.getUrl();
        }

        if (bgrUserHeaders != null && bgrUserHeaders.getCover() != null &&
                !TextUtils.isEmpty(bgrUserHeaders.getCover().getUrl())) {
            cover = bgrUserHeaders.getCover().getUrl();
        }

        if (!TextUtils.isEmpty(cover)) {
            Picasso.with(this)
                    .load(cover)
                    .resize(AppDevices.getDeviceWidth(), AppDevices.dp(260))
                    .centerCrop()
                    .into(header);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class LoadListPhotoByUsers extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoUsers> bgrPhotoUsersCall = BgrApi.getBgrService().listPhotoByUsers(user.getUsername());
                if (bgrPhotoUsersCall != null) {
                    BgrPhotoUsers bgrPhotoUsers = bgrPhotoUsersCall.execute().body();
                    if (bgrPhotoUsers != null) {
                        if (bgrPhotoUsers.getHeaders() != null && !bgrPhotoUsers.getHeaders().isEmpty()) {
                            bgrUserHeaders = bgrPhotoUsers.getHeaders().get(0);
                        }
                        last_pos = Utils.decodeUrl(bgrPhotoUsers.getNext()).getString("last_pos", "");
                        if (bgrPhotoUsers.getBgrPhotoItems() != null && !bgrPhotoUsers.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotoUsers.getBgrPhotoItems();
                        }
                    }
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            isLoading = false;
            updateHeaders();
            if (params != null && !params.isEmpty()) {
                bgrPhotoItems.clear();
                bgrPhotoItems.addAll(params);
                photoAdapter.notifyDataSetChanged();
            } else if (!Utils.isInternetAvailable(UserProfileActivity.this)) {
            }
            totalItemCount = bgrPhotoItems.size();
            mSwipeRefreshLayout.setRefreshing(false);
            Log.e("BGR", "Size: " + bgrPhotoItems.size());
            super.onPostExecute(params);
        }
    }

    class LoadListNextPhotoByUsers extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoUsers> bgrPhotoUsersCall = BgrApi.getBgrService().listNextPhotoByUsers(user.getUsername(), last_pos);
                if (bgrPhotoUsersCall != null) {
                    BgrPhotoUsers bgrPhotoUsers = bgrPhotoUsersCall.execute().body();
                    if (bgrPhotoUsers != null) {
                        last_pos = Utils.decodeUrl(bgrPhotoUsers.getNext()).getString("last_pos", "");
                        if (bgrPhotoUsers.getBgrPhotoItems() != null && !bgrPhotoUsers.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotoUsers.getBgrPhotoItems();
                        } else {
                            isFinished = true;
                        }
                    } else {
                        isFinished = true;
                    }
                } else {
                    isFinished = true;
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            isLoading = false;
            if (params != null && !params.isEmpty()) {
                bgrPhotoItems.addAll(params);
                photoAdapter.notifyDataSetChanged();
            } else if (!Utils.isInternetAvailable(UserProfileActivity.this)) {
            }
            totalItemCount = bgrPhotoItems.size();
            super.onPostExecute(params);
        }
    }
}
