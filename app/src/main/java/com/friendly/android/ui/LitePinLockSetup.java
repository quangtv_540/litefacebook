package com.friendly.android.ui;


//
// Lite for Facebook & Instagram
//
//
// Created by Quang Tran on 10/14/16.
// Copyright (C) 2015 QQFresh Studio, Inc. All rights reserved.
//

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.friendly.android.R;
import com.friendly.android.pinlock.SetPinActivity;
import com.friendly.android.preferences.AppPreferences;

public class LitePinLockSetup extends SetPinActivity {

    private TextView descriptionText;
    private EditText editText1;
    private EditText editText2;
    private Button buttonOk;
    private Button buttonCancel;

    @Override
    public void onPinSet(String pin) {

        AppPreferences.INSTANCE.putString("lockcode", pin);

        AlertDialog.Builder clear = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        clear.setTitle(getResources().getString(R.string.set_pin_success_title));
        clear.setMessage(String.format(getResources().getString(R.string.set_pin_success_desc), pin));
        clear.setPositiveButton(R.string.btn_got_it, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        clear.show();
    }
}
