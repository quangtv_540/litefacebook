package com.friendly.android.ui.fragment;//

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.friendly.android.AppConfig;
import com.friendly.android.Constants;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.event.UiEvent;
import com.friendly.android.manager.FriendlyListener;
import com.friendly.android.manager.Theme;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.ui.HomeActivity;
import com.friendly.android.ui.SettingsActivity;
import com.friendly.android.utils.AppDevices;
import com.friendly.android.utils.Connectivity;
import com.friendly.android.utils.SdkUtils;
import com.friendly.android.utils.TrackerScreen;
import com.friendly.android.utils.Utils;
import com.friendly.android.views.FriendlyWebView;
import com.friendly.android.views.FriendlyWebViewClient;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class MessengerFragment extends BaseFragment implements HomeActivity.HeaderControl, FriendlyWebView.OnScrollChangedCallback {

    HomeActivity mActivity;

    View mCustomView;
    @BindView(R.id.fullscreen_custom_content)
    FrameLayout customViewContainer;
    WebChromeClient.CustomViewCallback mCustomViewCallback;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.friendlyWebView)
    FriendlyWebView webView;

    Toolbar toolbar;

    boolean isOnInbox = true;
    boolean isOnChat = false;
    boolean isLoadingMoreThread = false;
    long lastTimeReadMessageDetails;
    boolean isLoadingMessage = false;

    float currentScale = 1;

    @Subscribe
    public void onEventMainThread(UiEvent.SelectTabMessageEvent e) {
        if (webView.canGoBack() && Connectivity.isConnected(mActivity)) {
            webView.goBack();
            return;
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack() && Connectivity.isConnected(mActivity)) {
            webView.goBack();
            isOnChat = false;
            return;
        }
        mActivity.popFragments(true);
    }

    public MessengerFragment() {
    }

    @Override
    public void onHomeHeaderUpdate(Toolbar toolbar) {
        super.onHomeHeaderUpdate(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webView.canGoBack()) {
                    webView.goBack();
                    isOnChat = false;
                    return;
                }
                mActivity.onBackPressed();
            }
        });
        this.toolbar = toolbar;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview, null);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mActivity = (HomeActivity) getActivity();

        TrackerScreen.openLiteScreen(mActivity, "MessengerFragment");

        setups();

        webViewSettings();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_inbox, menu);
        menu.findItem(R.id.menu_remove_ads).setVisible(FriendlyApplication.isShowAds);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu) {
        try {
            menu.setGroupVisible(R.id.main_menu_message_group, isOnInbox&&!isOnChat);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_friends_online:
                webView.loadUrl(AppConfig.FB_ONLINE_URL);
                return true;
            case R.id.menu_friends_request:
                if (SdkUtils.hasLollipop())
                    webView.loadUrl(AppConfig.FB_FRIEND_JS_URL);
                else webView.loadUrl(AppConfig.FB_FRIEND_URL);
                return true;
            case R.id.menu_settings:
                mActivity.startActivity(new Intent(mActivity, SettingsActivity.class));
                return true;
            case R.id.menu_remove_ads:
                mActivity.showRemoveAdsOptions(false);
                return true;
            case R.id.action_message_requests:
                webView.loadUrl(AppConfig.FB_MESSAGE_REQUESTS_URL);
                return true;
            case R.id.action_filtered_messages:
                webView.loadUrl(AppConfig.FB_MESSAGE_FILTERED_URL);
                return true;
            case R.id.action_unread_message:
                webView.loadUrl(AppConfig.FB_MESSAGE_UNREAD);
                return true;
            case R.id.action_spam_messages:
                webView.loadUrl(AppConfig.FB_MESSAGE_SPAM_URL);
                return true;
            case R.id.action_archived_messages:
                webView.loadUrl(AppConfig.FB_MESSAGE_ARCHIVED_URL);
                return true;
            case R.id.menu_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void setups() {
        swipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isOnChat && Build.VERSION.SDK_INT > 18) {
                    swipeRefreshLayout.setRefreshing(true);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }, 400);
                    return;
                }
                webView.reload();
            }
        });

        if (toolbar == null)
            toolbar = mActivity.toolbar;
    }

    void webViewSettings() {
        // Todo set user agent

        webView.getSettings().setUserAgentString(Constants.MESSENGER);

        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setGeolocationDatabasePath(mActivity.getFilesDir().getPath());
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

        webView.setVerticalScrollBarEnabled(false);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        if (Build.VERSION.SDK_INT < 18) {
            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webView.getSettings().setAppCacheMaxSize(5 * 1024 * 1024);
        }
        webView.getSettings().setAppCachePath(mActivity.getCacheDir().getAbsolutePath());
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.setOnScrollChangedCallback(this);
        webView.setListener(mActivity, new FriendlyListener(mActivity, webView));
        webView.addJavascriptInterface(new MessengerScriptInterface(mActivity), "adv");

        setupTextSize(AppPreferences.INSTANCE.getFont());

        if (preferences.getBoolean("no_images", false))
            webView.getSettings().setLoadsImagesAutomatically(false);

        webView.loadUrl("https://m.facebook.com/messages");

        setupWebViewClient();

        setupWebChromeClient();
    }

    void setupWebViewClient() {
        webView.setWebViewClient(new FriendlyWebViewClient() {

            @Override
            public void onScaleChanged(WebView view, float oldScale, float newScale) {
                super.onScaleChanged(view, oldScale, newScale);
                currentScale = newScale;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url != null && url.contains("facebook.com/messages")) {
                    isOnInbox = true;
                } else {
                    isOnInbox = false;
                }
                if (url != null && url.contains("facebook.com/messages/read/?")) {
                    isOnChat = true;
                } else if (url != null) {
                    isOnChat = false;
                }
                mActivity.supportInvalidateOptionsMenu();
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                if (view.canGoBack()) {
                    if (isAdded())
                        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.nav_back));
                } else {
                    if (isAdded())
                        toolbar.setNavigationIcon(null);
                }

                if ((url.startsWith("https://m.facebook.com/messages/read/")
                        || url.startsWith("https://m.facebook.com/messages/thread/"))) {
                    if (!url.contains("last_message_timestamp=")) {
                        lastTimeReadMessageDetails = Calendar.getInstance().getTimeInMillis();
                        isLoadingMessage = true;

                    }
                    isOnChat = true;
                    AppPreferences.INSTANCE.increaseValuesChatInter();
                }

                mActivity.supportInvalidateOptionsMenu();
            }

            @SuppressLint("ResourceAsColor")
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (url.contains("sharer") || url.contains("/composer/") || url.contains("throwback_share_source")) {
                    view.loadUrl(Utils.injectCss("web/showtopbar.css", context));
                }

                String themeCss = "default_messages.css";
                if (mActivity.materialTheme) {
                    themeCss = "materialize.css";
                } else if (mActivity.blackTheme) {
                    themeCss = "black.css";
                } else if (mActivity.draculaTheme) {
                    themeCss = "dark.css";
                }

                String customCss = Utils.getTheme(themeCss);

                String userhref = !TextUtils.isEmpty(AppPreferences.INSTANCE.getUserName())
                        ? AppPreferences.INSTANCE.getUserName() : null;
                if (TextUtils.isEmpty(userhref))
                    userhref = "username";

                customCss = customCss.replace("user_name", userhref);

                int color = preferences.getInt("pref_message_bubble", -16285192);
                customCss = customCss.replace("#bubble_color", String.format("#%06X", (0xFFFFFF & color)))
                        .replace("#bubble_color_border",
                                Theme.INSTANCE.getColorBubbleBorder(String.format("#%06X", (0xFFFFFF & color))));

                view.loadUrl("javascript:function addStyleString(str) { var node = document.createElement('style'); " +
                        "node.innerHTML = str; document.body.appendChild(node); } addStyleString('" + customCss + "');");
            }
        });
    }

    void setupWebChromeClient() {
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 40) {
                    swipeRefreshLayout.setRefreshing(false);
                } else swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }
                mCustomView = view;
                customViewContainer.setVisibility(View.VISIBLE);
                customViewContainer.addView(view);
                mCustomViewCallback = callback;
            }

            @Override
            public void onHideCustomView() {
                super.onHideCustomView();
                if (mCustomView == null)
                    return;
                // hide and remove customViewContainer
                mCustomView.setVisibility(View.GONE);
                customViewContainer.setVisibility(View.GONE);
                customViewContainer.removeView(mCustomView);
                mCustomViewCallback.onCustomViewHidden();
                mCustomView = null;
            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                super.onGeolocationPermissionsShowPrompt(origin, callback);
                callback.invoke(origin, true, false);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (!TextUtils.isEmpty(title) && getActivity() != null &&
                        !title.contains("https://") && !title.contains("http://")) {
                    if ("Facebook".equals(title)) {
                        toolbar.setTitle(getString(R.string.messenger));
                    } else if (webView.getUrl() != null && webView.getUrl().contains("facebook.com/help"))
                        toolbar.setTitle(getString(R.string.app_name));
                    else {
                        toolbar.setTitle(title);
                    }
                }
            }
        });
    }

    /*
     * JavaScript Interface. Web code can access methods in here
     * (as long as they have the @JavascriptInterface annotation)
     */
    public class MessengerScriptInterface {

        private Context context;

        /*
         * Need a reference to the context in order to sent a post message
         */
        public MessengerScriptInterface(Context context) {
            this.context = context;
        }

        /*
         * This method can be called from Android. @JavascriptInterface
         * required after SDK version 17.
         */
        @JavascriptInterface
        public void makeToast(String message, boolean lengthLong) {
            Toast.makeText(context, message, (lengthLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
        }

        @JavascriptInterface
        public void onMessageThreadLoaded () {
            isLoadingMessage = false;
            Log.e("Fast", "doScrollToBottomThreads");
        }
    }

    @Override
    public void onScroll(int l, int t) {
        if (isOnChat && !isLoadingMoreThread)
            if (t == 0) {
                swipeRefreshLayout.setRefreshing(true);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 600);
                return;
            }
        if (isOnInbox &&
                !isOnChat && !isLoadingMoreThread) {
            int height = (int) Math.floor(webView.getContentHeight() * webView.getScale());
            int webViewHeight = webView.getHeight();
            int cutoff = height - webViewHeight - AppDevices.dp(50); // Don't be too strict on the cutoff point
            if (t >= cutoff) {
                isLoadingMoreThread = true;
                webView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isLoadingMoreThread = false;
                    }
                }, 1431);
            }
        }

    }

    @Subscribe
    public void onEventMainThread(UiEvent.BlockPhotoEvent e) {
        if (preferences.getBoolean("no_images", false)) {
            webView.getSettings().setLoadsImagesAutomatically(false);
        } else {
            webView.getSettings().setLoadsImagesAutomatically(true);
        }
        webView.reload();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.ChangeBubbleColorEvent e) {
        webView.reload();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.UpdateTextSizeEvent e) {
        setupTextSize(e.font);
    }

    void setupTextSize(String font) {
        boolean defaultfont = font.equals("default_font");
        boolean mediumfont = font.equals("medium_font");
        boolean largefont = font.equals("large_font");
        boolean xlfont = font.equals("xl_font");
        boolean xxlfont = font.equals("xxl_font");
        boolean smallfont = font.equals("small_font");

        if (defaultfont)
            webView.getSettings().setTextZoom(100);

        if (smallfont)
            webView.getSettings().setTextZoom(90);

        if (mediumfont)
            webView.getSettings().setTextZoom(105);

        if (largefont)
            webView.getSettings().setTextZoom(110);

        if (xlfont)
            webView.getSettings().setTextZoom(120);

        if (xxlfont)
            webView.getSettings().setTextZoom(150);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.removeAllViews();
            webView.destroy();
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        webView.onActivityResult(requestCode, resultCode, data);
    }
}
