package com.friendly.android.ui;//

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.friendly.android.R;


//
//  AsapChat
//
//  Created by Quang Tran on 4/22/2016.
//  Copyright (c) 2015 Superior Studio, Inc. All rights reserved.
//
public class BaseActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {
    }

    protected void showLoading() {
        showLoading(null);
    }

    protected void showLoading(@StringRes int resId) {
        showLoading(getString(resId));
    }

    protected void showLoading(String loadingMessage) {
        final View v = findViewById(R.id.loading_view);
        if (v == null) {
            return;
        }
        TextView message = (TextView) v.findViewById(R.id.loading_title);
        if (TextUtils.isEmpty(loadingMessage)) {
            message.setVisibility(View.GONE);
        } else {
            message.setVisibility(View.VISIBLE);
            message.setText(loadingMessage);
        }
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    v.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    protected void hideLoading() {
        final View v = findViewById(R.id.loading_view);
        if (v == null) {
            return;
        }
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    v.setVisibility(View.GONE);
                }
            });
        }
    }

    public void showAlert(final String title, final String message) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    new AlertDialog.Builder(BaseActivity.this).setTitle(title)
                            .setMessage(message)
                            .setNegativeButton(android.R.string.ok, null).create()
                            .show();
                }
            });
        }
    }

    public void showAlert(@StringRes final int titleResId, @StringRes final int messageResId) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    new AlertDialog.Builder(BaseActivity.this).setTitle(titleResId)
                            .setMessage(messageResId)
                            .setNegativeButton(android.R.string.ok, null).create()
                            .show();
                }
            });
        }
    }

    public void showAlert(final int title, final int message, final int yes, final int no, final AlertListener listener) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                new AlertDialog.Builder(BaseActivity.this).setTitle(title)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (listener != null)
                                    listener.yesOnClick();
                            }
                        }).setNegativeButton(no, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create().show();
            }
        });
    }

    public void showAlertSupport(final int title, final int message, final AlertListener listener) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                new AlertDialog.Builder(BaseActivity.this).setTitle(title)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.btn_support_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (listener != null)
                                    listener.yesOnClick();
                            }
                        }).setNegativeButton(R.string.btn_support_no, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                }).create()
                        .show();
            }
        });
    }

    public AlertListener listener;
    public interface AlertListener {
        public void yesOnClick();
    }

    public interface MultiAlertListener {
        public void yesOnClick();
        public void noOnClick();
    }

    protected void showKeyboard(View target) {
        if (target == null) {
            return;
        }
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(target,
                InputMethodManager.SHOW_IMPLICIT);
    }

    protected void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    protected void showSnackbar(final View view, final int resMessage) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (view != null) {
                    Snackbar snackbar = Snackbar
                            .make(view, getString(resMessage), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }

    protected void showSnackbar(final View view, final String message) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (view != null) {
                    Snackbar snackbar = Snackbar
                            .make(view, message, Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });
    }
}
