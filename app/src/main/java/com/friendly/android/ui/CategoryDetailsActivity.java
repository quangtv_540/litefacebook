package com.friendly.android.ui;//

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.ImageView;

import com.friendly.android.R;
import com.friendly.android.adapter.RecentPhotoAdapter;
import com.friendly.android.model.BgrCategoryItem;
import com.friendly.android.model.BgrPhotoCategories;
import com.friendly.android.model.BgrPhotoCategoryHeaders;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.service.BgrApi;
import com.friendly.android.utils.AppDevices;
import com.friendly.android.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

//
//  Tripi
//
//  Created by Quang Tran on 2/9/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class CategoryDetailsActivity extends AppCompatActivity {

    BgrCategoryItem bgrCategoryItem;
    BgrPhotoCategoryHeaders qhdHeaders;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsing;

    @BindView(R.id.header)
    ImageView header;
    @BindView(R.id.avatar)
    ImageView avatar;

    List<BgrPhotoItem> bgrPhotoItems;
    @BindView(R.id.photos)
    RecyclerView recyclerView;
    RecentPhotoAdapter photoAdapter;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    String last_pos;

    boolean isLoading, isFinished;

    GridLayoutManager gridLayoutManager;
    int visibleItemCount, totalItemCount, pastVisiblesItems, maxInvisibleItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_details);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.nav_back);
        setSupportActionBar(toolbar);

        bgrCategoryItem = getIntent().getParcelableExtra(BgrCategoryItem.KEY);
        if (bgrCategoryItem == null) {
            finish();
        }

        if (bgrPhotoItems == null) {
            bgrPhotoItems = new ArrayList<>();
        }

        Picasso.with(this)
                .load(bgrCategoryItem.getIcon().getUrl())
                .resize(AppDevices.dp(80), AppDevices.dp(80))
                .centerCrop()
                .into(avatar);

        toolbar.setTitle(bgrCategoryItem.getName());
        collapsing.setTitle(bgrCategoryItem.getName());

        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        maxInvisibleItems = 8;

        photoAdapter = new RecentPhotoAdapter(bgrPhotoItems, this);
        recyclerView.setAdapter(photoAdapter);

        recyclerView.addOnScrollListener(
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (isLoading || isFinished || totalItemCount == 0 || TextUtils.isEmpty(last_pos))
                            return;
                        visibleItemCount = gridLayoutManager.getChildCount();
                        pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                        if (visibleItemCount + pastVisiblesItems + maxInvisibleItems > totalItemCount) {
                            if (!Utils.isInternetAvailable(CategoryDetailsActivity.this)){
                                isLoading = false;
                                return;
                            }
                            new LoadListNextPhotoCategories().execute();
                        }
                    }
                });

        new LoadListPhotoCategories().execute();

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Utils.isInternetAvailable(CategoryDetailsActivity.this)){
                    Snackbar.make(findViewById(R.id.root), "Refresh failed! Please check your internet connection.", Snackbar.LENGTH_SHORT);
                    return;
                }

                new LoadListPhotoCategories().execute();
            }
        });
    }

    void updateHeaders() {
        if (qhdHeaders != null) {
            Picasso.with(this)
                    .load(qhdHeaders.getBackground().getUrl())
                    .resize(AppDevices.getDeviceWidth(), AppDevices.dp(210))
                    .centerCrop()
                    .into(header);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class LoadListPhotoCategories extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoCategories> bgrPhotoCategoriesCall = BgrApi.getBgrService().listPhotoByCategories(bgrCategoryItem.getUuid());
                if (bgrPhotoCategoriesCall != null) {
                    BgrPhotoCategories bgrPhotoCategories = bgrPhotoCategoriesCall.execute().body();
                    if (bgrPhotoCategories != null) {
                        if (bgrPhotoCategories.getBgrHeaderses() != null && !bgrPhotoCategories.getBgrHeaderses().isEmpty()) {
                            qhdHeaders = bgrPhotoCategories.getBgrHeaderses().get(0);
                        }
                        last_pos = Utils.decodeUrl(bgrPhotoCategories.getNext()).getString("last_pos", "");
                        if (bgrPhotoCategories.getBgrPhotoItems() != null && !bgrPhotoCategories.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotoCategories.getBgrPhotoItems();
                        }
                    }
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            isLoading = false;
            updateHeaders();
            if (params != null && !params.isEmpty()) {
                bgrPhotoItems.clear();
                bgrPhotoItems.addAll(params);
                photoAdapter.notifyDataSetChanged();
            } else if (!Utils.isInternetAvailable(CategoryDetailsActivity.this)) {
            }
            totalItemCount = bgrPhotoItems.size();
            mSwipeRefreshLayout.setRefreshing(false);
            super.onPostExecute(params);
        }
    }

    class LoadListNextPhotoCategories extends AsyncTask<Void, Void, List<BgrPhotoItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        }

        @Override
        protected List<BgrPhotoItem> doInBackground(Void... params) {
            try {
                Call<BgrPhotoCategories> bgrPhotoCategoriesCall = BgrApi.getBgrService().listNextPhotoByCategories(bgrCategoryItem.getUuid(), last_pos);
                if (bgrPhotoCategoriesCall != null) {
                    BgrPhotoCategories bgrPhotoCategories = bgrPhotoCategoriesCall.execute().body();
                    if (bgrPhotoCategories != null) {
                        last_pos = Utils.decodeUrl(bgrPhotoCategories.getNext()).getString("last_pos", "");
                        if (bgrPhotoCategories.getBgrPhotoItems() != null && !bgrPhotoCategories.getBgrPhotoItems().isEmpty()) {
                            return bgrPhotoCategories.getBgrPhotoItems();
                        } else {
                            isFinished = true;
                        }
                    } else {
                        isFinished = true;
                    }
                } else {
                    isFinished = true;
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<BgrPhotoItem> params) {
            isLoading = false;
            if (params != null && !params.isEmpty()) {
                bgrPhotoItems.addAll(params);
                photoAdapter.notifyDataSetChanged();
            } else if (!Utils.isInternetAvailable(CategoryDetailsActivity.this)) {
            }
            totalItemCount = bgrPhotoItems.size();
            super.onPostExecute(params);
        }
    }
}
