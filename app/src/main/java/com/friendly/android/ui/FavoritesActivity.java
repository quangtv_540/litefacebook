package com.friendly.android.ui;//

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.friendly.android.R;
import com.friendly.android.adapter.FavoritesAdapter;
import com.friendly.android.db.DatabaseManager;
import com.friendly.android.model.BgrPhotoItem;
import com.friendly.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//
//  Tripi
//
//  Created by Quang Tran on 2/14/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class FavoritesActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    List<BgrPhotoItem> bgrPhotoItems;
    @BindView(R.id.photos)
    RecyclerView recyclerView;
    FavoritesAdapter photoAdapter;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.nav_back);
        setSupportActionBar(toolbar);


        if (bgrPhotoItems == null) {
            bgrPhotoItems = new ArrayList<>();
        }

        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        photoAdapter = new FavoritesAdapter(bgrPhotoItems, this);
        recyclerView.setAdapter(photoAdapter);

        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Utils.isInternetAvailable(FavoritesActivity.this)){
                    Snackbar.make(findViewById(R.id.root), "Refresh failed! Please check your internet connection.", Snackbar.LENGTH_SHORT);
                    return;
                }

                bgrPhotoItems.clear();
                loadData();
            }
        });

        if (bgrPhotoItems.isEmpty())
            loadData();
    }

    void loadData() {
        bgrPhotoItems.addAll(DatabaseManager.DB.getPhotos());
        photoAdapter.notifyDataSetChanged();

        mSwipeRefreshLayout.setRefreshing(false);

        findViewById(R.id.emptyView).setVisibility(bgrPhotoItems.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favorites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_delete_all:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Favorites")
                        .setMessage("Are you sure you want to clear all your favorites?");

                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                DatabaseManager.DB.clearAndStopData();
                                loadData();
                            }
                        });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
