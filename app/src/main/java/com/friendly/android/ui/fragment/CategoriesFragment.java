package com.friendly.android.ui.fragment;//

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.friendly.android.R;
import com.friendly.android.adapter.CategoriesAdapter;
import com.friendly.android.model.BgrCategories;
import com.friendly.android.model.BgrCategoryItem;
import com.friendly.android.service.BgrApi;
import com.friendly.android.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//
//  Tripi
//
//  Created by Quang Tran on 2/8/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class CategoriesFragment extends Fragment {

    @BindView(R.id.categories)
    RecyclerView listView;

    List<BgrCategoryItem> bgrCategoryItems;
    LinearLayoutManager mLayoutManager;
    CategoriesAdapter categoriesAdapter;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_categories, container,false);
        ButterKnife.bind(this, v);
        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (bgrCategoryItems == null) {
            bgrCategoryItems = new ArrayList<>();
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(mLayoutManager);

        categoriesAdapter = new CategoriesAdapter(getActivity(), bgrCategoryItems);
        listView.setAdapter(categoriesAdapter);

        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Utils.isInternetAvailable(getActivity())){
                    Snackbar.make(getView(), "Refresh failed! Please check your internet connection.", Snackbar.LENGTH_SHORT);
                    return;
                }

                loadListCategories();
            }
        });

        if (bgrCategoryItems.isEmpty())
            loadListCategories();
    }

    void loadListCategories() {
        if (!Utils.isInternetAvailable(getActivity())){
            return;
        }
        mSwipeRefreshLayout.setRefreshing(true);
        Call<BgrCategories> bgrCategories = BgrApi.getBgrService().lookupBgrCategories();
        if (bgrCategories != null) {
            bgrCategories.enqueue(new Callback<BgrCategories>() {
                @Override
                public void onResponse(Call<BgrCategories> call, Response<BgrCategories> response) {
                    if (response.body().getBgrCategoryItems() != null && !response.body().getBgrCategoryItems().isEmpty()) {
                        bgrCategoryItems.clear();
                        bgrCategoryItems.addAll(response.body().getBgrCategoryItems());
                        Collections.sort(bgrCategoryItems, new Comparator<BgrCategoryItem>() {

                            @Override
                            public int compare(BgrCategoryItem lhs, BgrCategoryItem rhs) {
                                return lhs.getName().compareTo(rhs.getName());
                            }
                        });
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                categoriesAdapter.notifyDataSetChanged();
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<BgrCategories> call, Throwable t) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            });
        }
    }
}
