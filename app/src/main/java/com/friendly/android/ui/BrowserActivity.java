package com.friendly.android.ui;//

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friendly.android.Constants;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.utils.Connectivity;
import com.friendly.android.utils.ToastAdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import butterknife.BindView;
import butterknife.ButterKnife;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/29/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class BrowserActivity extends AppCompatActivity {

    WebView mWebView;
    View mCustomView;
    @BindView(R.id.fullscreen_custom_content)
    FrameLayout customViewContainer;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    android.webkit.WebChromeClient.CustomViewCallback mCustomViewCallback;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_browser);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.browser_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
        }

        ImageView closeButton = (ImageView) findViewById(R.id.upButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.slide_in_right, android.R.anim.slide_out_right);
                finish();
            }
        });

        Uri url = getIntent().getData();

        swipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.gplus_colors));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mWebView.reload();
                if (!Connectivity.isConnected(getApplicationContext()))
                    swipeRefreshLayout.setRefreshing(false);
                else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);

                        }

                    }, 3000);
                }
            }});


        mWebView = (WebView) findViewById(R.id.webview);
        if (Build.VERSION.SDK_INT >= 19) {
            mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mWebView.setWebViewClient(new Callback());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.loadUrl(url.toString());

        if (FriendlyApplication.isShowAds)
            initAdmob();
        else findViewById(R.id.adBanner).setVisibility(View.GONE);
    }

    // AdMob
    AdView mAdView;
    void initAdmob() {
        mAdView = new AdView(this);
        mAdView.setAdUnitId(Constants.ADMOB_BANNER);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdListener(new ToastAdListener(this));
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.adBanner);
        layout.addView(mAdView);

        mAdView.loadAd(new AdRequest.Builder().build());

        layout.setVisibility(View.VISIBLE);
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if ((url.contains("market://")
                    || url.contains("mailto:")
                    || url.contains("play.google")
                    || url.contains("tel:")
                    || url.contains("vid:")
                    || url.contains("youtube")) == true) {
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }
            if ((url.contains("http://") || url.contains("https://"))) {
                return false;
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            try {
                view.getContext().startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Log.e("shouldOverrideUrlLoad", "" + e.getMessage());
                e.printStackTrace();
            }
            return true;

        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            ((TextView) findViewById(R.id.toolbarSub)).setText(url);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            swipeRefreshLayout.setRefreshing(true);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(ProgressBar.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

            }, 2000);
        }
    }

    private class WebChromeClient extends android.webkit.WebChromeClient {

        public void onProgressChanged(WebView view, int progress) {
            if (progress < 100 && progressBar.getVisibility() == ProgressBar.GONE)
                progressBar.setVisibility(ProgressBar.VISIBLE);
            progressBar.setProgress(progress);
            if (progress == 100)
                progressBar.setVisibility(ProgressBar.GONE);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            ((TextView) findViewById(R.id.toolbarTitle)).setText(title);
        }


        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            customViewContainer.setVisibility(View.VISIBLE);
            Toolbar toolbar = (Toolbar) findViewById(R.id.browser_toolbar);
            toolbar.setVisibility(View.GONE);
            customViewContainer.addView(view);
            mCustomViewCallback = callback;
        }


        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            if (mCustomView == null)
                return;
            mCustomView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.GONE);
            Toolbar toolbar = (Toolbar) findViewById(R.id.browser_toolbar);
            toolbar.setVisibility(View.VISIBLE);
            customViewContainer.removeView(mCustomView);
            mCustomViewCallback.onCustomViewHidden();
            mCustomView = null;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_browser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_share:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, R.string.share_action_subject);
                i.putExtra(Intent.EXTRA_TEXT, mWebView.getUrl());
                startActivity(Intent.createChooser(i, getString(R.string.share_action)));
                return true;
            case R.id.menu_open_link:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                try {
                    intent.setData(Uri.parse(mWebView.getUrl()));
                    startActivity(intent);
                    finish();
                } catch (ActivityNotFoundException e) {
                    Log.e("shouldOverrideUrlLoad", "" + e.getMessage());
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
        if (mAdView != null)
            mAdView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        if (mAdView != null)
            mAdView.resume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mWebView.removeAllViews();
        mWebView.destroy();
        if (mAdView != null)
            mAdView.destroy();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.back_slide_in, R.anim.back_slide_out);
    }
}
