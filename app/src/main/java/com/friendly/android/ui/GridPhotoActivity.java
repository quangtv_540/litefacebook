package com.friendly.android.ui;//

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friendly.android.Constants;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.adapter.GridPhotoAdapter;
import com.friendly.android.event.UiEvent;
import com.friendly.android.manager.Theme;
import com.friendly.android.model.InstaPhotos;
import com.friendly.android.utils.Connectivity;
import com.friendly.android.utils.ToastAdListener;
import com.friendly.android.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/30/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class GridPhotoActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_main)
    View root;

    LinearLayoutManager mLayoutManager;

    @BindView(R.id.gridPhotos)
    RecyclerView gridPhotosView;

    GridPhotoAdapter gridPhotoAdapter;

    public static ArrayList<InstaPhotos> instaPhotoses;

    String url;

    boolean isDownloadPhoto = false;
    String appDirectoryName;
    static final int REQUEST_STORAGE = 1;

    public static TextView getToolbarTitleView(GridPhotoActivity activity, Toolbar toolbar) {
        ActionBar actionBar = activity.getSupportActionBar();
        CharSequence actionbarTitle = null;
        if(actionBar != null)
            actionbarTitle = actionBar.getTitle();
        actionbarTitle = TextUtils.isEmpty(actionbarTitle) ? toolbar.getTitle() : actionbarTitle;
        if(TextUtils.isEmpty(actionbarTitle)) return null;
        // can't find if title not set
        for(int i= 0; i < toolbar.getChildCount(); i++){
            View v = toolbar.getChildAt(i);
            if(v != null && v instanceof TextView){
                TextView t = (TextView) v;
                CharSequence title = t.getText();
                if(!TextUtils.isEmpty(title) && actionbarTitle.equals(title) && t.getId() == View.NO_ID){
                    //Toolbar does not assign id to views with layout params SYSTEM, hence getId() == View.NO_ID
                    //in same manner subtitle TextView can be obtained.

                    t.setTypeface(Theme.INSTANCE.getTypeface("fonts/SF-UI-Text-Medium.otf"));
                    t.setTextSize(18);

                    return t;
                }
            }
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        setContentView(R.layout.activity_instagram_grid_photos);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getToolbarTitleView(this, toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.instagram);

        appDirectoryName = getString(R.string.app_name).replace(" ", " ");

        mLayoutManager = new LinearLayoutManager(this);

        gridPhotoAdapter = new GridPhotoAdapter(instaPhotoses, this);

        gridPhotosView.setAdapter(gridPhotoAdapter);
        gridPhotosView.setLayoutManager(mLayoutManager);

        if (FriendlyApplication.isShowAds)
            initAdmob();
        else findViewById(R.id.adBanner).setVisibility(View.GONE);
    }

    // AdMob
    AdView mAdView;
    void initAdmob() {
        mAdView = new AdView(this);
        mAdView.setAdUnitId(Constants.ADMOB_BANNER);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdListener(new ToastAdListener(this));
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.adBanner);
        layout.addView(mAdView);

        mAdView.loadAd(new AdRequest.Builder().build());

        layout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }

    void downloadPhotoOnClick(String url) {
        if (!Connectivity.isConnected(this)) {
            return;
        }
        isDownloadPhoto = true;
        this.url = url;
        if (!hasStoragePermission()) {
            String[] permissions = new String[REQUEST_STORAGE];
            permissions[0] = "android.permission.WRITE_EXTERNAL_STORAGE";
            ActivityCompat.requestPermissions(this, permissions, REQUEST_STORAGE);
            return;
        } else {
            if (Utils.isDownloadManagerAvailable(this)) {
                downloadPhoto(url);
                return;
            }
        }
    }

    void downloadPhoto(String url) {
        try {
            File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageStorageDir.exists()) {
                imageStorageDir.mkdirs();
            }
            String imgExtension = ".jpg";

            if (url.contains(".gif")) {
                imgExtension = ".gif";

            } else if (url.contains(".png")) {
                imgExtension = ".png";

            } else if (url.contains(".jpeg")) {
                imgExtension = ".jpeg";
            }
            String file = "IMG_" + System.currentTimeMillis() + imgExtension;
            DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES + File.separator + appDirectoryName, file)
                    .setTitle(file).setDescription(getString(R.string.lbl_save_image))
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            dm.enqueue(request);
            Snackbar.make(root, getString(R.string.downloading), Snackbar.LENGTH_SHORT).show();
        } catch (IllegalStateException ex) {
            Snackbar.make(root, getString(R.string.permission_denied), Snackbar.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Snackbar.make(root, getString(R.string.fb_offline), Snackbar.LENGTH_SHORT).show();
        }
    }

    void sharePhotoOnClick(String url) {
        if (!Connectivity.isConnected(this)) {
            return;
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    void sharePhotos(String url) {
        final Uri uri = Uri.parse(url);
        // Share image
        com.squareup.picasso.Target target = new com.squareup.picasso.Target() {
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom arg1) {
                String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, uri.getLastPathSegment(), null);

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                startActivity(Intent.createChooser(shareIntent, getString(R.string.context_share_image)));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }
        };

        Picasso.with(this).load(uri).into(target);
        Snackbar.make(root, getString(R.string.context_loading_image_progress), Snackbar.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.DownloadPhotoEvent e) {
        if (e.url != null) {
            downloadPhotoOnClick(e.url);
        }
    }

    @Subscribe
    public void onEventMainThread(UiEvent.SharePhotoEvent e) {
        if (e.url != null) {
            sharePhotoOnClick(e.url);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isDownloadPhoto) {
                        downloadPhoto(url);
                    } else {
                        sharePhotos(url);
                    }
                } else {
                    Snackbar.make(root, getString(R.string.permission_not_granted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.back_slide_in, R.anim.back_slide_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }
}
