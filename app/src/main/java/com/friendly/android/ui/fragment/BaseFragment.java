package com.friendly.android.ui.fragment;//

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.friendly.android.FriendlyApplication;
import com.friendly.android.ui.HomeActivity;

//
//  Tripi
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class BaseFragment extends Fragment {

    protected HomeActivity activity;

    protected static Context context = FriendlyApplication.getContext();

    protected final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (HomeActivity) getActivity();
    }

    public void onHomeHeaderUpdate(Toolbar homeHeader) {
    }

    protected void finish() {
        getActivity().finish();
    }
}
