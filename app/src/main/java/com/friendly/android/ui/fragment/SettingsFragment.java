package com.friendly.android.ui.fragment;//

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.event.UiEvent;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.receiver.BootCompletedIntentReceiver;
import com.friendly.android.service.ReadRssService;
import com.friendly.android.ui.BaseActivity;
import com.friendly.android.ui.LitePinLockSetup;
import com.friendly.android.ui.LoginActivity;
import com.friendly.android.views.preferencecompat.ListPreferenceCompat;
import com.friendly.android.views.preferencecompat.SwitchPreferenceCompat;

import de.greenrobot.event.EventBus;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {

    static final String TAG = SettingsFragment.class.getSimpleName();

    SharedPreferences.OnSharedPreferenceChangeListener myPrefListner;

    static final int REQUEST_STORAGE = 1;

    static Context context = FriendlyApplication.getContext();

    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

    boolean applyChangeThemes = false;

    SwitchPreferenceCompat lock;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        Preference about = findPreference("about_settings");
        Preference rateSettings = findPreference("rate_settings");
        lock = (SwitchPreferenceCompat) findPreference("locker");
        Preference clearCachePref = findPreference("clear");
        Preference logout = findPreference("logout_settings");

        about.setOnPreferenceClickListener(this);
        rateSettings.setOnPreferenceClickListener(this);
        clearCachePref.setOnPreferenceClickListener(this);
        logout.setOnPreferenceClickListener(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(context);

        ListPreference lp = (ListPreference) findPreference("interval_pref");
        String temp1 = getString(R.string.interval_pref_description).replace("%s", "");
        String temp2 = lp.getSummary().toString();
        if (temp1.equals(temp2))
            lp.setValueIndex(2);

        myPrefListner = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                final Intent intent = new Intent(context, ReadRssService.class);
                switch (key) {
                    case "disable_twitter":
                        AppPreferences.INSTANCE.setShowTwitterTab(prefs.getBoolean("disable_twitter", true));
                        EventBus.getDefault().post(new UiEvent.HideTwitterTabEvent());
                        break;
                    case "ask_download_video":
                        AppPreferences.INSTANCE.setAskToDownloadVideo(prefs.getBoolean("ask_download_video", true));
                        break;
                    case "interval_pref":
                        AppPreferences.INSTANCE.setIntervalNotifications(Long.parseLong(prefs.getString("interval_pref", "300000")));
                        if (AppPreferences.INSTANCE.isNotificationEnabled() || AppPreferences.INSTANCE.isMessageNotificationEnable()) {
                            context.stopService(intent);
                            context.startService(intent);
                            BootCompletedIntentReceiver.scheduleAlarms(context, false);
                        }
                        break;
                    case "notifications_activated":
                        AppPreferences.INSTANCE.setNotificationEnabled(prefs.getBoolean("notifications_activated", true));
                        if (prefs.getBoolean("notifications_activated", true) && preferences.getBoolean("messages_activated", true)) {
                            context.stopService(intent);
                            context.startService(intent);
                            BootCompletedIntentReceiver.scheduleAlarms(context, false);
                        } else { //noinspection StatementWithEmptyBody
                            if (!prefs.getBoolean("notifications_activated", true) && AppPreferences.INSTANCE.isMessageNotificationEnable()) {
                                // ignore this case
                            } else if (prefs.getBoolean("notifications_activated", true) && !AppPreferences.INSTANCE.isMessageNotificationEnable()) {
                                context.startService(intent);
                                BootCompletedIntentReceiver.scheduleAlarms(context, false);
                            } else {
                                context.stopService(intent);
                                BootCompletedIntentReceiver.scheduleAlarms(context, true);
                            }
                        }

                        SwitchPreferenceCompat rp = (SwitchPreferenceCompat) findPreference(AppPreferences.NOTIFICATION_RINGTONE_SOUND_KEY);
                        rp.setEnabled(prefs.getBoolean("notifications_activated", true));

                        boolean enable = prefs.getBoolean("notifications_activated", true) || prefs.getBoolean("messages_activated", true);

                        SwitchPreferenceCompat vibrate = (SwitchPreferenceCompat) findPreference("vibrate");
                        SwitchPreferenceCompat led_light = (SwitchPreferenceCompat) findPreference("led_light");
                        vibrate.setEnabled(enable);
                        led_light.setEnabled(enable);
                        break;
                    case "messages_activated":
                        AppPreferences.INSTANCE.setMessageNotificationEnable(prefs.getBoolean("messages_activated", true));
                        if (prefs.getBoolean("messages_activated", true) && AppPreferences.INSTANCE.isNotificationEnabled()) {
                            context.stopService(intent);
                            context.startService(intent);
                            BootCompletedIntentReceiver.scheduleAlarms(context, false);
                        } else {//noinspection StatementWithEmptyBody
                            if (!prefs.getBoolean("messages_activated", true) && AppPreferences.INSTANCE.isNotificationEnabled()) {
                                // ignore this case
                            } else if (prefs.getBoolean("messages_activated", true) && !AppPreferences.INSTANCE.isNotificationEnabled()) {
                                context.startService(intent);
                                BootCompletedIntentReceiver.scheduleAlarms(context, false);
                            } else {
                                context.stopService(intent);
                                BootCompletedIntentReceiver.scheduleAlarms(context, true);
                            }
                        }

                        SwitchPreferenceCompat rpm = (SwitchPreferenceCompat) findPreference(AppPreferences.MESSAGE_RINGTONE_SOUND_KEY);
                        rpm.setEnabled(prefs.getBoolean("messages_activated", true));

                        boolean enableMsg = prefs.getBoolean("notifications_activated", true) || prefs.getBoolean("messages_activated", true);

                        SwitchPreferenceCompat vibrateMsg = (SwitchPreferenceCompat) findPreference("vibrate");
                        SwitchPreferenceCompat led_lightMsg = (SwitchPreferenceCompat) findPreference("led_light");
                        vibrateMsg.setEnabled(enableMsg);
                        led_lightMsg.setEnabled(enableMsg);
                        break;
                    case AppPreferences.NOTIFICATION_RINGTONE_SOUND_KEY:
                        AppPreferences.INSTANCE.setNotificationRingtoneSound(
                                prefs.getBoolean(AppPreferences.NOTIFICATION_RINGTONE_SOUND_KEY, false));
                        break;
                    case AppPreferences.MESSAGE_RINGTONE_SOUND_KEY:
                        AppPreferences.INSTANCE.setMessageRingtoneSound(
                                prefs.getBoolean(AppPreferences.MESSAGE_RINGTONE_SOUND_KEY, false));
                        break;
                    case "vibrate":
                        AppPreferences.INSTANCE.setEnableVibrate(prefs.getBoolean("vibrate", true));
                        break;
                    case "led_light":
                        AppPreferences.INSTANCE.setEnableLedLight(prefs.getBoolean("led_light", true));
                        break;
                    case "show_fab":
                        EventBus.getDefault().post(new UiEvent.ShowHideFABEvent());
                        break;
                    case "locker":
                        if (preferences.getBoolean("locker", false)) {
                            Intent folioLocker = new Intent(getActivity(), LitePinLockSetup.class);
                            startActivity(folioLocker);
                        } else {
                            AppPreferences.INSTANCE.putString("lockcode", "");
                        }
                        break;
                    case "no_images":
                        EventBus.getDefault().post(new UiEvent.BlockPhotoEvent());
                        break;
                    case "pref_message_bubble":
                        EventBus.getDefault().post(new UiEvent.ChangeBubbleColorEvent());
                        break;
                }
            }
        };

        final ListPreferenceCompat themes = (ListPreferenceCompat) findPreference("theme_preference_fb");
        themes.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                ListPreference listPreference = (ListPreference) preference;
                String value = listPreference.getValue();
                if (!TextUtils.isEmpty(value) && !value.equals(newValue.toString())) {
                    applyChangeThemes = true;
                }
                int id = 0;
                for (int i = 0; i < listPreference.getEntryValues().length; i++) {
                    if(listPreference.getEntryValues()[i].equals(newValue.toString())){
                        id = i;
                        break;
                    }
                }
                preference.setSummary(listPreference.getEntries()[id]);
                if (applyChangeThemes) {
                    EventBus.getDefault().post(new UiEvent.UpdateThemeEvent(id));
                }
                return true;
            }
        });

        final ListPreferenceCompat sections = (ListPreferenceCompat) findPreference("section_preference_fb");
        sections.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean applyChangeSection = false;
                ListPreference listPreference = (ListPreference) preference;
                String value = listPreference.getValue();
                if (!TextUtils.isEmpty(value) && !value.equals(newValue.toString())) {
                    applyChangeSection = true;
                }
                int id = 0;
                for (int i = 0; i < listPreference.getEntryValues().length; i++) {
                    if(listPreference.getEntryValues()[i].equals(newValue.toString())){
                        id = i;
                        break;
                    }
                }
                preference.setSummary(listPreference.getEntries()[id]);
                if (applyChangeSection) {
//                    EventBus.getDefault().post(new UiEvent.SectionUpdateEvent(id));
                }
                return true;
            }
        });

        final ListPreferenceCompat fonts = (ListPreferenceCompat) findPreference("font_pref");
        fonts.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean applyChangeFont = false;
                ListPreference listPreference = (ListPreference) preference;
                String value = listPreference.getValue();
                if (!TextUtils.isEmpty(value) && !value.equals(newValue.toString())) {
                    applyChangeFont = true;
                }
                int id = 0;
                for (int i = 0; i < listPreference.getEntryValues().length; i++) {
                    if(listPreference.getEntryValues()[i].equals(newValue.toString())){
                        id = i;
                        break;
                    }
                }
                preference.setSummary(listPreference.getEntries()[id]);
                if (applyChangeFont) {
                    EventBus.getDefault().post(new UiEvent.UpdateTextSizeEvent(newValue.toString()));
                }
                return true;
            }
        });

        preferences.registerOnSharedPreferenceChangeListener(myPrefListner);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();
        switch (key) {
            case "only_wallpapers":
                String url = !TextUtils.isEmpty(AppPreferences.INSTANCE.getPackageWallpapers()) ?
                        AppPreferences.INSTANCE.getPackageWallpapers() : getString(R.string.get_app_store_wallpapers);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;
            case "about_settings":
                new AlertDialog.Builder(getActivity()).setTitle(R.string.title_activity_about)
                        .setMessage(Html.fromHtml(getActivity().getString(R.string.txt_about)))
                        .setNegativeButton(android.R.string.ok, null).create()
                        .show();
                break;
            case "rate_settings":
                Toast.makeText(context, "Thank you for your support!", Toast.LENGTH_LONG).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.get_app_store))));
                break;
            case "clear":
                AlertDialog.Builder clear =
                        new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                clear.setTitle(getResources().getString(R.string.clear_cache_title));
                clear.setMessage(Html.fromHtml(getResources().getString(R.string.clear_cache_message)));
                clear.setPositiveButton(R.string.ok, new AlertDialog.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Toast.makeText(getActivity(), "Cache deleted", Toast.LENGTH_SHORT).show();
                    }
                });
                clear.setNeutralButton(R.string.cancel, null);
                clear.show();
                break;
            case "logout_settings":
                logout();
                break;
        }
        return false;
    }


    private void logout() {
        showAlert(R.string.prefs_logout, R.string.alert_logout_messages, R.string.lbl_yes, R.string.lbl_no, new BaseActivity.AlertListener() {
            @Override
            public void yesOnClick() {
//                setupQuickbar(false);
                if (LoginManager.getInstance() != null) {
                    LoginManager.getInstance().logOut();
                }

                AppPreferences.INSTANCE.setUserName(null);
                AppPreferences.INSTANCE.setUserId(null);

                clearCookies(context);

                final Intent service = new Intent(context, ReadRssService.class);
                context.stopService(service);
                BootCompletedIntentReceiver.scheduleAlarms(context, true);

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("Exit", true);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        preferences.registerOnSharedPreferenceChangeListener(myPrefListner);
    }

    @Override
    public void onStop() {
        super.onStop();
        preferences.unregisterOnSharedPreferenceChangeListener(myPrefListner);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(AppPreferences.INSTANCE.getString("lockcode", ""))) {
            lock.setChecked(false);
        }

        SwitchPreferenceCompat rpmNotify = (SwitchPreferenceCompat) findPreference(AppPreferences.NOTIFICATION_RINGTONE_SOUND_KEY);
        rpmNotify.setEnabled(AppPreferences.INSTANCE.isNotificationEnabled());

        SwitchPreferenceCompat rpmMessage = (SwitchPreferenceCompat) findPreference(AppPreferences.MESSAGE_RINGTONE_SOUND_KEY);
        rpmMessage.setEnabled(AppPreferences.INSTANCE.isMessageNotificationEnable());
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void showAlert(final int title, final int message, final int yes, final int no, final BaseActivity.AlertListener listener) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                new AlertDialog.Builder(getActivity()).setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (listener != null)
                                    listener.yesOnClick();
                            }
                        }).setNegativeButton(no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create().show();
            }
        });
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_FINE_LOCATION") == 0;
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.d("Logout", "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Log.d("Logout", "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }
}
