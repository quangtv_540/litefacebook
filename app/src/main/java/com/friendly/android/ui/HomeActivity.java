package com.friendly.android.ui;//

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.friendly.android.AppConfig;
import com.friendly.android.Constants;
import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.event.UiEvent;
import com.friendly.android.manager.Theme;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.receiver.BootCompletedIntentReceiver;
import com.friendly.android.service.ReadRssService;
import com.friendly.android.ui.fragment.BaseFragment;
import com.friendly.android.ui.fragment.FeedsFragment;
import com.friendly.android.ui.fragment.InstagramFragment;
import com.friendly.android.ui.fragment.MessengerFragment;
import com.friendly.android.ui.fragment.TwitterFragment;
import com.friendly.android.ui.fragment.WallpapersFragment;
import com.friendly.android.utils.ToastAdListener;
import com.friendly.android.utils.TrackerScreen;
import com.friendly.android.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.Request;

import static com.friendly.android.FriendlyApplication.onRunning;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {


    HashMap<String, TabInfo> mapTabInfo;

    TabHost mTabHost;
    String currentTabId;
    boolean animating;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;

    @BindView(R.id.tabs_container)
    public RelativeLayout tabContainer;

    @BindView(R.id.divider)
    View divider;

    TextView badgeCount;

    public String sectionDefault;
    public boolean defaultTheme, blackTheme, draculaTheme, materialTheme;

    public static boolean isKeyboardShowing = false;

    public static HomeActivity context;

    public static TextView getToolbarTitleView(HomeActivity activity, Toolbar toolbar) {
        ActionBar actionBar = activity.getSupportActionBar();
        CharSequence actionbarTitle = null;
        if(actionBar != null)
            actionbarTitle = actionBar.getTitle();
        actionbarTitle = TextUtils.isEmpty(actionbarTitle) ? toolbar.getTitle() : actionbarTitle;
        if(TextUtils.isEmpty(actionbarTitle)) return null;
        // can't find if title not set
        for(int i= 0; i < toolbar.getChildCount(); i++){
            View v = toolbar.getChildAt(i);
            if(v != null && v instanceof TextView){
                TextView t = (TextView) v;
                CharSequence title = t.getText();
                if(!TextUtils.isEmpty(title) && actionbarTitle.equals(title) && t.getId() == View.NO_ID){
                    //Toolbar does not assign id to views with layout params SYSTEM, hence getId() == View.NO_ID
                    //in same manner subtitle TextView can be obtained.

                    t.setTypeface(Theme.INSTANCE.getTypeface("fonts/SF-UI-Text-Regular.otf"));
                    t.setTextSize(18);

                    return t;
                }
            }
        }
        return null;
    }

    /**
     * Change to use onClick to select tab
     *
     * @param tag
     */
    public void selectTab(Object tag) {
        selectTab(tag, false);
    }

    /**
     * Change to a tab with specified tag, with option to back to root
     *
     * @param tag
     * @param backToRoot
     */
    public void selectTab(Object tag, boolean backToRoot) {
        if (tag == null) {
            return;
        }

        int index = -1;
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); ++i) {
            if (tag.equals(mTabHost.getTabWidget().getChildAt(i).getTag())) {
                index = i;
                break;
            }
        }
        if (index < 0) {
            return;
        }
        String tabTag = tag.toString();

        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); ++i) {
            mTabHost.getTabWidget().getChildAt(i).setSelected(i == index);
        }
        TabInfo tabInfo = mapTabInfo.get(tabTag);
        if (tabInfo.backStack.size() == 0) {
            pushFragments(tabTag, Fragment.instantiate(this, tabInfo.topFragmentClass.getName(), tabInfo.bundle),
                    true, false, false);
        } else if ((backToRoot || tabTag.equals(currentTabId))
                && tabInfo.backStack.size() > 1) {
            popFragments(false, tabInfo.backStack.size() - 1, tabTag);
        } else if (!tabTag.equals(currentTabId)) {
            pushFragments(tabTag, tabInfo.backStack.peek(), false, false, false);
        } else if (tabTag.equals(currentTabId)) {
            if (tabTag.equals(Constants.TAB_FEEDS)) {
                EventBus.getDefault().post(new UiEvent.SelectTabNewsFeedEvent());
            } else if (tabTag.equals(Constants.TAB_INBOX)) {
                EventBus.getDefault().post(new UiEvent.SelectTabMessageEvent());
            }
        }
    }

    public void pushFragments(Fragment fragment) {
        pushFragments(currentTabId, fragment, true, true, false);
    }

    /**
     * Push Fragment and set show header or not
     * @param fragment
     * @param hideHeader
     */
    public void pushFragments(Fragment fragment, boolean hideHeader) {
        pushFragments(currentTabId, fragment, true, true, hideHeader);
    }

    /**
     * @param fragment
     */
    public void pushFragments(String tabId, Fragment fragment) {
        pushFragments(tabId, fragment, true, true, false);
    }

    /**
     * Push a fragment into backstack and display it
     *
     * @param tabId
     * @param fragment
     * @param addToBackStack
     * @param animate
     */
    public void pushFragments(String tabId, Fragment fragment,
                              boolean addToBackStack, boolean animate, boolean hideHeader) {
        Fragment currentFragment = getCurrentFragment();
        currentTabId = tabId;
        if (addToBackStack) {
            mapTabInfo.get(tabId).backStack.push(fragment);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        animating = true;
        if (animate) {
            ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
        }
        if (currentFragment != null) {
            ft.remove(currentFragment);
        }
        ft.add(R.id.realtabcontent, fragment, fragment.getClass().getName());
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        updateHeaderListener(fragment);
        animating = false;
    }

    /**
     * Pop a fragment from backstack and retrive last element from backstack to
     * be displayed
     *
     */
    public void popFragment() {
        popFragments(true);
    }

    /**
     * Pop a fragment from backstack and retrive last element from backstack to
     * be displayed
     *
     * @param animate
     */
    public void popFragments(boolean animate) {
        popFragments(animate, 1, null);
    }

    /**
     * Pop fragment(s) from backstack and retrive last element from backstack to
     * be displayed
     *
     * @param animate
     * @param depth
     */
    public void popFragments(boolean animate, int depth, String targetTag) {
        if (depth < 1) {
            return;
        }
        Fragment currentFragment = getCurrentFragment();
        if (targetTag != null) {
            currentTabId = targetTag;
        }
        if (mapTabInfo.get(currentTabId).backStack.size() == 1) {
            finishApp();
            return;
        }
        while (depth >= 1) {
            Fragment next = mapTabInfo.get(currentTabId).backStack.pop();
            depth--;
            if (mapTabInfo.get(currentTabId).backStack.size() < 2) {
                break;
            }
        }
        Fragment fragment = mapTabInfo.get(currentTabId).backStack.peek();
        animating = true;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (animate) {
            ft.setCustomAnimations(R.anim.back_slide_in, R.anim.back_slide_out);
        }
        if (currentFragment != null) {
            ft.remove(currentFragment);
        }
        ft.add(R.id.realtabcontent, fragment, fragment.getClass().getName());
        ft.addToBackStack(null).commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        updateHeaderListener(fragment);
        animating = false;
    }

    @Override
    public void onBackPressed() {
        Fragment fg = getCurrentFragment();
        if (fg instanceof HeaderControl && !animating) {
            ((HeaderControl) fg).onBackPressed();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getResources().updateConfiguration(newConfig, null);
        onContentChanged();
    }

    @Override
    public void onClick(View v) {
        selectTab(v.getTag());
        AppPreferences.INSTANCE.increaseValuesTabInter();
//        if (AppPreferences.INSTANCE.getValuesTabInter() > 0
//                && AppPreferences.INSTANCE.getValuesTabInter() % 10 == 0 && FriendlyApplication.isShowAds) {
//            mInterstitial.loadAd(new AdRequest.Builder().build());
//        }
        EventBus.getDefault().post(new UiEvent.UpdateBadgeMessageCount(0));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(this);

        sectionDefault = AppPreferences.INSTANCE.getSections();
        defaultTheme = AppPreferences.INSTANCE.getFreeTheme().equals("facebooktheme");
        blackTheme = AppPreferences.INSTANCE.getFreeTheme().equals("darktheme");
        draculaTheme = AppPreferences.INSTANCE.getFreeTheme().equals("draculatheme");
        materialTheme = AppPreferences.INSTANCE.getFreeTheme().equals("materialtheme");

        if (defaultTheme || materialTheme)
            setTheme(R.style.AppTheme_Blue);

        if (blackTheme)
            setTheme(R.style.AppTheme_Friendly_Black);

        if (draculaTheme)
            setTheme(R.style.AppTheme_Friendly_Dark);

        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        context = this;
        setSupportActionBar(toolbar);
        getToolbarTitleView(this, toolbar);

        if (mapTabInfo == null) {
            mapTabInfo = new HashMap<>();
        }

        String lockState = (String) getIntent().getSerializableExtra("state");
        if (lockState != null && lockState.equals("unlocked")) {
        } else {
            if (AppPreferences.INSTANCE.getBoolean("locker", false)) {
                startActivity(new Intent(HomeActivity.this, LitePinUnlock.class));
            }
        }

        TrackerScreen.openLiteScreen(this, HomeActivity.class.getSimpleName());

        callbackManager = CallbackManager.Factory.create();

        CookieManager manager = CookieManager.getInstance();
        manager.setAcceptCookie(true);

        String cookieString = manager.getCookie("https://m.facebook.com");
        if (!TextUtils.isEmpty(cookieString) && AppPreferences.INSTANCE.getUserName() == null)
            loadUserName(cookieString);

        initialiseTabHost(savedInstanceState);

        // start the service when it's activated but somehow it's not running
        // when it's already running nothing happens so it's ok
        if (AppPreferences.INSTANCE.isNotificationEnabled() || AppPreferences.INSTANCE.isMessageNotificationEnable()) {
            final Intent intent = new Intent(FriendlyApplication.getContext(), ReadRssService.class);
            FriendlyApplication.getContext().startService(intent);
            BootCompletedIntentReceiver.scheduleAlarms(getApplicationContext(), false);
        }

        /** if opened by a notification or a shortcut */
        try {
            if (getIntent().getExtras().getString("start_url") != null) {
                boolean isMessage = getIntent().getExtras().getBoolean("isMessage", false);
                if (isMessage) {
                    selectTab(Constants.TAB_INBOX);
                }
            }
        } catch (Exception ignored) {}
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRunning = true;
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        // Todo check keyboard showing
        checkKeyboardShowing();
    }

    @Override
    protected void onPause() {
        super.onPause();
        onRunning = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onRunning = false;
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    /**
     * Initialize tab host in current view
     *
     * @param bundle
     */
    private void initialiseTabHost(Bundle bundle) {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
        mTabHost.getTabWidget().setDividerDrawable(null);

        addTab(Constants.TAB_FEEDS, R.string.newsfeed,
                R.drawable.ic_home_black,
                FeedsFragment.class, bundle, false);
        addTab(Constants.TAB_INBOX, R.string.messenger,
                R.drawable.ic_message_black,
                MessengerFragment.class, bundle, false);
      
        addTab(Constants.TAB_INSTA,
                R.string.instagram,
                R.drawable.ic_instagram_black,
                InstagramFragment.class, bundle, false);
        if (AppPreferences.INSTANCE.isShowTwitterTab()) {
            addTab(Constants.TAB_TWITTER,
                    R.string.twitter,
                    R.drawable.ic_twitter_black,
                    TwitterFragment.class, bundle, false);
        }
        if (bundle == null) {
            selectTab(sectionDefault.equalsIgnoreCase("message") ? Constants.TAB_INBOX :
                    (sectionDefault.equalsIgnoreCase("newsfeed") ? Constants.TAB_FEEDS : Constants.TAB_INSTA));
        }
    }

    /**
     * Add a tab into a tab host of activity, with tab spec and tab info
     *
     * @param tabTag
     * @param tabName
     * @param tabView
     * @param clazz
     * @param bundle
     * @param finalTab
     */
    private void addTab(String tabTag, int tabName, int tabView,
                        Class<? extends Fragment> clazz, Bundle bundle, boolean finalTab) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tabTag);

        View tabIndicator = getLayoutInflater().inflate(R.layout.tab_icon,
                mTabHost.getTabWidget(), false);
        TextView tabTitle = (TextView) tabIndicator.findViewById(R.id.title);
        tabTitle.setText(getResources().getString(tabName));
        tabTitle.setTypeface(Theme.INSTANCE.getTypeface(Theme.Font.SF_MEDIUM));
        ((ImageView) tabIndicator.findViewById(R.id.icon)).setImageResource(tabView);
        tabIndicator.findViewById(R.id.badgeCount).setVisibility(View.GONE);

        if (tabTag.contains(Constants.TAB_INBOX)) {
            badgeCount = (TextView) tabIndicator.findViewById(R.id.badgeCount);
        }

        tabSpec.setContent(new TabFactory(this));
        tabSpec.setIndicator(tabIndicator);
        mTabHost.addTab(tabSpec);
        tabIndicator.setTag(tabTag);
        tabIndicator.setOnClickListener(this);

        TabInfo tabInfo = new TabInfo(tabTag, clazz, bundle, tabTitle, tabName);
        mapTabInfo.put(tabInfo.tag, tabInfo);
    }

    /**
     * Get fragment which currently displaying on screen
     *
     * @return
     */
    private Fragment getCurrentFragment() {
        if (currentTabId != null && mapTabInfo != null
                && mapTabInfo.get(currentTabId) != null
                && mapTabInfo.get(currentTabId).backStack != null
                && !mapTabInfo.get(currentTabId).backStack.isEmpty()) {
            return mapTabInfo.get(currentTabId).backStack.peek();
        }
        return null;
    }

    /**
     * Replace listener on header: delegate to fragment
     *
     * @param fg
     */
    private void updateHeaderListener(final Fragment fg) {
        appBarLayout.setExpanded(true, false);

        final BaseFragment base = (BaseFragment) fg;
        base.onHomeHeaderUpdate(toolbar);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (fg instanceof MessengerFragment) {
                    setupCollapseToolbar(true);
                } else {
                    setupCollapseToolbar(false);
                }
            }
        }, 1000);
    }

    boolean doubleBackToExitPressedOnce = false;
    void finishApp() {
        int index = AppPreferences.INSTANCE.getValues();
        if (!AppPreferences.INSTANCE.isRateApp()) {
            if (index == 4 || index == 8 || index == 16) {
                showRate();
                return;
            } else if (!AppPreferences.INSTANCE.isSharedLite2Facebook()
                    && (index == 6 || index == 12 || index == 24 || index == 40)) {
                showRemoveAdsOptions(true);
                return;
            }
        } else {
            if (!AppPreferences.INSTANCE.isSharedLite2Facebook()
                    && (index == 6 || index == 12 || index == 24 || index == 40)) {
                showRemoveAdsOptions(true);
                return;
            }
        }
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press back again to close Lite", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    /**
     * Delegate listener to child fragments
     */
    public interface HeaderControl {
        void onBackPressed();
    }

    /**
     * Information of a tab
     */
    private class TabInfo {

        private String tag;
        private Class<? extends Fragment> topFragmentClass;
        private Bundle bundle;
        private Stack<Fragment> backStack;

        private TextView tabTitle;
        private int tabName;

        public TabInfo(String tag, Class<? extends Fragment> topFragmentClass,
                       Bundle bundle, TextView tabTitle, int tabName) {
            this.tag = tag;
            this.topFragmentClass = topFragmentClass;
            this.bundle = bundle;
            this.tabName = tabName;
            this.tabTitle = tabTitle;
            backStack = new Stack<>();
        }

        /**
         * Refresh state of a tab
         */
        public void refreshState() {
            tabTitle.setText(getString(tabName));
            tabTitle.setTextSize(getResources().getDimension(
                    R.dimen.text_size_main_tab)
                    / getResources().getDisplayMetrics().scaledDensity);
            tabTitle.setPadding(0, 0, 0,
                    getResources().getDimensionPixelSize(R.dimen.padding_icon));
        }
    }

    /**
     * Factory tab builder
     */
    private class TabFactory implements TabHost.TabContentFactory {

        private final Context mContext;

        public TabFactory(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }

    private void loadUserName(final String cookieString) {
        // get username or id
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Document doc = Jsoup.connect("https://www.facebook.com/settings")
                            .userAgent("")
                            .cookie("https://m.facebook.com", cookieString).get();
                    Element element = doc.select("div._1k67[data-click] > a._2s25[href]").first();

                    String href = element.attr("href");
                    String userName = href.split("\\?")[0].split("facebook.com/")[1];
                    if(userName != null && !userName.contains("profile.php"))
                        AppPreferences.INSTANCE.setUserName(userName);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.UpdateThemeEvent e) {
        if (e.themeIndex == 0 || e.themeIndex == 3)
            setTheme(R.style.AppTheme_Blue);

        if (e.themeIndex == 1)
            setTheme(R.style.AppTheme_Friendly_Black);

        if (e.themeIndex == 2)
            setTheme(R.style.AppTheme_Friendly_Dark);

        recreate();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.HideTwitterTabEvent e) {
        recreate();
    }

    @Subscribe
    public void onEventMainThread(UiEvent.QuitConfirmPINEvent e) {
        finish();
    }

    /**
     * Todo show rate dialog
     */
    public void showRemoveAdsOptions (final boolean isBackPress) {
        new AlertDialog.Builder(this).setTitle(R.string.remove_ads_title)
                .setMessage(Html.fromHtml(getResources().getString(R.string.share_popup)))
                .setPositiveButton(R.string.btn_support_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        removeAdsByInvitation();
                    }
                }).setNegativeButton(R.string.btn_support_no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (isBackPress)
                        finish();
            }
        }).create()
                .show();
    }

    public CallbackManager callbackManager;

    public void removeAdsByInvitation () {
        TrackerScreen.openLiteScreen(this, HomeActivity.class.getSimpleName() + " Show Remove Ads");
        ShareLinkContent.Builder post = new ShareLinkContent.Builder();
        post.setContentTitle("Lite - One app for all your Facebook, Instagram and Messenger needs");
        post.setContentDescription(getString(R.string.welcome_message));
        post.setImageUrl(Uri.parse(AppConfig.FB_INVITE_PREV_IMG));
        post.setContentUrl(Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName()));

        ShareDialog shareDialog = new ShareDialog(HomeActivity.this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                TrackerScreen.openLiteScreen(HomeActivity.this, HomeActivity.class.getSimpleName()
                        + " Show Remove Ads Success");
                AppPreferences.INSTANCE.setSharedLite2Facebook(true);
                AppPreferences.INSTANCE.setShareInviteUnixTimeStamp(Calendar.getInstance().getTimeInMillis());
                FriendlyApplication.isShowAds = false;

                Toast.makeText(HomeActivity.this,
                        getString(R.string.success_remove_ads_by_invitation, Utils.REMOVED_DAYS), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
        ShareDialog.show(this, post.build());
    }

    private void showRate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle("Rate Lite");
        builder.setMessage(Html.fromHtml(getResources().getString(R.string.Rate_App)));
        builder.setPositiveButton("Yes, Rate Lite", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                AppPreferences.INSTANCE.saveRateApp();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(
                        getString(R.string.appirator_market_url), getPackageName()))));
                finish();
            }
        });
        builder.setNegativeButton("No, Thanks", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void checkKeyboardShowing () {
        findViewById(R.id.layout_main).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                findViewById(R.id.layout_main).getWindowVisibleDisplayFrame(r);
                int screenHeight = findViewById(R.id.layout_main).getRootView().getHeight();
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.15) {
                    isKeyboardShowing = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tabContainer.setVisibility(View.GONE);
                            divider.setVisibility(View.GONE);
                        }
                    }, 300);
                } else {
                    isKeyboardShowing = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tabContainer.setVisibility(View.VISIBLE);
                            divider.setVisibility(View.VISIBLE);
                        }
                    }, 300);

                }
            }
        });
    }

    InterstitialAd mInterstitial;

    void initInterstitialAd() {
        mInterstitial = new InterstitialAd(this);
        mInterstitial.setAdUnitId(Constants.ADMOB_INTERSTITIALS);
        mInterstitial.setAdListener(new ToastAdListener(this) {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mInterstitial.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getCurrentFragment().onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe
    public void onEventMainThread(UiEvent.UpdateBadgeMessageCount e) {
        Log.e("Lite", "onEventMainThread: " + e.numMessages);
        if (e.numMessages > 0 && badgeCount != null) {
            badgeCount.setVisibility(View.VISIBLE);
            badgeCount.setText(String.valueOf(e.numMessages));
        } else if (badgeCount != null) {
            badgeCount.setVisibility(View.GONE);
        }
    }

    private void setupCollapseToolbar(boolean isFixed) {
        AppBarLayout.LayoutParams params =
                (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        CoordinatorLayout.LayoutParams appBarLayoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        if (!isFixed) {
            params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                    | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
            appBarLayoutParams.setBehavior(new AppBarLayout.Behavior());
            appBarLayout.setLayoutParams(appBarLayoutParams);
        } else {
            params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED);
            appBarLayoutParams.setBehavior(null);
            appBarLayout.setLayoutParams(appBarLayoutParams);
        }
    }
}
