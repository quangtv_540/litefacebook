package com.friendly.android.ui;//

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.friendly.android.Constants;
import com.friendly.android.R;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.ui.fragment.SettingsFragment;
import com.friendly.android.utils.Miscellany;
import com.friendly.android.utils.ToastAdListener;
import com.friendly.android.utils.TrackerScreen;
import com.google.android.gms.ads.InterstitialAd;

import butterknife.BindView;
import butterknife.ButterKnife;

//
//  Lite for Facebook & Instagram
//
//  Created by Quang Tran on 9/29/2016.
//  Copyright (c) 2015 QQFresh Studio, Inc. All rights reserved.
//
public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();
    private static final int REQUEST_STORAGE = 1;

    public boolean defaultTheme, blackTheme, draculaTheme, materialTheme;

    @BindView(R.id.root)
    View root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        defaultTheme = AppPreferences.INSTANCE.getFreeTheme().equals("facebooktheme");
        blackTheme = AppPreferences.INSTANCE.getFreeTheme().equals("darktheme");
        draculaTheme = AppPreferences.INSTANCE.getFreeTheme().equals("draculatheme");
        materialTheme = AppPreferences.INSTANCE.getFreeTheme().equals("materialtheme");

        if (defaultTheme || materialTheme)
            setTheme(R.style.AppTheme_Blue);

        if (blackTheme)
            setTheme(R.style.AppTheme_Friendly_Black);

        if (draculaTheme)
            setTheme(R.style.AppTheme_Friendly_Dark);
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        TrackerScreen.openLiteScreen(this, SettingsActivity.class.getSimpleName());

        getFragmentManager().beginTransaction().replace(R.id.content_frame,
                new SettingsFragment()).commit();

//        int index = AppPreferences.INSTANCE.getValuesInter();
//        AppPreferences.INSTANCE.saveValuesInter(index+1);
//
//        if (AppPreferences.INSTANCE.getValuesInter() % 2 == 0 && FriendlyApplication.isShowAds) {
//            initInterstitialAd();
//            mInterstitial.loadAd(new AdRequest.Builder().build());
//        }
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0)
            super.onBackPressed();
        else
            getFragmentManager().popBackStack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.settings_bugs:
                Intent bugIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "quangadmob@gmail.com", null));
                bugIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " Bug");
                bugIntent.putExtra(Intent.EXTRA_TEXT, "I found a bug in Lite \n\n--"  + Miscellany.getDeviceInfo(this));
                startActivity(Intent.createChooser(bugIntent, getString(R.string.choose_email_client)));
                return true;

            case R.id.settings_feedback:
                Intent feedbackIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "quangadmob@gmail.com", null));
                feedbackIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " Feedback");
                feedbackIntent.putExtra(Intent.EXTRA_TEXT, "Here is some awesome feedback for " + getString(R.string.app_name));
                startActivity(Intent.createChooser(feedbackIntent, getString(R.string.choose_email_client)));
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // It's awesome, dude!
                } else {
                    Snackbar.make(root, getString(R.string.permission_not_granted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.back_slide_in, R.anim.back_slide_out);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (AppPreferences.INSTANCE.getValuesInter() % 2 == 0
//                && FriendlyApplication.isShowAds && mInterstitial != null && mInterstitial.isLoaded()) {
//            mInterstitial.show();
//        }
    }

    InterstitialAd mInterstitial;

    void initInterstitialAd() {
        mInterstitial = new InterstitialAd(this);
        mInterstitial.setAdUnitId(Constants.ADMOB_INTERSTITIALS);
        mInterstitial.setAdListener(new ToastAdListener(this) {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }
        });
    }
}
