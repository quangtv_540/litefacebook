package com.friendly.android.service;//


import com.friendly.android.model.BgrCategories;
import com.friendly.android.model.BgrPhotoCategories;
import com.friendly.android.model.BgrPhotoRecents;
import com.friendly.android.model.BgrPhotoTags;
import com.friendly.android.model.BgrPhotoUsers;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

//
//  Tripi
//
//  Created by Quang Tran on 2/8/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public interface BgrService {

    @GET(BgrUrl.CATEGORIES_API_URL)
    Call<BgrCategories> lookupBgrCategories();

    @GET(BgrUrl.RECENT_API_URL)
    Call<BgrPhotoRecents> listRecentPhotos();

    @GET(BgrUrl.RECENT_API_URL)
    Call<BgrPhotoRecents> listRecentNextPhotos(@Query("last_pos") String last_pos);

    @GET(BgrUrl.PHOTO_BY_CATEGORIES_API_URL)
    Call<BgrPhotoCategories> listPhotoByCategories(@Path("uuid") String uuid);

    @GET(BgrUrl.PHOTO_BY_CATEGORIES_API_URL)
    Call<BgrPhotoCategories> listNextPhotoByCategories(@Path("uuid") String uuid, @Query("last_pos") String last_pos);

    @GET(BgrUrl.PHOTO_BY_USER_API_URL)
    Call<BgrPhotoUsers> listPhotoByUsers(@Path("username") String username);

    @GET(BgrUrl.PHOTO_BY_USER_API_URL)
    Call<BgrPhotoUsers> listNextPhotoByUsers(@Path("username") String username, @Query("last_pos") String last_pos);

    @GET(BgrUrl.PHOTO_BY_TAGS_API_URL)
    Call<BgrPhotoTags> listPhotoByTags(@Path("tags") String tags);

    @GET(BgrUrl.PHOTO_BY_TAGS_API_URL)
    Call<BgrPhotoTags> listNextPhotoByTags(@Path("tags") String tags, @Query("last_pos") String last_pos);

    @GET(BgrUrl.SHUFFLE_API_URL)
    Call<BgrPhotoRecents> listShufflePhotos();

    @GET(BgrUrl.FEATURED_SETS_ITEM_API_URL)
    Call<BgrPhotoTags> listFeaturedSetsItem(@Path("uuid") String uuid);

    @GET(BgrUrl.POPULAR_DAILY_API_URL)
    Call<BgrPhotoRecents> listPopularDaily();

    @GET(BgrUrl.POPULAR_DAILY_API_URL)
    Call<BgrPhotoRecents> listNextPopularDaily(@Query("last_pos") String last_pos);

    @GET(BgrUrl.POPULAR_WEEKLY_API_URL)
    Call<BgrPhotoRecents> listPopularWeekly();

    @GET(BgrUrl.POPULAR_MONTHLY_API_URL)
    Call<BgrPhotoRecents> listPopularMonth();

    @GET(BgrUrl.POPULAR_ALL_API_URL)
    Call<BgrPhotoRecents> listPopularAllTime();

    @GET(BgrUrl.SIMILAR_API_URL)
    Call<BgrPhotoRecents> listPhotoBySimilar(@Path("uuid") int uuid);

    @GET(BgrUrl.COMMENTS_API_URL)
    Call<BgrPhotoRecents> listCommentByPhoto(@Query("object_id") String object_id);
}
