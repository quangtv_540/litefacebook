package com.friendly.android.service;//

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.friendly.android.FriendlyApplication;
import com.friendly.android.R;
import com.friendly.android.event.UiEvent;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.ui.HomeActivity;
import com.friendly.android.utils.NetworkUtil;
import com.friendly.android.utils.Utils;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

//
//  AsapChat
//
//  Created by Quang Tran on 8/11/2016.
//  Copyright (c) 2015 Superior Studio, Inc. All rights reserved.
//
public class ReadRssService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public static final String TAG = "IntentService";
    public static final String IS_FROM_FIREBASE_NOTFI = "is_from_firebase_notification";
    private static int count = 0;
    private Handler handler;
    public static int mNotificationId = 101001;
    public static int mMessageNotifId = 1000192;
    public ReadRssService(String name) {
        super("RSSService");
    }

    public ReadRssService() {
        super("RSSService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(this);
        }
        cookieManager.setAcceptCookie(true);
        String str = cookieManager.getCookie("https://m.facebook.com");

        if (TextUtils.isEmpty(str)) {
            return;
        }

        if (intent.getBooleanExtra(IS_FROM_FIREBASE_NOTFI, false) && AppPreferences.INSTANCE.isNotificationEnabled()){
            try {
                getGeneralNotifications(str);
                return;
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        //TODO
        if (NetworkUtil.hasConnection(this) && AppPreferences.INSTANCE.getUserId() != null
                && !FriendlyApplication.onRunning) {
            try {
                // general notification
                if (AppPreferences.INSTANCE.isNotificationEnabled())
                    try {
                        getGeneralNotifications(str);
                    } catch (Exception e) {
                    }
                //notification for message
                if (AppPreferences.INSTANCE.isMessageNotificationEnable())
                    try {
                        betterWaygetMessageNotification(str);
                    } catch (Exception e) {
                        e.printStackTrace();
                        downloadDom2GetMessageNotification(str);
                    }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void getGeneralNotifications (String cookie) throws Exception {
        String hrefElements = null;
        Document doc = Jsoup.connect("http://m.facebook.com/notifications")
                .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36")
                .cookie("https://m.facebook.com", cookie).get();
        Element pd = doc.select("div.aclb > div.touchable-notification").first();
        long lastNotificationTime = AppPreferences.INSTANCE.getLastNotificationTime();

        //gt
        Element at = pd.select("abbr[data-sigil]").first();
        JSONObject dataObject = new JSONObject(at.attr("data-store"));
        long notificationTime = dataObject.getLong("time") * 1000;
        //Log.e(TAG, "time: " + dataObject.get("time"));
        if (notificationTime <= lastNotificationTime) {
            throw new Exception(" Facebook Rss doesn't work");
        }
        // gl
        Element aLink = pd.select("a[href]").first();
        //Log.e(TAG, "href: " + "https://m.facebook.com" + aLink.attr("href"));
        String link = "https://m.facebook.com" + aLink.attr("href");

        // ga
        Element iT = pd.select("div.ib > i").first();
        String ir = Pattern.quote("url(\"") + "(.*?)" + Pattern.quote("\")");
        String aI = Utils.getTextBetween(iT.attr("style"), ir);
        //Log.e(TAG, "img: " + Utils.getTextBetween(iT.attr("style"), ir));

        //gt
        Elements sTs = pd.select("div.ib > div.c");
        //Log.e(TAG, "text: " + Html.fromHtml(sTs.html()).toString());
        String text = Html.fromHtml(sTs.html()).toString();

        String soundUri = AppPreferences.INSTANCE.getNotificationRingtone();
        boolean isVibrate = AppPreferences.INSTANCE.isEnableVibrate();
        pushNotification("Lite", text, link,
                notificationTime, aI, link.hashCode(),
                soundUri, isVibrate, false);

        EventBus.getDefault().post(new UiEvent.CountNotificationsEvent(1));

        AppPreferences.INSTANCE.setLastNotificationTime(notificationTime);
    }


    public void downloadDom2GetMessageNotification (String cookie) throws IOException {
        int version = Build.VERSION.SDK_INT;
        Document doc = Jsoup.connect("http://m.facebook.com/messages")
                .userAgent(version >= 19 ? "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36" :
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36")
                .cookie("https://m.facebook.com", cookie).get();
        //Utils.logLoge(TAG, doc.toString());
        try {
            for (Element tag : doc.getElementsByTag("script")){
                for (DataNode node : tag.dataNodes()) {
                    if(node.getWholeData().contains("_2ykg")) {
                        //Utils.logLoge(TAG, node.toString());
                        String contentRegex = Pattern.quote("\"content\":") + "(.*?)" + Pattern.quote(",\"pageletConfig\"");
                        String contentObjString = Utils.getTextBetween(node.toString(), contentRegex);
                        JSONObject content = new JSONObject(contentObjString);
                        doc = Jsoup.parse(content.getString("__html"));
                        //Utils.logLoge(TAG, contentObjString);
                        break;
                    }
                }
            }
            Element divParent = doc.select("div._2ykg").first().select("div").first().select("div.aclb").first();
            // gt
            Element abbrTag = divParent.select("abbr[data-sigil]").first();
            JSONObject dataObject = new JSONObject(abbrTag.attr("data-store"));
            long messageTime = dataObject.getLong("time") * 1000;
            //Log.e(TAG, "time: " + dataObject.get("time"));
            long lastMessageTime = AppPreferences.INSTANCE.getLastMessageTime();
            if (messageTime <= lastMessageTime) {
                throw new Exception(" Facebook Rss doesn't work");
            }
            //ga
            Element imgTag = divParent.child(0).select("div._5xu4").first().select("i").first();
            String imagRegex = Pattern.quote("url(\"") + "(.*?)" + Pattern.quote("\")");
            String avatarImg = Utils.getTextBetween(imgTag.attr("style"), imagRegex);
            //Log.e(TAG, "profile url" + avatarImg);

            //gt
            Element headerTag = divParent.select("div._5xu4 > header").first();
            String title = headerTag.select("h3").first().text();
            //Log.e(TAG, "title: " + title);
            String message = headerTag.select("h3").get(1).text();
            //Log.e(TAG, "message: " + message);
            String soundUri = AppPreferences.INSTANCE.getMessageRingtone();
            boolean isVibrate = AppPreferences.INSTANCE.isEnableVibrate();

            // gl
            Element aTag = divParent.select("div > div._5xu4 > a._5b6s").first();
            String link = aTag.attr("href");
            //Log.e(TAG, "link: " + link);
            pushNotification(title, message, "https://m.facebook.com" + link, messageTime, avatarImg,
                    mMessageNotifId, soundUri, isVibrate, true);

//            AppPreferences.INSTANCE.setLastNumberMessage(AppPreferences.INSTANCE.getLastNumberMessage() + 1);
            EventBus.getDefault().post(new UiEvent.CountMessageNotificationsEvent(1));

            AppPreferences.INSTANCE.setLastMessageTime(messageTime);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }


    public void betterWaygetMessageNotification (String cookie) throws IOException {
        Document doc = Jsoup.connect("https://m.facebook.com/mobile/messages/jewel/content/?spinner_id=u_0_b")
                .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36")
                .cookie("https://m.facebook.com", cookie)
                .get();
        String body = doc.getElementsByTag("body").first().text();
        body = body.substring(9);

        String htmlRe = Pattern.quote("\"html\":\"") + "(.*?)" + Pattern.quote("},{");
        String html = Utils.getTextBetween(body, htmlRe);
        html = html.substring(0, html.lastIndexOf(">\""))  + ">";
        html = StringEscapeUtils.unescapeJson(html);
        doc = Jsoup.parse(html);
        //Utils.logLoge(TAG, StringEscapeUtils.unescapeJson(html));
        Element liElement = doc.select("ol._7k7.inner > li.item").first();
        if(!liElement.classNames().contains("aclb")) return;
        //Log.e(TAG,liElement.classNames().toArray()[0].toString());
        Element aElement = liElement.select("a.touchable.primary[href]").first();
        String sMlink = aElement.attr("href");
        //Log.e(TAG, "link: " + sMlink);
        String sIRegex = Pattern.quote("url(\"") + "(.*?)" + Pattern.quote("\")");
        String sImg = Utils.getTextBetween(html, sIRegex);
        //Log.e(TAG, "img: " + sImg);

        Element stimeElement = aElement.select("div.content > div.lr > div.time > abbr").first();
        String sTimeRegex = Pattern.quote("time\":") + "(.*?)" + Pattern.quote(",\"");
        String sTimeString = Utils.getTextBetween(stimeElement.toString(), sTimeRegex);
        long smime = Long.parseLong(sTimeString) * 1000;
        long lastMessageTime = AppPreferences.INSTANCE.getLastMessageTime();
        if (smime <= lastMessageTime)
            return;
        //Log.e(TAG, "time: " + smime);


        Element sNameElement = aElement.select("div.content > div.lr > div.title").first();
        //Log.e(TAG, "Name: " + sNameElement.text());

        Element sMessElement = aElement.select("div.content > div.oneLine.preview").first();
        //Log.e(TAG, "Message: " + sMessElement.text());

        String soundUri = AppPreferences.INSTANCE.getMessageRingtone();
        boolean isVibrate = AppPreferences.INSTANCE.isEnableVibrate();

        pushNotification(sNameElement.text(), sMessElement.text(), "https://m.facebook.com" + sMlink, smime, sImg,
                mMessageNotifId, soundUri, isVibrate, true);

        EventBus.getDefault().post(new UiEvent.CountMessageNotificationsEvent(1));

        AppPreferences.INSTANCE.setLastMessageTime(smime);
    }


    public void pushNotification (String title, String message, String link,
                                  long time, String imageUrl, int notificationId,
                                  String soundUri, boolean isVibrate, boolean isMessage) {
        Bitmap avatar = null;
        if(imageUrl != null) {
            avatar = Utils.getImage(imageUrl);
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(isMessage ?
                                R.drawable.ic_stat_messages : R.drawable.ic_stat_notifications)
                        .setContentTitle(TextUtils.isEmpty(title) ? "Facebook Notification" : title)
                        .setContentText(message)
                        .setWhen(time)
                        .setAutoCancel(true)
                        .setLargeIcon(avatar);

        if (isMessage) {
            if (AppPreferences.INSTANCE.isMessageRingtoneSound())
                mBuilder.setSound(Uri.parse(soundUri));
        } else {
            if (AppPreferences.INSTANCE.isNotificationRingtoneSound())
                mBuilder.setSound(Uri.parse(soundUri));
        }

        // vibration
        if (isVibrate)
            mBuilder.setVibrate(new long[] {500, 500});
        else
            mBuilder.setVibrate(new long[] {0l});

        boolean isLedLight = AppPreferences.INSTANCE.isEnableLedLight();

        // LED light
        if (isLedLight)
            mBuilder.setLights(0xFF446BB0, 400, 300);


        // priority for Heads-up
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            mBuilder.setPriority(Notification.PRIORITY_HIGH);

        Intent resultIntent = new Intent(this, HomeActivity.class);
        resultIntent.setAction("ALL_NOTIFICATIONS_ACTION");
        resultIntent.putExtra("start_url", link);
        resultIntent.putExtra("isMessage", isMessage);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notif = mBuilder.build();

        if (isVibrate)
            notif.defaults |= Notification.DEFAULT_VIBRATE;
        if (isLedLight)
            notif.flags |= Notification.FLAG_SHOW_LIGHTS;

        mNotifyMgr.notify(notificationId, mBuilder.build());
    }

    // cancel all the notifications which are visible at the moment
    public static void cancelAllNotifications() {
        NotificationManager notificationManager = (NotificationManager)
                FriendlyApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
}
