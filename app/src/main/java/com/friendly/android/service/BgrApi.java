package com.friendly.android.service;//

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//
//  Tripi
//
//  Created by Quang Tran on 2/8/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class BgrApi {

    static BgrService bgrService;

    static {
        setupBgr();
    }

    static void setupBgr() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BgrUrl.BGR_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        if (bgrService == null) {
            bgrService = retrofit.create(BgrService.class);
        }
    }

    public static BgrService getBgrService() {
        return bgrService;
    }
}
