package com.friendly.android.service;//

//  
//  Tripi
//
//  Created by Quang Tran on 2/8/2017.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public class BgrUrl {

    public static final String BGR_API_URL = "https://lite.bgr.com/api/";
    public static final String CATEGORIES_API_URL = "categories";
    public static final String RECENT_API_URL = "backgrounds/recent";
    public static final String PHOTO_BY_CATEGORIES_API_URL = "categories/{uuid}/backgrounds";
    public static final String PHOTO_BY_USER_API_URL = "users/{username}/backgrounds";
    public static final String PHOTO_BY_TAGS_API_URL = "tags/{tags}/backgrounds";
    public static final String SHUFFLE_API_URL = "backgrounds/shuffle";
    public static final String FEATURED_SETS_API_URL = "featuredsets";
    public static final String FEATURED_SETS_ITEM_API_URL = "featuredsets/{uuid}/backgrounds";
    public static final String FEATURED_USERS_API_URL = "featuredusers";
    public static final String POPULAR_DAILY_API_URL = "backgrounds/popular/daily";
    public static final String POPULAR_WEEKLY_API_URL = "backgrounds/popular/weekly";
    public static final String POPULAR_MONTHLY_API_URL = "backgrounds/popular/monthly";
    public static final String POPULAR_ALL_API_URL = "backgrounds/popular";
    public static final String SIMILAR_API_URL ="backgrounds/{uuid}/similar";
    public static final String COMMENTS_API_URL ="comments";
    public static final String SEARCH_TAGS_API_URL ="tags/search";
    public static final String SEARCH_USERS_API_URL ="users/search";
}
