package com.friendly.android.preferences;//

import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.preference.PreferenceManager;

import com.friendly.android.R;

//
//  Tripi
//
//  Created by Quang Tran on 9/28/2016.
//  Copyright (c) 2015 Tripi, Inc. All rights reserved.
//
public enum AppPreferences {

    INSTANCE;

    private SharedPreferences preferences;
    public Context applicationContext;

    public static final String IS_FIRST = "friendly.is.first";

    public static final String MESSAGE_RINGTONE_KEY = "friendly.message_ringtone";
    public static final String MESSAGE_RINGTONE_SOUND_KEY = "friendly.message_ringtone.sound";
    public static final String NOTIFICATION_RINGTONE_KEY = "friendly.general_ringtone";
    public static final String NOTIFICATION_RINGTONE_SOUND_KEY = "friendly.general_ringtone.sound";

    public static final String LAST_MESSENGER_TIME = "friendly.last_message_time";
    public static final String LAST_NOTIFICATION_TIME = "friendly.last_notification_time";
    public static final String LAST_NUMBER_MESSAGE = "friendly.last_number_message";

    private static final String DEVICE_ID = "friendly.device.id";
    private static final String KEY_NOTIFICATIONS_ID = "friendly.id.notifications";

    public static final String FIREBASE_TOKEN_KEY = "friendly.firebase_token_key";
    public static final String LAST_TIME_UPDATE_COOKIE = "friendly.last_time_update_cookie";
    public static final String REGISTER_NOTIFICATION_SUCCEED = "friendly.register_user_notification_succeed";

    public static final String NOTIFICATION_ENABLE = "friendly.notification.enable";
    public static final String MESSAGE_ENABLE = "friendly.message.enable";

    public static final String ASK_TO_DOWNLOAD_VIDEO = "friendly.ask.to.download.video";

    public static final String USER_ID = "friendly.user.id";
    public static final String USER_NAME = "friendly.username";


    public static final String FACEBOOK_FONT_SIZE="font_pref";
    public static final String FACEBOOK_THEMES="theme_preference_fb";
    public static final String FACEBOOK_SECTIONS ="section_preference_fb";

    private final static String RATE_APP = "lite.rate_app";
    private final static String VALUES = "lite.values";
    private final static String INTER_VALUES = "lite.inter.values";
    private final static String INTER_CHAT_VALUES = "lite.inter.chat.values";
    private final static String INTER_TAB = "lite.inter.tabselect.values";

    public static void load(Context context) {
        INSTANCE.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        INSTANCE.applicationContext = context;
    }

    public void setOnlyWallpapers(boolean isOnly) {
        preferences.edit().putBoolean("lite.isOnlyWallpapers", isOnly).commit();
    }

    public boolean isOnlyWallpapers() {
        return preferences.getBoolean("lite.isOnlyWallpapers", false);
    }

    public String getPackageWallpapers() {
        return preferences.getString("lite.packagename.wallpapers", "https://play.google.com/store/apps/details?id=com.bgrhd.android");
    }
    public void setPackageWallpapers(String packagename) {
        preferences.edit().putString("lite.packagename.wallpapers", packagename).commit();
    }

    public String getThemeUrlCss(String url) {
        return preferences.getString("Lite."+url, null);
    }
    public void setThemeUrlCss(String url, String css) {
        preferences.edit().putString("Lite."+url, css).commit();
    }

    public void setIsFirst(boolean isFirst) {
        preferences.edit().putBoolean(IS_FIRST, isFirst).commit();
    }

    public boolean isFist() {
        return preferences.getBoolean(IS_FIRST, true);
    }

    // Todo Settings: Notifications
    public void setNotificationEnabled(boolean isSucceed) {
        preferences.edit().putBoolean(NOTIFICATION_ENABLE, isSucceed).commit();
    }

    public boolean isNotificationEnabled() {
        return preferences.getBoolean(NOTIFICATION_ENABLE, false);
    }

    public void setMessageNotificationEnable(boolean isSucceed) {
        preferences.edit().putBoolean(MESSAGE_ENABLE, isSucceed).commit();
    }

    public boolean isMessageNotificationEnable() {
        return preferences.getBoolean(MESSAGE_ENABLE, true);
    }

    public String getMessageRingtone () {
        return preferences.getString(MESSAGE_RINGTONE_KEY,
                "android.resource://" + applicationContext.getPackageName() + "/" + R.raw.bling);
    }

    public void setMessageRingtoneSound(boolean ringtone) {
        preferences.edit().putBoolean(MESSAGE_RINGTONE_SOUND_KEY, ringtone).commit();
    }

    public boolean isMessageRingtoneSound() {
        return preferences.getBoolean(MESSAGE_RINGTONE_SOUND_KEY, true);
    }

    public String getNotificationRingtone () {
        return preferences.getString(NOTIFICATION_RINGTONE_KEY,
                RingtoneManager.getDefaultUri(RingtoneManager
                        .TYPE_NOTIFICATION).toString());
    }

    public void setNotificationRingtoneSound(boolean ringtone) {
        preferences.edit().putBoolean(NOTIFICATION_RINGTONE_SOUND_KEY, ringtone).commit();
    }

    public boolean isNotificationRingtoneSound() {
        return preferences.getBoolean(NOTIFICATION_RINGTONE_SOUND_KEY, true);
    }

    public void setLastMessageTime (long time) {
        preferences.edit().putLong(LAST_MESSENGER_TIME, time).commit();
    }
    public long getLastMessageTime () {
        return preferences.getLong(LAST_MESSENGER_TIME, 0);
    }

    public void setLastNotificationTime (long time) {
        preferences.edit().putLong(LAST_NOTIFICATION_TIME, time).commit();
    }
    public long getLastNotificationTime () {
        return preferences.getLong(LAST_NOTIFICATION_TIME, 0);
    }

    public boolean isEnableSound() {
        return preferences.getBoolean("friendly.enable_sound", true);
    }
    public void setEnableSound(boolean enable) {
        preferences.edit().putBoolean("friendly.enable_sound", enable).commit();
    }

    public boolean isEnableVibrate() {
        return preferences.getBoolean("friendly.enable_vibrate", true);
    }
    public void setEnableVibrate(boolean enable) {
        preferences.edit().putBoolean("friendly.enable_vibrate", enable).commit();
    }

    public boolean isEnableLedLight() {
        return preferences.getBoolean("friendly.enable_led_light", true);
    }
    public void setEnableLedLight(boolean enable) {
        preferences.edit().putBoolean("friendly.enable_led_light", enable).commit();
    }

    public long getIntervalNotifications() {
        return preferences.getLong("friendly.interval_pref", 600000);
    }
    public void setIntervalNotifications(long interval) {
        preferences.edit().putLong("friendly.interval_pref", interval).commit();
    }


    public String getUserId() {
        return preferences.getString(USER_ID, null);
    }
    public void setUserId(String userId) {
        preferences.edit().putString(USER_ID, userId).commit();
    }

    public String getUserName() {
        return preferences.getString(USER_NAME, null);
    }
    public void setUserName(String userName) {
        preferences.edit().putString(USER_NAME, userName).commit();
    }

    public void setShowTwitterTab(boolean show) {
        preferences.edit().putBoolean("lite.hide.twitter", show).commit();
    }

    public boolean isShowTwitterTab() {
        return preferences.getBoolean("lite.hide.twitter", true);
    }

    public void setShowWallpapersTab(boolean show) {
        preferences.edit().putBoolean("lite.hide.wallpapers", show).commit();
    }

    public boolean isShowWallpapersTab() {
        return preferences.getBoolean("lite.hide.wallpapers", true);
    }

    public void setAskToDownloadVideo(boolean asked) {
        preferences.edit().putBoolean(ASK_TO_DOWNLOAD_VIDEO, asked).commit();
    }

    public boolean isAskToDownloadVideo() {
        return preferences.getBoolean(ASK_TO_DOWNLOAD_VIDEO, true);
    }

    public String getFont(){
        return preferences.getString(FACEBOOK_FONT_SIZE, "default_font");
    }

    public String getFreeTheme() { return preferences.getString(FACEBOOK_THEMES, "facebooktheme"); }

    public String getSections() { return preferences.getString(FACEBOOK_SECTIONS, "newsfeed"); }

    public boolean isSharedLite2Facebook () {
        return preferences.getBoolean("lite.share.facebook", false);
    }

    public void setSharedLite2Facebook (boolean isShared) {
        preferences.edit().putBoolean("lite.share.facebook", isShared).commit();
    }

    public void setShareInviteUnixTimeStamp (long timeStamp) {
        preferences.edit().putLong("lite.last.time.share.invite", timeStamp).commit();
    }

    public long getLastTimeShareInvite () {
        return preferences.getLong("lite.last.time.share.invite", 0);
    }

    public boolean isBlockPhotoInstagram() {
        return preferences.getBoolean("lite.instagram.block.photo", false);
    }
    public void setBlockPhotoInstagram(boolean block) {
        preferences.edit().putBoolean("lite.instagram.block.photo", block).commit();
    }

    public boolean isBlockPhotoTwitter() {
        return preferences.getBoolean("lite.twitter.block.photo", false);
    }
    public void setBlockPhotoTwitter(boolean block) {
        preferences.edit().putBoolean("lite.twitter.block.photo", block).commit();
    }

    /**
     * Save value of Loft
     * @param
     */
    public void saveRateApp() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(RATE_APP, true);
        editor.commit();
    }

    /**
     * Get value of Loft
     * @return
     */
    public boolean isRateApp() {
        return preferences.getBoolean(RATE_APP, false);
    }


    public void removeRateApp() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(RATE_APP);
        editor.commit();
    }

    public void saveValues(int values) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(VALUES, values);
        editor.commit();
    }

    public int getValues() {
        return preferences.getInt(VALUES, 0);
    }

    public void saveValuesInter(int values) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(INTER_VALUES, values);
        editor.commit();
    }

    public int getValuesInter() {
        return preferences.getInt(INTER_VALUES, 0);
    }

    public void increaseValuesChatInter() {
        int values = getValuesChatInter() + 1;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(INTER_CHAT_VALUES, values);
        editor.commit();
    }

    public int getValuesChatInter() {
        return preferences.getInt(INTER_CHAT_VALUES, 0);
    }

    public int getValuesTabInter() {
        return preferences.getInt(INTER_TAB, 0);
    }

    public void increaseValuesTabInter() {
        int values = getValuesTabInter() + 1;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(INTER_TAB, values);
        editor.commit();
    }

    public boolean getBoolean(String key, boolean defValue){
        return preferences.getBoolean(key, defValue);
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public int getInt(String key, int defValue){
        return preferences.getInt(key, defValue);
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public long getLong(String key, long defValue){
        return preferences.getLong(key, defValue);
    }

    public void putLong(String key, long value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }
    public String getString(String key, String defValue){
        return preferences.getString(key, defValue);
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
    }
}
