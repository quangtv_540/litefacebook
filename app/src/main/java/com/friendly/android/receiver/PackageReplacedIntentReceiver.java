package com.friendly.android.receiver;//

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.friendly.android.FriendlyApplication;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.service.ReadRssService;

//
//  AsapChat
//
//  Created by Quang Tran on 4/21/2016.
//  Copyright (c) 2015 AsapChat, Inc. All rights reserved.
//
public class PackageReplacedIntentReceiver extends BroadcastReceiver {

    public static AlarmManager mgr;
    public static final int requestCode = 1002;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("Friendly", "********** Package replaced! PackageReplacedIntentReceiver **********");
        context = FriendlyApplication.getContext();

        // create service start intent
        Intent startIntent = new Intent(context, ReadRssService.class);

        // start notifications service when it's activated at Settings
        if (AppPreferences.INSTANCE.isNotificationEnabled() || AppPreferences.INSTANCE.isMessageNotificationEnable()) {
            context.startService(startIntent);
        }
    }

}
