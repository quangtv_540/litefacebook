package com.friendly.android.receiver;//

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.friendly.android.FriendlyApplication;
import com.friendly.android.preferences.AppPreferences;
import com.friendly.android.service.ReadRssService;


//
//  AsapChat
//
//  Created by Quang Tran on 4/21/2016.
//  Copyright (c) 2015 AsapChat, Inc. All rights reserved.
//
public class BootCompletedIntentReceiver extends BroadcastReceiver {


    public static AlarmManager mgr;
    public static final int requestCode = 1002;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i("BroadcastReceiver", "********** Boot time! **********");
        context = FriendlyApplication.getContext();

        Intent startIntent = new Intent(context, ReadRssService.class);

        // start notifications service when it's activated at Settings
        if (AppPreferences.INSTANCE.isNotificationEnabled() || AppPreferences.INSTANCE.isMessageNotificationEnable()) {
            context.startService(startIntent);
        }

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            scheduleAlarms(context, false);
        }
    }

    public static void scheduleAlarms(Context ctxt, boolean cancel) {
        if (mgr == null)
            mgr = (AlarmManager) ctxt.getSystemService(Context.ALARM_SERVICE);
        Intent intentService = new Intent(ctxt, ReadRssService.class);
        PendingIntent pi = PendingIntent.getService(ctxt, requestCode, intentService, PendingIntent.FLAG_UPDATE_CURRENT);

        if (!cancel && (AppPreferences.INSTANCE.isNotificationEnabled() || AppPreferences.INSTANCE.isMessageNotificationEnable())) {
            long interval = AppPreferences.INSTANCE.getIntervalNotifications();
            mgr.setRepeating(AlarmManager.ELAPSED_REALTIME, 5000, interval, pi);
        } else {
            mgr.cancel(pi);
        }
    }

    public static void cancelAlarm (Context ctxt) {
        Intent intentService = new Intent(ctxt, ReadRssService.class);
        PendingIntent alarmIntent = PendingIntent.getService(ctxt, requestCode, intentService, PendingIntent.FLAG_UPDATE_CURRENT);
        if (mgr == null)
            mgr = (AlarmManager) ctxt.getSystemService(Context.ALARM_SERVICE);
        mgr.cancel(alarmIntent);
    }
}
